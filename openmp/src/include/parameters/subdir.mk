
# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/source/include/parameters/IParamCalculator.cpp \


OBJS += \
./src/source/include/parameters/IParamCalculator.o \


CPP_DEPS += \
./src/source/include/parameters/IParamCalculator.d \


# Each subdirectory must supply rules for building sources it contributes
src/source/include/parameters/%.o: ../src/source/include/parameters/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -Os -g3 -Wall -c -fmessage-length=0 -fopenmp -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
