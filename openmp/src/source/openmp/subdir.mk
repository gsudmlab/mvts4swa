
# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/source/openmp/SolarFeatureCalc_omp.cpp 

OBJS += \
./src/source/openmp/SolarFeatureCalc_omp.o 

CPP_DEPS += \
./src/source/openmp/SolarFeatureCalc_omp.d 


# Each subdirectory must supply rules for building sources it contributes
src/source/openmp/%.o: ../src/source/openmp/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -Os -g3 -Wall -c -fmessage-length=0 -fopenmp -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


