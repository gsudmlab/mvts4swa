
# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/source/openmp/filters/LanczosDownSampleFilter_omp.cpp \
../src/source/openmp/filters/BoxCarSmoothingFilter_omp.cpp \
../src/source/openmp/filters/GaussianSmoothingFilter_omp.cpp \
../src/source/openmp/filters/AverageDownSampleFilter_omp.cpp \
../src/source/openmp/filters/PIL_Filter_omp.cpp \
../src/source/openmp/filters/PIL_Filter_W_Gauss_omp.cpp \

OBJS += \
./src/source/openmp/filters/LanczosDownSampleFilter_omp.o \
./src/source/openmp/filters/BoxCarSmoothingFilter_omp.o \
./src/source/openmp/filters/GaussianSmoothingFilter_omp.o \
./src/source/openmp/filters/AverageDownSampleFilter_omp.o \
./src/source/openmp/filters/PIL_Filter_omp.o \
./src/source/openmp/filters/PIL_Filter_W_Gauss_omp.o \

CPP_DEPS += \
./src/source/openmp/filters/LanczosDownSampleFilter_omp.d \
./src/source/openmp/filters/BoxCarSmoothingFilter_omp.d \
./src/source/openmp/filters/GaussianSmoothingFilter_omp.d \
./src/source/openmp/filters/AverageDownSampleFilter_omp.d \
./src/source/openmp/filters/PIL_Filter_omp.d \
./src/source/openmp/filters/PIL_Filter_W_Gauss_omp.d \


# Each subdirectory must supply rules for building sources it contributes
src/source/openmp/filters/%.o: ../src/source/openmp/filters/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -Os -g3 -Wall -c -fmessage-length=0 -fopenmp -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


