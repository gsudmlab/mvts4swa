"""
Most of the code in this file is from astropy.  We needed to have an asynchronous 
reading of the files on disk, and were not concerned with files too large to fit
into memory.  

FITS File Reader

Notes
-----
FITS
	[1] FITS files allow comments to be attached to every value in the header.
	This is implemented in this module as a KEYCOMMENTS dictionary in the
	sunpy header. To add a comment to the file on write, add a comment to this
	dictionary with the same name as a key in the header (upcased).

	[2] Due to the way `~astropy.io.fits` works with images the header dictionary may
	differ depending on whether is accessed before or after the fits[0].data
	is requested. If the header is read before the data then the original
	header will be returned. If the header is read after the data has been
	accessed then the data will have been scaled and a modified header
	reflecting these changes will be returned: BITPIX may differ and
	BSCALE and B_ZERO may be dropped in the modified version.

References
----------
| https://stackoverflow.com/questions/456672/class-factory-in-python
"""

import io
import warnings
import traceback
import collections
import asyncio
import aiofiles

from astropy.io.fits.hdu.hdulist import HDUList
from astropy.io.fits.hdu.hdulist import _BaseHDU
from sunpy.io.header import FileHeader
from sunpy.extern.six.moves import zip
import numpy as np


HDPair = collections.namedtuple('HDPair', ['data', 'header'])

async def read(filepath, hdus = None, memmap = None, **kwargs):
	"""
	Read a fits file

	Parameters
	----------
	filepath : `str`
		The fits file to be read
	hdu: `int` or iterable
		The HDU indexes to read from the file

	Returns
	-------
	pairs : `list`
		A list of (data, header) tuples

	Notes
	-----
	This routine reads all the HDU's in a fits file and returns a list of the
	data and a FileHeader instance for each one.
	Also all comments in the original file are concatenated into a single
	'comment' key in the returned FileHeader.
	"""

	"""
	Read the file async as astropy and sunpy do not do this and we don't want to block
	while waiting for the system to get the file, potentially from another machine on 
	the network. 
	"""
	buffer = io.BytesIO()
	async with aiofiles.open(filepath, 'rb') as f:
			contents = await f.read()
			buffer.write(contents)
	buffer.seek(0)

	mode = 'readonly'
	memmap = False
	lazy_load_hdus = False
	save_backup = False
	cache = False
	hdulist = HDUList.fromfile(buffer, mode, memmap, save_backup, cache, lazy_load_hdus, **kwargs)

	if hdus is not None:
		if isinstance(hdus, int):
			hdulist = hdulist[hdus]
		elif isinstance(hdus, collections.Iterable):
			hdulist = [hdulist[i] for i in hdus]

	hdulist.verify('silentfix+warn')

	headers = get_header(hdulist)
	pairs = []

	for i, (hdu, header) in enumerate(zip(hdulist, headers)):
		try:
			pairs.append(HDPair(hdu.data, header))
		except (KeyError, ValueError) as e:
			message = "Error when reading HDU {}. Skipping.\n".format(i)
			for line in traceback.format_tb(sys.exc_info()[2]):
				message += line
				message += '\n'
			message += repr(e)
			warnings.warn(message, Warning, stacklevel = 2)

	return pairs


def get_header(afile):
	"""
	Read a fits file and return just the headers for all HDU's. In each header,
	the key WAVEUNIT denotes the wavelength unit which is used to describe the
	value of the key WAVELNTH.

	Parameters
	----------
	afile : `str` or fits.HDUList
		The file to be read, or HDUList to process.

	Returns
	-------
	headers : `list`
		A list of FileHeader headers.
	"""

	hdulist = afile
	close = False

	headers = []
	for hdu in hdulist:
		try:
			comment = "".join(hdu.header['COMMENT']).strip()
		except KeyError:
			comment = ""
		try:
			history = "".join(hdu.header['HISTORY']).strip()
		except KeyError:
			history = ""

		header = FileHeader(hdu.header)
		header['COMMENT'] = comment
		header['HISTORY'] = history

		# Strip out KEYCOMMENTS to a dict, the hard way
		keydict = {}
		for card in hdu.header.cards:
			if card.comment != '':
				keydict.update({card.keyword:card.comment})
		header['KEYCOMMENTS'] = keydict
		header['WAVEUNIT'] = extract_waveunit(header)

		headers.append(header)

	return headers


def extract_waveunit(header):
	"""Attempt to read the wavelength unit from a given FITS header.

	Parameters
	----------
	header : FileHeader
		One :class:`sunpy.io.header.FileHeader` instance which was created by
		reading a FITS file. :func:`sunpy.io.fits.get_header` returns a list of
		such instances.

	Returns
	-------
	waveunit : `str`
		The wavelength unit that could be found or ``None`` otherwise.

	Examples
	--------
	The goal of this function is to return a string that can be used in
	conjunction with the astropy.units module so that the return value can be
	directly passed to ``astropy.units.Unit``::

		>>> import astropy.units
		>>> header = {'WAVEUNIT': 'Angstrom', 'KEYCOMMENTS': {}}
		>>> waveunit = extract_waveunit(header)
		>>> if waveunit is not None:
		...	 unit = astropy.units.Unit(waveunit)

	"""

	# algorithm: try the following procedures in the following order and return
	# as soon as a waveunit could be detected
	# 1. read header('WAVEUNIT'). If None, go to step 2.
	# 1.1 -9 -> 'nm'
	# 1.2 -10 -> 'angstrom'
	# 1.3 0 -> go to step 2
	# 1.4 if neither of the above, return the value itself in lowercase
	# 2. parse waveunit_comment
	# 2.1 'in meters' -> 'm'
	# 3. parse wavelnth_comment
	# 3.1 "[$UNIT] ..." -> $UNIT
	# 3.2 "Observed wavelength ($UNIT)" -> $UNIT
	def parse_waveunit_comment(waveunit_comment):
		if waveunit_comment == 'in meters':
			return 'm'

	waveunit_comment = header['KEYCOMMENTS'].get('WAVEUNIT')
	wavelnth_comment = header['KEYCOMMENTS'].get('WAVELNTH')
	waveunit = header.get('WAVEUNIT')
	if waveunit is not None:
		metre_submultiples = {
			0: parse_waveunit_comment(waveunit_comment),
			-1: 'dm',
			-2: 'cm',
			-3: 'mm',
			-6: 'um',
			-9: 'nm',
			-10: 'angstrom',
			-12: 'pm',
			-15: 'fm',
			-18: 'am',
			-21: 'zm',
			-24: 'ym'}
		waveunit = metre_submultiples.get(waveunit, str(waveunit).lower())
	elif waveunit_comment is not None:
		waveunit = parse_waveunit_comment(waveunit_comment)
	elif wavelnth_comment is not None:
		# supported formats (where $UNIT is the unit like "nm" or "Angstrom"):
		#   "Observed wavelength ($UNIT)"
		#   "[$UNIT] ..."
		parentheses_pattern = r'Observed wavelength \((\w+?)\)$'
		brackets_pattern = r'^\[(\w+?)\]'
		for pattern in [parentheses_pattern, brackets_pattern]:
			m = re.search(pattern, wavelnth_comment)
			if m is not None:
				waveunit = m.group(1)
				break
	if waveunit == '':
		return None  # To fix problems associated with HMI FITS.
	return waveunit

async def get_correctly_scaled_data(fileName, object, scale_type = 'int16') :

	

	#print("scaling")
	if object.meta['bitpix'] == 8 and object.meta.has_key('bzero') and object.meta.has_key('bscale'):
		bzero = object.meta.has_key('bzero')
		bscale = object.meta.has_key('bscale')
		f = await read(filepath = fileName, do_not_scale_image_data = True)
		hdu = f[1 if len(f) > 1 else 0]
	
		if bzero != 0:
			np.add(hdu.data , -bzero, out=hdu.data, casting='unsafe')
		if bscale and bscale != 1:
			hdu.data[:, :] /= bscale

		data=np.array(np.around(hdu.data), dtype=type)
		hdu.header['BZERO'] = 0
		hdu.header['BSCALE'] = 1
		hdu = HDUList([_BaseHDU(data, hdu.header)])
		#print("scaled")
		return hdu

	return object

