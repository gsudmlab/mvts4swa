import os
import traceback
import ctypes as ct
import xml.etree.ElementTree as etree


class SolarCompConfig():

	def __init__(self, configFile, initLib = True):
		self._lib = None
		self._numThreads = 1
		self._batchSize = 10
		self._missingDataFile = None
		self._rawDataDir = None
		self._headerDataDir = None
		self._computedDataDir = None
		self._drmsClientName = None
		self._initLib = initLib

		tree = etree.parse(configFile)
		self.__readConfig(tree)

	@property
	def lib(self):
		return self._lib

	@property
	def numThreads(self):
		return self._numThreads

	@property
	def batchSize(self):
		return self._batchSize

	@property
	def missingDataFile(self):
		return self._missingDataFile

	@property
	def rawDataDir(self):
		return self._rawDataDir

	@property
	def headerDataDir(self):
		return self._headerDataDir

	@property
	def computedDataDir(self):
		return self._computedDataDir

	@property
	def drmsClientName(self):
		return self._drmsClientName

	def __readConfig(self, xmlTree):

		root = xmlTree.getroot()

		dataDirectoriesElement = root.find('data-directories')
		self.__processDataDirectories(dataDirectoriesElement)

		logSettingsElement = root.find('log-settings')
		self.__processLogSettings(logSettingsElement)

		processingSettingsElement = root.find('processing-settings')
		self.__procesProcessingSettings(processingSettingsElement)

		drmsSettingsElement = root.find('drms-settings')
		self.__processDRMSConfig(drmsSettingsElement)

	def __processDataDirectories(self, elementRoot):

		argstr = """
	Config file requires a <data-directories> element as follows:
	
			<data-directories>
			<base-dir value="/data/SHARPS" />
			<raw-files-dir value="/raw-sharps" />
			<header-files-dir value="/sharps-headers" />
			<computed-header-files-dir value="/new-data" />
		</data-directories>
			"""

		try:

			if elementRoot is not None:
				baseDirElem = elementRoot.find('base-dir')
				if baseDirElem is not None:
					baseDataDir = baseDirElem.attrib['value']
				else:
					raise ConfigError('')

				rawFilesDirElem = elementRoot.find('raw-files-dir')
				if rawFilesDirElem is not None:
					self._rawDataDir = baseDataDir + rawFilesDirElem.attrib['value']

				else:
					raise ConfigError('')

				headerFilesDirElem = elementRoot.find('header-files-dir')
				if headerFilesDirElem is not None:
					self._headerDataDir = baseDataDir + headerFilesDirElem.attrib['value']
				else:
					raise ConfigError('')

				newHeaderFilesDir = elementRoot.find('computed-header-files-dir')
				if newHeaderFilesDir is not None:
					self._computedDataDir = baseDataDir + newHeaderFilesDir.attrib['value']
				else:
					raise ConfigError('')
			else:
				raise ConfigError('')

			# Now Check to see if directories exist and create them if they do not.
			# We will only do it if the bade directory exists though.
			if os.path.exists(baseDataDir):

				if not os.path.exists(self._rawDataDir):
					os.mkdir(self._rawDataDir)

				if not os.path.exists(self._headerDataDir):
					os.mkdir(self._headerDataDir)

				if not os.path.exists(self._computedDataDir):
					os.mkdir(self._computedDataDir)

		except ConfigError as cfe:
			print(argstr)
			exit()

	def __processLogSettings(self, elementRoot):

		argstr = """
	Config file requires a <log-settings> element as follows:
	
			<log-settings>
			<base-dir value="/home/username" />
			<missing-data-csv value="WrongData.csv" />
		</log-settings>
			"""

		try:
			if elementRoot is not None:
				baseDirElem = elementRoot.find("base-dir")
				if baseDirElem is not None:
					baseDir = baseDirElem.attrib['value']
				else:
					raise ConfigError('')

				missingFileElem = elementRoot.find("missing-data-csv")
				if missingFileElem is not None:
					missingFile = missingFileElem.attrib['value']
				else:
					raise ConfigError('')

				self._missingDataFile = baseDir + '/' + missingFile
			else:
				raise ConfigError("")

		except ConfigError as cfe:
			print(argstr)
			exit()

	def __procesProcessingSettings(self, elementRoot):

		argstr = """
   	Config file requires a <processing-settings> element as follows:

		    <processing-settings>
			<batch-size value="100" />
			<num-threads value="7" />
			<use-gpu value="False" />
		</processing-settings>
			    """
		try:
			dir_path = os.getcwd()
			if elementRoot is not None:
				batchElem = elementRoot.find('batch-size')
				if batchElem is not None:
					self._batchSize = int(batchElem.attrib['value'])
				else:
					raise ConfigError("")

				threadElem = elementRoot.find('num-threads')
				if threadElem is not None:
					self._numThreads = int(threadElem.attrib['value'])
				else:
					raise ConfigError("")

				gpuElem = elementRoot.find('use-gpu')
				if gpuElem is not None:
					if self._initLib :
						gpuYesNo = gpuElem.attrib['value']
						if gpuYesNo.lower() == 'true':
							self.__initACCLib(dir_path)
						else:
							self.__initLib(dir_path)
				else:
					raise ConfigError("")
			else:
				raise ConfigError("")

		except ConfigError as cfe:
			print(argstr)
			exit()

		except Exception as e:
			traceback.print_exc()
			exit()  # Need to uncomment this when running production

	def __processDRMSConfig(self, elementRoot):
		argstr = """
   	Config file requires a <drms-settings> element as follows:

		    <drms-settings>
			<client-name value="user@example.com"/>
		</drms-settings>
			    """
		try:
			if elementRoot is not None:
				clientElem = elementRoot.find('client-name')
				if clientElem is not None:
					self._drmsClientName = clientElem.attrib['value']
				else:
					raise ConfigError("")
			else:
				raise ConfigError("")

		except ConfigError as cfe:
			print(argstr)
			exit()

	def __initLib(self, baseDir):

		self._lib = ct.cdll.LoadLibrary(baseDir + '/lib/bin/libmvts4swa1.so')
		self._lib.init_lib(None)

		self._lib.computeSolarFeatures.argtypes = [ ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
				ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), 
				ct.POINTER(ct.c_double), ct.c_double, ct.c_double, ct.c_double, 
				ct.c_double, ct.c_double, ct.c_double, ct.c_double, ct.c_double, ct.c_double,
				ct.c_uint, ct.c_uint, ct.POINTER(ct.c_double)]
		#print("Done config.")

	def __initACCLib(self, baseDir):
		
		self._lib = ct.cdll.LoadLibrary(baseDir + '/lib/bin/libaccmvts4swa1.so')
		self._lib.init_lib(None)

		self._lib.computeSolarFeatures.argtypes = [ ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
				ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), 
				ct.POINTER(ct.c_double), ct.c_double, ct.c_double, ct.c_double, 
				ct.c_double, ct.c_double, ct.c_double, ct.c_double, ct.c_double, ct.c_double,
				ct.c_uint, ct.c_uint, ct.POINTER(ct.c_double)]
		#print("Done config acc.")

class ConfigError(Exception):

	def __init__(self, value):
		self.value = value

	def __str__(self):
		return repr(self.value)
