import math
import numpy as np


from .fits import read as  fits_read_file
from .fits import get_correctly_scaled_data

from sunpy.io.header import FileHeader
from sunpy.util.metadata import MetaDict
from sunpy.map.mapbase import GenericMap

import asyncio
import traceback


#radsindeg = np.pi / 180.

# Exception added by sunitha as the original file did not have it.
class FileNotFoundError(Exception):

	def __init__(self, value):
		self.value = value

	def __str__(self):
		return repr(self.value)



async def getMap(fileName, idx):
	import traceback
	try:

		# File gets read here.
		pairs = await fits_read_file(fileName)

		new_pairs = []
		for pair in pairs:
			filedata, filemeta = pair
			assert isinstance(filemeta, FileHeader)
			# This tests that the data is more than 1D
			if len(np.shape(filedata)) > 1:
				data = filedata
				meta = MetaDict(filemeta)
				new_pairs.append((data, meta))

		if len(new_pairs) > 0:
			data, header = new_pairs[0]
			meta = MetaDict(header)

			mapData = GenericMap(data, meta)

	except Exception as e:
		# traceback.print_exc()
		raise FileNotFoundError("Could not open the fits file in get Map %s" % fileName)

	return [mapData, idx ]


async def getScaledMap(fileName, idx):
	import traceback
	try:
		mapFile, idx = await getMap(fileName, idx)

		# this will rarely if ever change anything, it is just a check
		await get_correctly_scaled_data(fileName, object = mapFile, scale_type = 'int16')
	except Exception as e:
		traceback.print_exc()
		raise FileNotFoundError("Could not open the fits file in get Scaled %s" % fileName)

	return [mapFile, idx]


async def get_data(file_bz, file_by, file_bx, file_mask, file_bitmask, file_los):
	"""function: get_data

	This function reads the appropriate data and metadata.
	"""

	tasks = [getMap(file_bz, 0), getMap(file_by, 1), getMap(file_bx, 2), getScaledMap(file_mask, 3), getScaledMap(file_bitmask, 4), getMap(file_los, 5)]

	results = await asyncio.gather(*tasks)

	outData = [0] * 9
	for pair in results:
		mapFile = pair[0]
		idx = pair[1]
		outData[idx] = mapFile

	# get metadata
	header = outData[0].meta

	# get dimensions
	nx = outData[0].data.shape[1]
	ny = outData[0].data.shape[0]

	# flip the sign of by
	outData[1].data[:, :] *= -1.0

	outData[6] = nx
	outData[7] = ny
	outData[8] = header

	return outData  


async def load_file_data(file_bz, header_df, ts_index_map) :

	split_str = file_bz.split('.')
	prefix = '.'.join(split_str[:-2])
	curr_timestamp = split_str[3].replace('_TAI', '')

	file_by = prefix + '.Bt.fits'
	file_bx = prefix + '.Bp.fits'
	file_mask = prefix + '.conf_disambig.fits'
	file_bitmask = prefix + '.bitmap.fits'
	file_los = prefix + '.magnetogram.fits'
	# get the data
	try:
		data = await get_data(file_bz, file_by, file_bx, file_mask, file_bitmask, file_los)
	except  Exception as e:
		print("Some files not found... going to the next timestamp. Missing file: %s" % e)
		return tuple([None] * 1)

	bz, by, bx, mask, bitmask, los, nx, ny, header = data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8]
	
	try:
		# get from the header
		rsun_ref = header['RSUN_REF']
		cdelt1 = header['CDELT1']
		dsun_obs = header['DSUN_OBS']
		rsun_obs = header['RSUN_OBS']
		crval1 = header['CRVAL1']
		crval2 = header['CRVAL2']
		crln_obs = header['CRLN_OBS']
		crpix1 = header['CRPIX1']
		crpix2 = header['CRPIX2']
		latMin = header['LAT_MIN']
		lonMin = header['LON_MIN']
		latMax = header['LAT_MAX']
		lonMax = header['LON_MAX']

	except:
		header_i = ts_index_map[curr_timestamp]

		rsun_ref = header_df.iloc[header_i]['RSUN_REF']
		cdelt1 = header_df.iloc[header_i]['CDELT1']
		dsun_obs = header_df.iloc[header_i]['DSUN_OBS']
		rsun_obs = header_df.iloc[header_i]['RSUN_OBS']
		crval1 = header_df.iloc[header_i]['CRVAL1']
		crval2 = header_df.iloc[header_i]['CRVAL2']
		crln_obs = header_df.iloc[header_i]['CRLN_OBS']
		crpix1 = header_df.iloc[header_i]['CRPIX1']
		crpix2 = header_df.iloc[header_i]['CRPIX2']
		latMin = header_df.iloc[header_i]['LAT_MIN']
		lonMin = header_df.iloc[header_i]['LON_MIN']
		latMax = header_df.iloc[header_i]['LAT_MAX']
		lonMax = header_df.iloc[header_i]['LON_MAX']
		

	# convert cdelt1 from degrees to arcsec
	#cdelt1 = (math.atan((rsun_ref * cdelt1 * radsindeg) / (dsun_obs))) * (1 / radsindeg) * (3600.)

	bx_arr = np.asarray(bx.data, dtype = float).flatten()
	by_arr = np.asarray(by.data, dtype = float).flatten()
	bz_arr = np.asarray(bz.data, dtype = float).flatten()
	mask_arr = np.asarray(mask.data, dtype = float).flatten()
	bitmask_arr = np.asarray(bitmask.data, dtype = float).flatten()
	los_arr = np.asarray(los.data, dtype = float).flatten()

	
												
	returnArr = [bx_arr, by_arr, bz_arr, mask_arr, bitmask_arr, los_arr, cdelt1, 
				crpix1, crpix2, crval1, crval2, rsun_ref, rsun_obs, dsun_obs, crln_obs,
				latMin, lonMin, latMax, lonMax, nx, ny]
	
	return returnArr
