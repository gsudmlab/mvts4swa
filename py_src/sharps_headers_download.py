import os
import drms
import traceback
import pandas as pd

from _datetime import datetime


class SharpsHeaderHelper():

	def __init__(self, dataDirectory):
		self._dataDirectory = dataDirectory

	def __downloadFirstKeys(self, sharpNum):

		try:
			c = drms.Client(verbose = False)

			c.info('hmi.sharp_cea_720s')
			q = "hmi.sharp_cea_720s[%s][]" % (sharpNum)
			
			success = False
			while success == False:

				try:
					k = c.query(q, key = '**ALL**', n = 1)
				except urllib.error.URLError:
					print('\t urlib query error.. retrying..')
					time.sleep(30)
				except:
					print('\t other query error.. retrying..')
					time.sleep(30)
				else:
					success = True
	
			if not k is None and k.shape[0] > 0:
				currTime = datetime.strptime(k.loc[0, 'DATE'], "%Y-%m-%dT%H:%M:%SZ")
				return currTime
	
			return None
		except Exception:
			return None

	def __downloadKeys(self, sharpNum):

		fname = self._dataDirectory + '/' + sharpNum + '.txt'
		try:
			c = drms.Client(verbose = False)

			c.info('hmi.sharp_cea_720s')
			q = "hmi.sharp_cea_720s[%s][]" % (sharpNum)

			success = False
			while success == False:

				try:
					k = c.query(q, key = '**ALL**')
				except urllib.error.URLError:
					print('\t urlib query error.. retrying..')
					time.sleep(30)
				except:
					print('\t other query error.. retrying..')
					time.sleep(30)
				else:
					success = True

			if 'CODEVER7' in k.columns:
				k['CODEVER7'] = k['CODEVER7'].str.replace('\n', '\\n')

			k.to_csv(fname, sep = '\t', index = False)
			return True
		except Exception:
			return False

	def getProcessDate(self, sharpNum, redownload = False):

		sharp_header_file = self._dataDirectory + '/' + sharpNum + ".txt"

		if redownload or not os.path.isfile(sharp_header_file):
			if os.path.isfile(sharp_header_file):
				os.remove(sharp_header_file)

			if not self.__downloadKeys(sharpNum):
				return None
		else:
			processTime = self.__downloadFirstKeys(sharpNum)
			if processTime is not None and os.path.isfile(sharp_header_file):
				fileTime = datetime.fromtimestamp(os.path.getmtime(sharp_header_file))
				if fileTime < processTime:
					os.remove(sharp_header_file)
					if not self.__downloadKeys(sharpNum):
						return None
			elif processTime is not None:
				if not self.__downloadKeys(sharpNum):
					return None

		try:
			header_df = pd.read_csv(sharp_header_file, sep = '\t')
			maxTime = datetime.strptime(header_df.loc[0, 'DATE'], "%Y-%m-%dT%H:%M:%SZ")
			
			for row in header_df.itertuples():
				compareTime = datetime.strptime(row[1], "%Y-%m-%dT%H:%M:%SZ")
				tDelta = maxTime - compareTime
				if tDelta.total_seconds() < 0 :
					maxTime = compareTime

			return maxTime
		except Exception as e:
			#traceback.print_exc()
			return None
