import os
import traceback
import pandas as pd

from _datetime import datetime


class SoarFeatureVerifyHelper:

	def __init__(self, computedDataDir, headerDataDir):
		self._computedDataDir = computedDataDir
		self._headerDataDir = headerDataDir

	def __readHeaders(self, sharpNum):

		sharp_header_file = self._headerDataDir + '/' + sharpNum + ".txt"
		computed_values_file = self._computedDataDir + '/' + sharpNum + ".csv"
		if os.path.isfile(sharp_header_file) and os.path.isfile(computed_values_file):
			header_df = pd.read_csv(sharp_header_file, sep='\t')
			computed_df = pd.read_csv(computed_values_file, sep='\t')
			return [computed_df, header_df]

		return None

	def test_for_wrong_data(self, sharpNum):

		times_wrong = []

		try:
			files = self.__readHeaders(sharpNum)
			if not files is None:
				header_df = files[1]
				new_header_df = files[0]

				if not header_df.empty:
					# if the lengths don't match then it clearly is the old data
					# and needs to be reprocessed.
					if new_header_df.shape[0] != header_df.shape[0]:
						times_wrong = ["All 1"]
					else:
						# print("NewHeader (%s, %s)" %new_header_df.shape)
						# print("JsocHeader (%s, %s)" %header_df.shape)
						# isWrong = False
						for i in range(0, header_df.shape[0]):
							if abs(new_header_df.iloc[i]['TOTUSJH'] - header_df.iloc[i]['TOTUSJH']) > abs(header_df.iloc[i]['TOTUSJH'] * 0.01) and abs(header_df.iloc[i]['TOTUSJH']) > 10e-4:
								times_wrong.append(header_df.iloc[i]['T_REC'])
							elif abs(new_header_df.iloc[i]['MEANGBZ'] - header_df.iloc[i]['MEANGBZ']) > abs(header_df.iloc[i]['MEANGBZ'] * 0.01) and abs(header_df.iloc[i]['MEANGBZ']) > 10e-4:
								times_wrong.append(header_df.iloc[i]['T_REC'])
							elif abs(new_header_df.iloc[i]['MEANGBH'] - header_df.iloc[i]['MEANGBH']) > abs(header_df.iloc[i]['MEANGBH'] * 0.01) and abs(header_df.iloc[i]['MEANGBH']) > 10e-4:
								times_wrong.append(header_df.iloc[i]['T_REC'])
							elif abs(new_header_df.iloc[i]['MEANGAM'] - header_df.iloc[i]['MEANGAM']) > abs(header_df.iloc[i]['MEANGAM'] * 0.01) and abs(header_df.iloc[i]['MEANGAM']) > 10e-4:
								times_wrong.append(header_df.iloc[i]['T_REC'])
							#elif abs(new_header_df.iloc[i]['R_VALUE'] - header_df.iloc[i]['R_VALUE']) > abs(header_df.iloc[i]['R_VALUE'] * 0.01) and abs(header_df.iloc[i]['R_VALUE']) > 10e-4:
								#print("R_VALUE off: new {}, old {}".format(new_header_df.iloc[i]['R_VALUE'], header_df.iloc[i]['R_VALUE']))

						if header_df.shape[0] == len(times_wrong):
							times_wrong = ["All 2"]
			else:
				# Indicate that we have a missing file
				print("Missing Header")
				times_wrong = None

		except Exception as e:
			traceback.print_exc()
			times_wrong = ["All 2"]
			return times_wrong

		return times_wrong
