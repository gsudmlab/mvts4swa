import os
import asyncio
import fnmatch

import ctypes as ct
import numpy as np
import pandas as pd

import scipy.misc

from _datetime import datetime
import py_src.solar_comp_utils as scu

NUM_SOLAR_FEATURES_PER_TS = 61
NON_COMPUTED_SOLAR_FEATURES = 6

returnedHeader = ['Timestamp', 'USFLUX', 'MEANGAM', 'MEANGBH', 'EPSX', 'EPSY', 'EPSZ', 'TOTBSQ', 'TOTFX', 'TOTFY',
				'TOTFZ', 'MEANGBT', 'MEANGBZ', 'TOTUSJZ', 'MEANJZD', 'MEANALP', 'MEANJZH', 'TOTUSJH',
				'ABSNJZH', 'SAVNCPP', 'TOTPOT', 'MEANPOT', 'SHRGT45', 'MEANSHR', 'R_VALUE', 'RBZ_VALUE',
				'RBT_VALUE', 'RBP_VALUE', 'FDIM', 'BZ_FDIM', 'BT_FDIM', 'BP_FDIM', 'PIR_USFLUX', 'PIR_MEANGAM', 
				'PIR_MEANGBH', 'PIR_EPSX', 'PIR_EPSY', 'PIR_EPSZ', 'PIR_TOTBSQ', 'PIR_TOTFX', 'PIR_TOTFY',
				'PIR_TOTFZ', 'PIR_MEANGBT', 'PIR_MEANGBZ', 'PIR_TOTUSJZ', 'PIR_MEANJZD', 'PIR_MEANALP', 
				'PIR_MEANJZH', 'PIR_TOTUSJH', 'PIR_ABSNJZH', 'PIR_SAVNCPP', 'PIR_TOTPOT', 'PIR_MEANPOT', 
				'PIR_SHRGT45', 'PIR_MEANSHR', 'PIL_LEN', 'CRVAL1', 'CRLN_OBS', 'LAT_MIN', 'LON_MIN', 'LAT_MAX', 'LON_MAX']


class SharpsRawDataProcessingHelper():

	def __init__(self, dataDirectory, headerDataDir, batchSize, lib):
		self._dataDirectory = dataDirectory
		self._headerDataDir = headerDataDir
		self._batchSize = batchSize
		self._lib = lib

	def __find_files(self, path, path_filter):

		ret_list = []
		for entry in os.scandir(path=path):
			if fnmatch.fnmatch(entry.name, path_filter):
				ret_list.append(entry.name)
		return ret_list

	def __generate_solar_features(self, bx_arr, by_arr, bz_arr, mask_arr, bitmask_arr, los_arr, cdelt1, crpix1,
								crpix2, crval1, crval2, rsun_ref, rsun_obs, dsun_obs, crln_obs, nx, ny) :
		out_solar_features = np.zeros((NUM_SOLAR_FEATURES_PER_TS - NON_COMPUTED_SOLAR_FEATURES), float)

		self._lib.computeSolarFeatures(bx_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
			by_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
			bz_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
			mask_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
			bitmask_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
			los_arr.ctypes.data_as(ct.POINTER(ct.c_double)),
			ct.c_double(cdelt1),
			ct.c_double(crpix1), ct.c_double(crpix2), ct.c_double(crval1), ct.c_double(crval2),
			ct.c_double(rsun_ref), ct.c_double(rsun_obs), ct.c_double(dsun_obs),
			ct.c_double(crln_obs), ct.c_uint(nx), ct.c_uint(ny),
			out_solar_features.ctypes.data_as(ct.POINTER(ct.c_double)))

		return out_solar_features

	async def __genAsync(self, bz_file_path, header_df, ts_index_map, ts_param):

		try:
			split_str = bz_file_path.split('.')
			prefix = '.'.join(split_str[:-2])
			curr_timestamp = split_str[3].replace('_TAI', '')
			#print ("Generating solar features for part ", curr_timestamp)
	
			if curr_timestamp in ts_index_map:
				# Load the raw file data for current processing step
				retValues = await scu.load_file_data(bz_file_path, header_df, ts_index_map)
				# If failed, the number of returned values is different
				if(len(retValues) == 21):
	
					idx = ts_index_map[curr_timestamp]
	
					[bx_arr, by_arr, bz_arr, mask_arr, bitmask_arr, los_arr, cdelt1,
					crpix1, crpix2, crval1, crval2, rsun_ref, rsun_obs, dsun_obs, crln_obs,
					latMin, lonMin, latMax, lonMax, nx, ny] = retValues
	
					ts_param[idx, -NON_COMPUTED_SOLAR_FEATURES:] = [crval1, crln_obs, latMin, lonMin, latMax, lonMax]
	
					c_features = self.__generate_solar_features(bx_arr, by_arr, bz_arr, mask_arr, bitmask_arr,
							los_arr, cdelt1, crpix1, crpix2, crval1, crval2, rsun_ref,
							rsun_obs, dsun_obs, crln_obs, nx, ny)
	
					for feat_num in range(0, len(c_features)):
						ts_param[idx][feat_num] = c_features[feat_num]
	
				else:
					# Failed to read so set to NAN
					idx = ts_index_map[curr_timestamp]
					ts_param[idx, :] = np.repeat("NaN", NUM_SOLAR_FEATURES_PER_TS)
					print("Ts not in raw data: ", curr_timestamp)
		except Exception as e:
			# Return None so that we do not have invalid output files
			traceback.print_exc()
		
	def __get_ts_index_map(self, header_df):
		ts_map = dict()
		for index, row in header_df.iterrows():
			key_val = row['T_REC']
			key_val = key_val.replace(".", "").replace(":", "").replace("_TAI", "")
			if key_val not in ts_map:
				ts_map[key_val] = index
			else:
				raise Exception("Duplicate timestamp %s found in header file." % key_val)

		return ts_map

	def process_sharp_num(self, sharpNum):
		'''
			Returns a dataframe with the columns in this order.
			['Timestamp', 'USFLUX', 'MEANGAM', 'MEANGBH', 'EPSX', 'EPSY', 'EPSZ', 'TOTBSQ', 'TOTFX', 'TOTFY',
				'TOTFZ', 'MEANGBT', 'MEANGBZ', 'TOTUSJZ', 'MEANJZD', 'MEANALP', 'MEANJZH', 'TOTUSJH',
				'ABSNJZH', 'SAVNCPP', 'TOTPOT', 'MEANPOT', 'SHRGT45', 'MEANSHR', 'R_VALUE', 'RBZ_VALUE',
				'RBT_VALUE', 'RBP_VALUE', 'FDIM', 'BZ_FDIM', 'BT_FDIM', 'BP_FDIM', 'PIR_USFLUX', 'PIR_MEANGAM', 
				'PIR_MEANGBH', 'PIR_EPSX', 'PIR_EPSY', 'PIR_EPSZ', 'PIR_TOTBSQ', 'PIR_TOTFX', 'PIR_TOTFY',
				'PIR_TOTFZ', 'PIR_MEANGBT', 'PIR_MEANGBZ', 'PIR_TOTUSJZ', 'PIR_MEANJZD', 'PIR_MEANALP', 
				'PIR_MEANJZH', 'PIR_TOTUSJH', 'PIR_ABSNJZH', 'PIR_SAVNCPP', 'PIR_TOTPOT', 'PIR_MEANPOT', 
				'PIR_SHRGT45', 'PIR_MEANSHR', 'CRVAL1', 'CRLN_OBS', 'LAT_MIN', 'LON_MIN', 'LAT_MAX', 'LON_MAX']
		'''
		#print("Process Sharp: "+str(sharpNum))
		raw_sharp_dir = self._dataDirectory + '/' + sharpNum
		sharp_header_file = self._headerDataDir + '/' + sharpNum + ".txt"

		import traceback

		try :

			files_bz = sorted(self.__find_files(raw_sharp_dir + "/", "*Br.fits"))
			num_fits = len(files_bz)
			#print("Num Fits: "+str(num_fits))

			if num_fits < 1:
				print ("No .fits files to process in folder: ", raw_sharp_dir)
				return None

			# Load header files
			header_df = pd.read_csv(sharp_header_file, sep='\t')
			ts_index_map = self.__get_ts_index_map(header_df)

			num_steps = len(ts_index_map)

			dim_nd_array = (num_steps, NUM_SOLAR_FEATURES_PER_TS)
			ts_params = np.empty(dim_nd_array)
			loop = asyncio.new_event_loop()
			asyncio.set_event_loop(loop)

			chunk_size = self._batchSize
			#print("Chunks: "+str(int(num_fits / chunk_size)))
			for chunk_num in range(0, int(num_fits / chunk_size) + 1):
				#print("Chunk Num: "+str(chunk_num))
				chunk_start = chunk_num * chunk_size
				if chunk_start + chunk_size < num_fits:
					futures = [	self.__genAsync(raw_sharp_dir + "/" + files_bz[x + chunk_start], header_df, ts_index_map, ts_params) for x in  range (0, chunk_size)]
					loop.run_until_complete(asyncio.gather(*futures))
				else:
					futures = [	self.__genAsync(raw_sharp_dir + "/" + files_bz[x], header_df, ts_index_map, ts_params) for x in  range (chunk_start, num_fits)]
					loop.run_until_complete(asyncio.gather(*futures))

			loop.close()

			ts_list = [None] * num_steps
			ts_labels = [None] * num_steps
			for key, value in ts_index_map.items():
				ts_list[value] = datetime.strptime(key, "%Y%m%d_%H%M%S")
				ts_labels[value] = key

			param_df = pd.DataFrame(ts_params)
			ts_df = pd.DataFrame(ts_labels)
			df = pd.concat([ts_df, param_df], axis=1, ignore_index=True)

			df.set_index(pd.DatetimeIndex(ts_list))
			df.sort_index(inplace=True, kind='mergesort')
			df.columns = returnedHeader
			return df

		except Exception as e:
			# Return None so that we do not have invalid output files
			traceback.print_exc()
			return None
