import os
import time
import drms
import glob
import shutil
import fnmatch
import tarfile
import traceback
import urllib.request

from _datetime import datetime


class SharpsRawDataHelper():
	
	def __init__(self, dataDirectory, drmsClientName):
		self._dataDirectory = dataDirectory
		self._drmsClientName = drmsClientName
		
	def __downloadData(self, sharpNum):
		
		save_dir = self._dataDirectory + '/' + sharpNum
		attemptCount = 0
	
		hasComplete = False
		while attemptCount < 5 and not hasComplete:
			try:
				c = drms.Client(verbose=False)
			   
				q = "hmi.sharp_cea_720s[%s][]" % (sharpNum)
				
				print(q)
	
				k = c.query(q, key='HARPNUM, T_REC')
				print('  Total records found: ', len(k))
	
				tdelt = 0
				if(len(k) > 0):
	
					# now retrieve export data
					maxReq = 25000
	
					r = c.export(q + '{Br,Bt,Bp,Bitmap,Conf_disambig,magnetogram,Dopplergram,continuum,Bp_err,Bt_err}', method='ftp-tar', protocol='fits', n=maxReq, email=self._drmsClientName)
	
					print('Starting download...')
					time.sleep(10)
					
					tryCnt = 0
					done = False
					while tryCnt < 10 and not done:
	
						while (not r.has_finished()) and (not r.has_failed()) :
							r.wait(sleep=30, timeout=120)
	
						tarurl = r.urls['url'][0]
						filename = r.urls['filename'][0]
						save_loc = save_dir + '/' + filename
						print(tarurl)
						print(save_loc)
	
						try:
	
							# make request and set timeout for no data to 500 seconds and enable streaming
							t_out = 120
							with urllib.request.urlopen(tarurl, timeout=t_out) as resp:
								# Open output file and make sure we write in binary mode
								with open(save_loc + '.part', 'wb') as fh:
									# walk through the request response in chunks of 1MiB
									while True:
										chunk = resp.read(1024 * 1024)
										if not chunk:
											break
										# Write the chunk to the file
										fh.write(chunk)
	
							# Rename the file
							os.rename(save_loc + '.part', save_loc)
							
							done = True
						except Exception:
							print(traceback.format_exc())
							time.sleep(10)
							tryCnt = tryCnt + 1
	
				hasComplete = True
			except Exception as e:
				time.sleep(10)
				if isinstance(e, drms.DrmsExportError):
					hasComplete = self.__download_chunks(sharpNum, save_dir)
				elif isinstance(e, urllib.error.HTTPError):
					print("urlErr: " + str(e.code))
				else:
					#print(traceback.format_exc())
					
					attemptCount = attemptCount + 1
	
		return hasComplete
	
	def __download_chunks(self, sharpNum, save_dir):
		# Some file lists are large enought that they don't let us download all of them at once.
		# So, we just do several chunks.
		
		partSize = 100
		attemptCount = 0
		hasComplete = False
		while attemptCount < 5 and not hasComplete:
			try:
				c = drms.Client(verbose=False)
		
				q = "hmi.sharp_cea_720s[%s][]" % (sharpNum)
				print(q)
	
				k = c.query(q, key='HARPNUM, T_REC')
				print('  Total records found: ', len(k))
	
				tiaList = []
				for t in k.T_REC:
					tiaList.append(t)
	
				tdelt = 0
	
				if(len(k) > 0):
	
					numPartitions = len(tiaList) // partSize
	
					# Check to see how many parts were already downloaded since I screwed up once.
					files = []
					files.extend(glob.glob(save_dir + "/*.tar"))
					partStart = 0
					for fname in files:
						if(fname.endswith("tar")):
							partStart = partStart + 1
	
					for i in range(partStart, numPartitions + 1):
						tsTmp = ''
						if(i < numPartitions):
							for j in range(0, partSize):
								tsTmp = tsTmp + tiaList[j + i * partSize] + ', '
						else:
							j = i * partSize
							while (j < len(tiaList)):
								tsTmp = tsTmp + tiaList[j] + ', '
								j = j + 1
	
						q = "hmi.sharp_cea_720s[%s][%s]" % (sharpNum, tsTmp)
						# print(q)
	
						k = c.query(q, key='HARPNUM, T_REC')
						print('  Total records found: ', len(k))
						# now retrieve export data
						maxReq = 25000
	
						r = c.export(q + '{Br,Bt,Bp,Bitmap,Conf_disambig,magnetogram,Dopplergram,continuum,Bp_err,Bt_err}', method='ftp-tar', protocol='fits', n=maxReq, email=self._drmsClientName)
	
						print('Starting download...')
						time.sleep(10)
						
						tryCnt = 0
						done = False
						while tryCnt < 10 and not done:
	
							while (not r.has_finished()) and (not r.has_failed()) :
								r.wait(sleep=30, timeout=120)
	
							tarurl = r.urls['url'][0]
							filename = r.urls['filename'][0]
							save_loc = save_dir + '/' + filename
							print(tarurl)
							print(save_loc)
	
							try:
	
								# make request and set timeout for no data to 500 seconds and enable streaming
								t_out = 120
								with urllib.request.urlopen(tarurl, timeout=t_out) as resp:
									# Open output file and make sure we write in binary mode
									with open(save_loc + '.part', 'wb') as fh:
										# walk through the request response in chunks of 1MiB
										while True:
											chunk = resp.read(1024 * 1024)
											if not chunk:
												break
											# Write the chunk to the file
											fh.write(chunk)
	
								# Rename the file
								os.rename(save_loc + '.part', save_loc)

								done = True
							except Exception:
								print(traceback.format_exc())
								time.sleep(10)
								tryCnt = tryCnt + 1
	
				hasComplete = True
			except Exception:
				print(traceback.format_exc())
				time.sleep(10)
				attemptCount = attemptCount + 1
	
		return hasComplete
	
	def __check_for_tar(self, save_dir):
		files = []
		files.extend(glob.glob(save_dir + "/*.tar"))
	
		for fname in files:
			if(fname.endswith("tar")):
				try:
					tar = tarfile.open(fname, "r:")
					tar.extractall(save_dir)
					tar.close()
				except Exception:
					print(traceback.format_exc())
				os.remove(fname)
	
		types = ('*.txt', '*.html', '*.json', '*.drmsrun', '*.qsub', '*.tar.*')
		files = []
		for ftype in types:
			files.extend(glob.glob(save_dir + "/" + ftype))
	
		for f in files:
			os.remove(f)
		
	def isProcessDateBeforData(self, sharpNum, processDate):
		
		raw_dir = self._dataDirectory + '/' + sharpNum + '/'

		if os.path.exists(raw_dir):
			for entry in os.scandir(path=raw_dir):
				if fnmatch.fnmatch(entry.name, "*.fits"):
					fileTime = datetime.fromtimestamp(os.path.getmtime(raw_dir + entry.name))
					return processDate < fileTime
		
		return None
		
	def downloadData(self, sharpNum):
		
		fdir = self._dataDirectory + '/' + sharpNum + '/'
		if os.path.exists(fdir):
			shutil.rmtree(fdir)
		
		if not os.path.exists(fdir):
			os.mkdir(fdir)
			
		downloaded = self.__downloadData(sharpNum)
		
		if downloaded:
			self.__check_for_tar(fdir)
			return True
		
		return False
