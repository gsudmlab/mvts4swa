# Listing of the mvts4swa-project config file lines #

* [Go Back to README](README.MD)

###data-directories###
This set of values is for configuration of where data is and where to place the calculated data.
We must have each of the sub values within the __data-directories__ tags. Otherwise, your configuration will fail. They don't need to be in any particular order though.

	<data-directories>
		<base-dir value="/data/SHARPS" />
		<raw-files-dir value="/raw-sharps" />
		<header-files-dir value="/sharps-headers" />
		<computed-header-files-dir value="/new-data" />
	</data-directories>

###base-dir###

	<base-dir value="/data/SHARPS" />
	
The calculation program assumes that all the data directories share a parent directory. This value is attached to beginning of each of the other data directories to create the full directory path. It is required that this directory exists. All of the sub-directories shall be created as part of the processing procedure if they do not already exist.

###raw-files-dir###

	<raw-files-dir value="/raw-sharps" />
	
This is the sub-directory that contains all of the SHARP sub-directories that contain the raw fits files. It is assumed that each SHARP is in a directory that has the name of its number.  For example, SHARP number 16 would be in a directory named 16.  

###header-files-dir###

	<header-files-dir value="/sharps-headers" />
	
The JSOC web interface provides a number of key-word values that correspond with the observations in each SHARP series.  This is the sub-directory that they will be stored in, or have already been stored in. Each .txt file contains these values for the entire series for which they are named. For example 16.txt would contain all these values for SHARP number 16.

###computed-header-files-dir###

	<computed-header-files-dir value="/new-data" />
	
This is the sub-directory that contains the results of the computations done by this project.  The output is similar to the header files, but they contain only the values that have been determined to be of interest for this project.  The files are also named with a .csv instead of the .txt of the headers mentioned above.  

	