
RM := rm -rf

alib = libmvts4swa1.so
acclib = libaccmvts4swa1.so

openmpDir = ./openmp
cudaDir = ./cuda
libDir = ./lib



makemp: fromDir = $(openmpDir) 
makemp:
	$(MAKE) -C ./openmp all 
	$(MAKE) copy_lib_mp
	

makecuda:
	$(MAKE) -C ./cuda all
	$(MAKE) copy_lib_cuda
	
.PHONY:clean
clean:
	$(MAKE) -C ./openmp clean
	$(MAKE) -C ./cuda clean
	-$(RM) $(libDir)
	

copy_lib_mp:
	mkdir -p $(libDir)/bin
	cp $(openmpDir)/$(alib) $(libDir)/bin/$(alib)
	
copy_lib_cuda:
	mkdir -p $(libDir)/bin
	cp $(cudaDir)/$(acclib) $(libDir)/bin/$(acclib)