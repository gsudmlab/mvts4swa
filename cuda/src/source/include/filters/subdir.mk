
# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/source/include/filters/IImageFilter.cpp \


OBJS += \
./src/source/include/filters/IImageFilter.o \


CPP_DEPS += \
./src/source/include/filters/IImageFilter.d \


# Each subdirectory must supply rules for building sources it contributes
src/source/include/filters/%.o: ../src/source/include/filters/%.cpp
	@echo 'Building file: $<'
	@echo 'PGC++ Compiler'
	pgc++ -acc -O2 -ta=tesla:nordc -Mlarge_arrays -Kieee -std=c++11 -c -fPIC -MMD "$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '