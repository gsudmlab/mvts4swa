
# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/source/openacc/SolarFeatureCalc_acc.cpp 

OBJS += \
./src/source/openacc/SolarFeatureCalc_acc.o 

CPP_DEPS += \
./src/source/openacc/SolarFeatureCalc_acc.d 


# Each subdirectory must supply rules for building sources it contributes
src/source/openacc/%.o: ../src/source/openacc/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: PGC++ Compiler'
	pgc++ -acc -O2 -ta=tesla:nordc -Mlarge_arrays -std=c++11 -c -fPIC -MMD "$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

#-fast
