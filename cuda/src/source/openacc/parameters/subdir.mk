
# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/source/openacc/parameters/AbsoluteFluxCalculator_acc.cpp \
../src/source/openacc/parameters/GreenPotCalculator_acc.cpp \
../src/source/openacc/parameters/GammaCalculator_acc.cpp \
../src/source/openacc/parameters/B_DerivativeCalculator_acc.cpp \
../src/source/openacc/parameters/LorentzCalculator_acc.cpp \
../src/source/openacc/parameters/BH_DerivativeCalculator_acc.cpp \
../src/source/openacc/parameters/BZ_DerivativeCalculator_acc.cpp \
../src/source/openacc/parameters/JZ_Calculator_acc.cpp \
../src/source/openacc/parameters/R_ValueCalculator_acc.cpp \
../src/source/openacc/parameters/FractalDimCalculator_acc.cpp \
../src/source/openacc/parameters/PIR_AbsoluteFluxCalculator_acc.cpp \
../src/source/openacc/parameters/PIR_GreenPotCalculator_acc.cpp \
../src/source/openacc/parameters/PIR_GammaCalculator_acc.cpp \
../src/source/openacc/parameters/PIR_B_DerivativeCalculator_acc.cpp \
../src/source/openacc/parameters/PIR_LorentzCalculator_acc.cpp \
../src/source/openacc/parameters/PIR_BH_DerivativeCalculator_acc.cpp \
../src/source/openacc/parameters/PIR_BZ_DerivativeCalculator_acc.cpp \
../src/source/openacc/parameters/PIR_JZ_Calculator_acc.cpp \

OBJS += \
./src/source/openacc/parameters/AbsoluteFluxCalculator_acc.o \
./src/source/openacc/parameters/GreenPotCalculator_acc.o \
./src/source/openacc/parameters/GammaCalculator_acc.o \
./src/source/openacc/parameters/B_DerivativeCalculator_acc.o \
./src/source/openacc/parameters/LorentzCalculator_acc.o \
./src/source/openacc/parameters/BH_DerivativeCalculator_acc.o \
./src/source/openacc/parameters/BZ_DerivativeCalculator_acc.o \
./src/source/openacc/parameters/JZ_Calculator_acc.o \
./src/source/openacc/parameters/R_ValueCalculator_acc.o \
./src/source/openacc/parameters/FractalDimCalculator_acc.o \
./src/source/openacc/parameters/PIR_AbsoluteFluxCalculator_acc.o \
./src/source/openacc/parameters/PIR_GreenPotCalculator_acc.o \
./src/source/openacc/parameters/PIR_GammaCalculator_acc.o \
./src/source/openacc/parameters/PIR_B_DerivativeCalculator_acc.o \
./src/source/openacc/parameters/PIR_LorentzCalculator_acc.o \
./src/source/openacc/parameters/PIR_BH_DerivativeCalculator_acc.o \
./src/source/openacc/parameters/PIR_BZ_DerivativeCalculator_acc.o \
./src/source/openacc/parameters/PIR_JZ_Calculator_acc.o \

CPP_DEPS += \
./src/source/openacc/parameters/AbsoluteFluxCalculator_acc.d \
./src/source/openacc/parameters/GreenPotCalculator_acc.d \
./src/source/openacc/parameters/GammaCalculator_acc.d \
./src/source/openacc/parameters/B_DerivativeCalculator_acc.d \
./src/source/openacc/parameters/LorentzCalculator_acc.d \
./src/source/openacc/parameters/BH_DerivativeCalculator_acc.d \
./src/source/openacc/parameters/BZ_DerivativeCalculator_acc.d \
./src/source/openacc/parameters/JZ_Calculator_acc.d \
./src/source/openacc/parameters/R_ValueCalculator_acc.d \
./src/source/openacc/parameters/FractalDimCalculator_acc.d \
./src/source/openacc/parameters/PIR_AbsoluteFluxCalculator_acc.d \
./src/source/openacc/parameters/PIR_GreenPotCalculator_acc.d \
./src/source/openacc/parameters/PIR_GammaCalculator_acc.d \
./src/source/openacc/parameters/PIR_B_DerivativeCalculator_acc.d \
./src/source/openacc/parameters/PIR_LorentzCalculator_acc.d \
./src/source/openacc/parameters/PIR_BH_DerivativeCalculator_acc.d \
./src/source/openacc/parameters/PIR_BZ_DerivativeCalculator_acc.d \
./src/source/openacc/parameters/PIR_JZ_Calculator_acc.d \


# Each subdirectory must supply rules for building sources it contributes
src/source/openacc/parameters/%.o: ../src/source/openacc/parameters/%.cpp
	@echo 'Building file: $<'
	@echo 'PGC++ Compiler'
	pgc++ -acc -O2 -ta=tesla:nordc -Mlarge_arrays  -Kieee -std=c++11 -c -fPIC -MMD "$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

#-fast

