
# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/source/openacc/filters/AverageDownSampleFilter_acc.cpp \
../src/source/openacc/filters/GaussianSmoothingFilter_acc.cpp \
../src/source/openacc/filters/LanczosDownSampleFilter_acc.cpp \
../src/source/openacc/filters/BoxCarSmoothingFilter_acc.cpp \
../src/source/openacc/filters/PIL_Filter_acc.cpp \
../src/source/openacc/filters/PIL_Filter_W_Gauss_acc.cpp \


OBJS += \
./src/source/openacc/filters/AverageDownSampleFilter_acc.o \
./src/source/openacc/filters/GaussianSmoothingFilter_acc.o \
./src/source/openacc/filters/LanczosDownSampleFilter_acc.o \
./src/source/openacc/filters/BoxCarSmoothingFilter_acc.o \
./src/source/openacc/filters/PIL_Filter_acc.o \
./src/source/openacc/filters/PIL_Filter_W_Gauss_acc.o \


CPP_DEPS += \
./src/source/openacc/filters/AverageDownSampleFilter_acc.d \
./src/source/openacc/filters/GaussianSmoothingFilter_acc.d \
./src/source/openacc/filters/LanczosDownSampleFilter_acc.d \
./src/source/openacc/filters/BoxCarSmoothingFilter_acc.d \
./src/source/openacc/filters/PIL_Filter_acc.d \
./src/source/openacc/filters/PIL_Filter_W_Gauss_acc.d \


# Each subdirectory must supply rules for building sources it contributes
src/source/openacc/filters/%.o: ../src/source/openacc/filters/%.cpp
	@echo 'Building file: $<'
	@echo 'PGC++ Compiler'
	pgc++ -acc -O2 -ta=tesla:nordc -Mlarge_arrays -Kieee -std=c++11 -c -fPIC -MMD "$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

#-fast
