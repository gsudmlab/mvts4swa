
# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/source/openacc/transforms/LOSTransform_acc.cpp \


OBJS += \
./src/source/openacc/transforms/LOSTransform_acc.o \


CPP_DEPS += \
./src/source/openacc/transforms/LOSTransform_acc.d \



# Each subdirectory must supply rules for building sources it contributes
src/source/openacc/transforms/%.o: ../src/source/openacc/transforms/%.cpp
	@echo 'Building file: $<'
	@echo 'PGC++ Compiler'
	pgc++ -acc -O2 -ta=tesla:nordc -Mlarge_arrays -Kieee -std=c++11 -c -fPIC -MMD "$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

#-Minline