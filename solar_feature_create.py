import sys
import os
import csv

import pandas as pd

from _datetime import datetime

from py_src.solar_comp_config import SolarCompConfig
from py_src.sharps_headers_download import SharpsHeaderHelper
from py_src.raw_sharps_download import SharpsRawDataHelper
from py_src.solar_feature_comp import SharpsRawDataProcessingHelper
from py_src.solar_feature_verify import SoarFeatureVerifyHelper

import asyncio

from multiprocessing.pool import Pool
from concurrent.futures import process

outHeader = ['Timestamp', 
			'TOTUSJH', 'PIR_TOTUSJH', 
			'TOTBSQ', 'PIR_TOTBSQ', 
			'TOTPOT', 'PIR_TOTPOT', 
			'TOTUSJZ', 'PIR_TOTUSJZ', 
			'ABSNJZH', 'PIR_ABSNJZH', 
			'SAVNCPP', 'PIR_SAVNCPP', 
			'USFLUX', 'PIR_USFLUX',
			'TOTFZ', 'PIR_TOTFZ', 
			'MEANPOT', 'PIR_MEANPOT', 
			'EPSZ', 'PIR_EPSZ', 
			'SHRGT45', 'PIR_SHRGT45', 
			'MEANSHR', 'PIR_MEANSHR', 
			'MEANGAM', 'PIR_MEANGAM',
			'MEANGBT', 'PIR_MEANGBT', 
			'MEANGBZ', 'PIR_MEANGBZ', 
			'MEANGBH', 'PIR_MEANGBH', 
			'MEANJZH', 'PIR_MEANJZH', 
			'TOTFY', 'PIR_TOTFY', 
			'MEANJZD', 'PIR_MEANJZD', 
			'MEANALP', 'PIR_MEANALP', 
			'TOTFX', 'PIR_TOTFX', 
			'EPSY', 'PIR_EPSY', 
			'EPSX', 'PIR_EPSX', 
			'R_VALUE', 'RBZ_VALUE', 
			'RBT_VALUE', 'RBP_VALUE', 'FDIM', 'BZ_FDIM', 'BT_FDIM', 'BP_FDIM','CRVAL1', 
			'PIL_LEN', 'CRLN_OBS', 'LAT_MIN', 'LON_MIN', 'LAT_MAX', 'LON_MAX']

def process_sharp_num_forced(sharpNum):
	process_sharp_num(sharpNum, forceRun = True)
	return None
	
def process_sharp_num(sharpNum, isRecursiveCall = False, forceRun = False):
	maxAttempts = 5

	processTime = headerHelper.getProcessDate(sharpNum)
	#print(processTime)
	# If it failed to get a process time from the header, try to download
	# the header over again.
	numAttempts = 0
	while processTime is None and numAttempts < maxAttempts:
		processTime = headerHelper.getProcessDate(sharpNum)
		numAttempts = numAttempts + 1

	# If it is none, we have nothing to process, I guess
	if not processTime is None:
		
		isBefore = rawDataHelper.isProcessDateBeforData(sharpNum, processTime)
		#print("Is Before: "+str(isBefore))
		# If process was after the age of the current data, then we need to download
		if isBefore is None or not isBefore:
			downloaded = False
			numAttempts = 0
			while not downloaded and numAttempts < maxAttempts:
				print("Downloading SHARP# %s" % sharpNum)
				downloaded = rawDataHelper.downloadData(sharpNum)
				numAttempts = numAttempts + 1

			isBefore = rawDataHelper.isProcessDateBeforData(sharpNum, processTime)

			# Something broke and we don't have data to process at this time
			# Maybe we should do some log here.
			if (isBefore is None or not isBefore) and not downloaded:
				# Failed Download, nothing more we can do, return false to indicate
				# that things didn't work out.
				return False
			else:
				# Process the data
				comp_and_save(sharpNum)
		elif(forceRun):
			#if we are forcing the calculation then just process
			comp_and_save(sharpNum)
		else:
		# If process time was before, make sure data is correct and re-process if not
			wrongTimes = verifyHelper.test_for_wrong_data(sharpNum)
			
			#print("SharpNum %s has %s wrong times." %(sharpNum, len(wrongTimes)))
			if wrongTimes is None:
				# We are missing data of one file or another. Since we have the isBefore
				# That indicates we don't have the calculated values file. So, try to calculate.
				comp_and_save(sharpNum)
			elif len(wrongTimes) == 0:
				# Everything is fine, return a true to indicate that
				return True
			elif wrongTimes[0] == "All 1":
				# The header and the calculated values aren't the same shape.
				# Probably due to a new header and old calculated values.
				# Recalculate and check again.
				print("SharpNum %s has %s wrong times." % (sharpNum, wrongTimes))
				comp_and_save(sharpNum)
			elif wrongTimes[0] == "All 2":
				# Every time step is incorrect. This is also probably due
				# to a new header and an old calculated values file.
				# Try to recalculate and see where it gets us.
				print("SharpNum %s has %s wrong times." % (sharpNum, wrongTimes))
				comp_and_save(sharpNum)
			else:
				# Some number of time steps are incorrect.
				print("SharpNum %s has %s wrong times." % (sharpNum, len(wrongTimes)))
				comp_and_save(sharpNum)

		wrongTimes = verifyHelper.test_for_wrong_data(sharpNum)
		if len(wrongTimes) > 0 and not isRecursiveCall:
			# If first time through, try again
			process_sharp_num(sharpNum, isRecursiveCall = True)
			return None
		else:
			# Time to give up
			return wrongTimes
	else:
		# We had nothing to process, so we return None
		return None


def comp_and_save(sharpNum):

	processedData = compDataHelper.process_sharp_num(sharpNum)

	# At this point, we shouldn't have to check this, but something could
	# have caused a None to be returned, like the library not being loaded.
	if not processedData is None:

		out_filename = solarCompConfig.computedDataDir + '/' + sharpNum + '.csv'
		processedData = processedData[outHeader]
		with open(out_filename, 'w') as out_file:
			processedData.to_csv(out_file, sep = '\t', index = False, header = outHeader)

		print ("Output file written to: ", out_filename)
		return True
	else:
		print("Failed to get data.")
		return False

def init_worker():
	global solarCompConfig, compDataHelper
	#print("Init Worker Called.")
	dir_path = os.getcwd()
	solarCompConfig = SolarCompConfig(dir_path + "/config/mvts4swa-config.xml")
	compDataHelper = SharpsRawDataProcessingHelper(solarCompConfig.rawDataDir, solarCompConfig.headerDataDir, solarCompConfig.batchSize, solarCompConfig.lib)

def run_param_gen(startSharpNum, endSharpNum, numThreads):

	hdata = [str(x) for x in range(startSharpNum, endSharpNum)]
	with Pool(initializer=init_worker, processes = numThreads) as pool:
		res = pool.map(process_sharp_num, hdata, chunksize=1)
		pool.close()
		pool.join()
		
	
def run_param_gen_forced(startSharpNum, endSharpNum, numThreads):

	hdata = [str(x) for x in range(startSharpNum, endSharpNum)]
	with Pool(initializer=init_worker, processes = numThreads) as pool:
		res = pool.map(process_sharp_num_forced, hdata, chunksize=1)
		pool.close()
		pool.join()

def run_one(sharpNum):
	
	hdata = [sharpNum]
	with  Pool(initializer=init_worker, processes = 1) as pool:
		res = pool.apply(process_sharp_num, hdata)
		pool.close()
		pool.join()

def run_main():

	global headerHelper, verifyHelper, rawDataHelper

	if len(sys.argv) == 1:
		help()
		exit()

	dir_path = os.getcwd()
	solarCompConfig = SolarCompConfig(dir_path + "/config/mvts4swa-config.xml", False)
	headerHelper = SharpsHeaderHelper(solarCompConfig.headerDataDir)
	verifyHelper = SoarFeatureVerifyHelper(solarCompConfig.computedDataDir, solarCompConfig.headerDataDir)
	rawDataHelper = SharpsRawDataHelper(solarCompConfig.rawDataDir, solarCompConfig.drmsClientName)
	

	opt = int(sys.argv[1])
	if opt == 1:
		if len(sys.argv) < 4:
			print("Not enough arguments for option.")
			help()
			exit()
		startSn = int(sys.argv[2])
		endSn = int(sys.argv[3])
		run_param_gen(startSn, endSn, solarCompConfig.numThreads)
	elif opt == 2:
		if len(sys.argv) < 3:
			print("Not enough arguments for option.")
			help()
			exit()
		sn = sys.argv[2]
		run_one(sn)
	elif opt == 3:
		if len(sys.argv) < 4:
			print("Not enough arguments for option.")
			help()
			exit()
		startSn = int(sys.argv[2])
		endSn = int(sys.argv[3])
		run_param_gen_forced(startSn, endSn, solarCompConfig.numThreads)
	else:
		print('Invalid Mode')


def help():
    argstr = """
    python solar_feature.py [mode] [optional:args]
        
        1: [mode] [startSharpNum] [endSharpNum] checks range of sharps and runs those that need it
        2: [mode] [sharpnum] runs just one sharp
        3: [mode] [startSharpNum] [endSharpNum] forces run without check on range of sharps
    
    """
    print(argstr)


if __name__ == "__main__":
	run_main()
