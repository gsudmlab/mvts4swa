/*
 * File IParamCalculator.cpp
 * Author: Dustin Kempton, GSU DMLab
 *
 * Created on July 27, 2018
 */

#ifndef IPARAMCALCULATOR_CPP
#define IPARAMCALCULATOR_CPP

class IParamCalculator {
public:
	virtual ~IParamCalculator(){};

	virtual int calculateParameter(double* bx, double* by, double* bz,
			double* mask, double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) = 0;

};

#endif /* IPARAMCALCULATOR_CPP */
