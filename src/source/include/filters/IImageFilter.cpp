/*
 * IImageFilter.cpp
 *
 * Author: Dustin Kempton, GSU DMLab
 *
 * Created on September 17, 2018
 */

#ifndef OPENACC_IIMAGEFILTER_CPP
#define OPENACC_IIMAGEFILTER_CPP

class IImageFilter {
public:
	virtual ~IImageFilter(){};

	virtual int process(double* img, double* out_img, int nx, int ny)=0;

};

#endif /* OPENACC_IIMAGEFILTER_HPP */
