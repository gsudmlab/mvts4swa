/*
 * File: AbsoluteFluxCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 27, 2018
 */

#ifndef ABSOLUTEFLUX_ACC_CPP
#define ABSOLUTEFLUX_ACC_CPP

#include <math.h>
#include <stdlib.h>

using namespace std;
#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the USFLUX parameter.
 */
class AbsoluteFluxCalculator_acc: public IParamCalculator {

public:

	~AbsoluteFluxCalculator_acc() {
	}

	/**
	 * calculateParameter: returns the USFLUX parameter in results[0]
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return -1;

		//Calculate how many elements to sum
		const int n = nx * ny;

		//var for holding sum
		double sum = 0.0;

#pragma acc data present(mask[0:n],bitmask[0:n],bz[0:n])
#pragma acc declare deviceptr(results)
		{
#pragma acc parallel loop reduction(+:sum)
			for (int i = 0; i < n; i++) {
				if (mask[i] < 70 || bitmask[i] < 30)
					continue;
				if (test_isnan(bz[i]))
					continue;

				sum += (fabs(bz[i]));
			}

				double mean_vf = sum * cdelt1 * cdelt1 * (rsun_ref / rsun_obs)
						* (rsun_ref / rsun_obs) * 100.0 * 100.0;

#pragma acc region
			{
				results[0] = mean_vf;
			}
		}

		return 1;
	}

protected:
#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};

#endif
