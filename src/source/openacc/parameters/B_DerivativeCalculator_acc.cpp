/*
 * File: B_DerivativeCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 31, 2018
 */

#ifndef B_DERIVATIVE_ACC_CPP
#define B_DERIVATIVE_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the MEANGBT parameter
 */
class B_DerivativeCalculator_acc: public IParamCalculator {

public:

	/**
	 * calculateParameter: returns the MEANGBT parameter in results[0]
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return -1;

		const int n = nx * ny;

		double* bt = (double*) acc_malloc((size_t) n * sizeof(double));
		double* derx_bt = (double*) acc_malloc((size_t) n * sizeof(double));
		double* dery_bt = (double*) acc_malloc((size_t) n * sizeof(double));

#pragma acc data present(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(bt, derx_bt, dery_bt, results)
		{

#pragma acc parallel loop independent
			for (int i = 0; i < n; i++) {
				if (test_isnan(bx[i])) {
					bt[i] = NAN;
					continue;
				}
				if (test_isnan(by[i])) {
					bt[i] = NAN;
					continue;
				}
				if (test_isnan(bz[i])) {
					bt[i] = NAN;
					continue;
				}
				bt[i] = sqrt(bx[i] * bx[i] + by[i] * by[i] + bz[i] * bz[i]);

			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
#pragma acc loop independent
				for (int i = 1; i <= nx - 2; i++) {
					derx_bt[j * nx + i] = (bt[j * nx + i + 1]
							- bt[j * nx + i - 1]) * 0.5;

				}
			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma acc parallel loop independent
			for (int j = 1; j <= ny - 2; j++) {
#pragma acc loop independent
				for (int i = 0; i <= nx - 1; i++) {
					dery_bt[j * nx + i] = (bt[(j + 1) * nx + i]
							- bt[(j - 1) * nx + i]) * 0.5;

				}
			}

			/* consider the edges for the arrays that contribute to the variable "sum" in the computation below.
			 ignore the edges for the error terms as those arrays have been initialized to zero.
			 this is okay because the error term will ultimately not include the edge pixels as they are selected out by the mask and bitmask arrays.*/

#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
				derx_bt[j * nx] = ((-3 * bt[j * nx]) + (4 * bt[j * nx + (1)])
						- (bt[j * nx + (2)])) * 0.5;
			}

			const int edgI = nx - 1;
#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
				derx_bt[j * nx + edgI] = ((3 * bt[j * nx + edgI])
						+ (-4 * bt[j * nx + (edgI - 1)])
						- (-bt[j * nx + (edgI - 2)])) * 0.5;
			}

#pragma acc parallel loop independent
			for (int i = 0; i <= nx - 1; i++) {
				dery_bt[i] =
						((-3 * bt[i]) + (4 * bt[nx + i]) - (bt[2 * nx + i]))
								* 0.5;
			}

			const int edgJ = ny - 1;
#pragma acc parallel loop independent
			for (int i = 0; i <= nx - 1; i++) {
				dery_bt[edgJ * nx + i] = ((3 * bt[edgJ * nx + i])
						+ (-4 * bt[(edgJ - 1) * nx + i])
						- (-bt[(edgJ - 2) * nx + i])) * 0.5;
			}

			int count_mask = 0;
			double sum = 0.0;

#pragma acc parallel loop collapse(2) reduction(+:sum, count_mask)
			for (int j = 1; j <= ny - 2; j++) {
				for (int i = 1; i <= nx - 2; i++) {
					if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
						continue;
					if ((derx_bt[j * nx + i] + dery_bt[j * nx + i]) == 0)
						continue;
					if (test_isnan(bt[j * nx + i]))
						continue;
					if (test_isnan(bt[(j + 1) * nx + i]))
						continue;
					if (test_isnan(bt[(j - 1) * nx + i]))
						continue;
					if (test_isnan(bt[j * nx + i - 1]))
						continue;
					if (test_isnan(bt[j * nx + i + 1]))
						continue;
					if (test_isnan(derx_bt[j * nx + i]))
						continue;
					if (test_isnan(dery_bt[j * nx + i]))
						continue;

					//Compiler doesn't like function calls on variables being reduced
					double result = sqrt(
							derx_bt[j * nx + i] * derx_bt[j * nx + i]
									+ dery_bt[j * nx + i]
											* dery_bt[j * nx + i]); /* Units of Gauss */
					sum += result;
					count_mask++;

				}

			}

				double mean_derivative_btotal = (sum) / (count_mask);
#pragma acc region
			{
				results[0] = mean_derivative_btotal;
			}

		}

		acc_free(bt);
		acc_free(derx_bt);
		acc_free(dery_bt);

		return 1;

	}

private:
#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};

#endif
