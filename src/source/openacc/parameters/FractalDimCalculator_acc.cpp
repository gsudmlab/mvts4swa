/*
 * File: FractalDimCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 *
 *
 * Created on October 03, 2018
 */
#ifndef FRACTALCALC_ACC_CPP
#define FRACTALCALC_ACC_CPP

#include <algorithm>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <openacc.h>

#include "../../include/parameters/IParamCalculator.cpp"
#include "../../include/filters/IImageFilter.cpp"

class FractalDimCalculator_acc: public IParamCalculator {

	IImageFilter* PILFilter;
public:

	FractalDimCalculator_acc(IImageFilter* PILFilter) {
		this->PILFilter = PILFilter;
	}

	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		int n = nx * ny;
		int maxSize = std::max(nx, ny);
		double* p1 = (double*) acc_malloc((size_t) n * sizeof(double));
		//double* tmpResult = (double*) acc_malloc((size_t) 1 * sizeof(double));

#pragma acc data present(bz[0:n], bx[0:n], by[0:n], los[0:n])
#pragma acc declare deviceptr(p1)
		{
			this->PILFilter->process(los, p1, nx, ny);
			double fractDim = this->computeFDimension(p1, nx, ny, maxSize);
#pragma acc region deviceptr(results)
			{
				results[0] = fractDim;
			}

			this->PILFilter->process(bz, p1, nx, ny);
			fractDim = this->computeFDimension(p1, nx, ny, maxSize);
#pragma acc region deviceptr(results)
			{
				results[1] = fractDim;
			}

			this->PILFilter->process(by, p1, nx, ny);
			fractDim = this->computeFDimension(p1, nx, ny, maxSize);
#pragma acc region deviceptr(results)
			{
				results[2] = fractDim;
			}
			this->PILFilter->process(bx, p1, nx, ny);
			fractDim = this->computeFDimension(p1, nx, ny, maxSize);
#pragma acc region deviceptr(results)
			{
				results[3] = fractDim;
			}
		}

		acc_free(p1);

		return 4;
	}

private:

	double computeFDimension(double* image, int nx, int ny, int n) {

		int maxBoxSize = ((int) (2 * round(sqrt(n))));
		int numBoxes = (int) log2(maxBoxSize * 1.0);

		int boxSizes[numBoxes];

		boxSizes[0] = maxBoxSize;
		int i = 1;
		while ((maxBoxSize /= 2) > 1) {
			boxSizes[i++] = maxBoxSize;
		}

		/*
		 * Do counting for boxes of different sizes.
		 */
		double counts[numBoxes];

		for (int i = 0; i < numBoxes; i++) {
			counts[i] = this->countBoxes(image, nx, ny, boxSizes[i]);
		}

		//do Simple linear regression.
		double sumX = 0;
		double sumY = 0;
		double sumXX = 0;
		double sumXY = 0;
		for (int i = 0; i < numBoxes; i++) {
			double x = counts[i];
			double y = log(counts[i]);
			sumX += x;
			sumY += y;
			sumXX += x * x;
			sumXY += x * y;
		}

		double slope = (numBoxes * sumXY - sumX * sumY)
				/ (numBoxes * sumXX - sumX * sumX);

		if (std::isfinite(slope))
			return slope;
		else
			return 0;
	}
	/**
	 * This method counts the number of boxes which intersect with at least one pixel
	 * of the foreground (i.e., detected edges)
	 *
	 * <br>
	 * <b>Note:</b> This method assumes that the given image (represented by the 1D
	 * matrix) is already <i>edge-detected</i>, meaning that the array <code>image</code>
	 * is a binary array containing only <code>colors[0]</code> as the background and
	 * <code>colors[1]</code> as the foreground.
	 * <br>
	 * <b>Note:</b> The box counting algorithm is inspired from: <a href =
	 * "https://imagej.nih.gov/ij/developer/source/index.html">FractalBoxCounter</a>
	 *
	 * @param image
	 *            The given image in the form of a 1D matrix of binary values; background
	 *            and foreground.
	 * @param nx
	 *            The width of the image
	 * @param ny
	 *            The height of the image
	 * @param boxSize
	 *            The size of the box using for applying the box-counting method
	 *            on the edges.
	 * @return The number of boxes needed to cover all the edges on the given
	 *         image.
	 */

	int countBoxes(double* image, int nx, int ny, int boxSize) {

		int sum = 0;
#pragma acc data deviceptr(image)
		{
			//Step over the image, one box at a time.
#pragma acc parallel loop collapse(2),reduction(+:sum)
			for (int y = 0; y < ny; y += boxSize) {
				for (int x = 0; x < nx; x += boxSize) {

					if (this->boxHasEdge(image, nx, ny, boxSize, x, y)) {
						sum++;
					}

				}
			}
		}

		return sum;
	}

#pragma acc routine seq
	bool boxHasEdge(double* img, int nx, int ny, int boxSize, int x, int y) {
		//Test if the box contains an edge in it and count this box if it does.
		bool boxHasEdge = false;
#pragma acc data deviceptr(img)
		{
			for (int dy = 0; dy < boxSize; dy++) {

				//Test if we have gone beyond the height of the image.
				if ((y + dy) > ny || boxHasEdge)
					break;

				int idxY = (y + dy) * nx;

				for (int dx = 0; dx < boxSize; dx++) {
					int idxX = (x + dx);

					//Test if we have gone beyond the width of the image.
					if (idxX > nx)
						break;

					if (img[idxY + idxX] > 0) { // If subPatch has any foreground color in it
						/*
						 * this subPatch spans over a segment of an edge, so
						 * it should be counted and there is no need to proceed any further.
						 */
						boxHasEdge = true;
						break;
					}
				}
			}
		}
		return boxHasEdge;
	}
};

#endif
