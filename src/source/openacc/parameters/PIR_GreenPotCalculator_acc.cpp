/*
 * File: PIR_GreenPotCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 * With modification to utilize a weighting map around the polarity inversion line.
 *
 * Created on April 23, 2019
 */

#ifndef PIR_GREENPOT_OMP_CPP
#define PIR_GREENPOT_OMP_CPP

#include <cstdlib>
#include <math.h>
#include <stdlib.h>
#include <openacc.h>

using namespace std;

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the TOTPOT, MEANPOT, SHRGT45, MEANSHR parameters
 * using a weighting function around the PIL.
 */
class PIR_GreenPotCalculator_acc: public IParamCalculator {
private:
	double pi = (3.14159265358979323846264338327950288);

public:

	/**
	 * calculateParameter: returns PIR_TOTPOT, PIR_MEANPOT, PIR_SHRGT45, PIR_MEANSHR
	 * parameters in results[0] -- results[3]
	 * Assumes that the los is the PIR weight map and is a device pointer.
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return -1;

		const int n = nx * ny;

		double retValue = 4;

		double *bztmp = (double*) acc_malloc((size_t) n * sizeof(double));
		double *bpx = (double*) acc_malloc((size_t) n * sizeof(double));
		double *bpy = (double*) acc_malloc((size_t) n * sizeof(double));
		double *tmpResult = (double*) acc_malloc((size_t) 2 * sizeof(double));

#pragma acc data present(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(bztmp, bpx, bpy, tmpResult, los)
		{

			this->greenpot(bz, bpx, bpy, nx, ny, bztmp);

			this->computeFreeEnergy(bx, by, bpx, bpy, los, nx, ny, mask,
					bitmask, cdelt1, rsun_ref, rsun_obs, tmpResult);
#pragma acc region deviceptr(results)
			{

				double meanpot = tmpResult[0];
				double totpot = tmpResult[1];
				results[0] = totpot;
				results[1] = meanpot;
			}

			this->computeShearAngle(bx, by, bz, bpx, bpy, los, nx, ny, mask,
					bitmask, tmpResult);
#pragma acc region deviceptr(results)
			{
				double area_w_shear_gt_45 = tmpResult[0];
				double meanshear_angle = tmpResult[1];
				results[2] = area_w_shear_gt_45;
				results[3] = meanshear_angle;
			}

		}

		acc_free(bztmp);
		acc_free(bpx);
		acc_free(bpy);
		acc_free(tmpResult);

		return retValue;
	}
private:

	void greenpot(double *bz, double *bpx, double *bpy, const int nx,
			const int ny, double *bztmp) {

		const int n = nx * ny;

		double *pfpot = (double*) acc_malloc((size_t) n * sizeof(double));
		double *rdist = (double*) acc_malloc((size_t) n * sizeof(double));

#pragma acc data present(bz[0:n])
#pragma acc declare deviceptr(bpx, bpy, bztmp, pfpot, rdist)
		{

			// dz is the monopole depth
			const double dz = 0.001;
			const int iwindow = 3; //this used to be a bunch of code that did nothing.

#pragma acc parallel loop independent
			for (int i = 0; i < n; i++) {

				double val0 = bz[i];
				if (test_isnan(val0)) {
					bztmp[i] = 0.0;
				} else {
					bztmp[i] = val0;
				}
			}

#pragma acc parallel loop independent
			for (int iny = 0; iny < ny; iny++) {
#pragma acc loop independent
				for (int inx = 0; inx < nx; inx++) {
					double rdd1 = (double) (inx);
					double rdd2 = (double) (iny);
					double rdd = rdd1 * rdd1 + rdd2 * rdd2 + dz * dz;
					rdist[nx * iny + inx] = 1.0 / sqrt(rdd);
				}
			}

#pragma acc parallel loop independent
			for (int iny = 0; iny < ny; iny++) {
#pragma acc loop independent
				for (int inx = 0; inx < nx; inx++) {
					double val0 = bz[nx * iny + inx];
					if (test_isnan(val0)) {
						pfpot[nx * iny + inx] = 0.0; // hmmm.. NAN;
					} else {

						int j2s, j2e, i2s, i2e;
						j2s = iny - iwindow;
						j2e = iny + iwindow;
						if (j2s < 0) {
							j2s = 0;
						}
						if (j2e > ny) {
							j2e = ny;
						}
						i2s = inx - iwindow;
						i2e = inx + iwindow;
						if (i2s < 0) {
							i2s = 0;
						}
						if (i2e > nx) {
							i2e = nx;
						}

						double sum = pfpotSum(j2s, j2e, i2s, i2e, nx, inx, iny,
								bztmp, rdist);

						pfpot[nx * iny + inx] = sum; // Note that this is a simplified definition.

					}
				}
			}

#pragma acc parallel loop independent
			for (int iny = 1; iny < ny - 1; iny++) {
#pragma acc loop independent
				for (int inx = 1; inx < nx - 1; inx++) {
					bpx[nx * iny + inx] = -(pfpot[nx * iny + (inx + 1)]
							- pfpot[nx * iny + (inx - 1)]) * 0.5;
					bpy[nx * iny + inx] = -(pfpot[nx * (iny + 1) + inx]
							- pfpot[nx * (iny - 1) + inx]) * 0.5;
				}
			}

		}

		acc_free(pfpot);
		acc_free(rdist);

	}

#pragma acc routine seq
	double pfpotSum(const int j2s, const int j2e, const int i2s, const int i2e,
			const int nx, const int inx, const int iny, double* bztmp,
			double* rdist) {
		//had to offload this into a routine because the compiler was having difficulties
		//figuring out how to handle the double loop inside a double loop

		// dz is the monopole depth
		const double dz = 0.001;

		double sum = 0;
		for (int j2 = j2s; j2 < j2e; j2++) {
			for (int i2 = i2s; i2 < i2e; i2++) {
				double val1 = bztmp[nx * j2 + i2];
				int di, dj;
				di = abs(i2 - inx);
				dj = abs(j2 - iny);
				sum += val1 * rdist[nx * dj + di] * dz;
			}
		}
		return sum;
	}

	void computeFreeEnergy(double *bx, double *by, double *bpx, double *bpy,
			double *los, const int nx, const int ny, double *mask,
			double *bitmask, const double cdelt1, const double rsun_ref,
			const double rsun_obs, double* tmpResult) {

		const int n = nx * ny;

#pragma acc data present(bx[0:n], by[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(bpx, bpy, los)
		{

			int count_mask = 0;
			double sum = 0.0;
			double sumOne = 0.0;

#pragma acc parallel loop reduction(+:sum, sumOne, count_mask)
			for (int i = 0; i < n; i++) {

				if (mask[i] < 70 || bitmask[i] < 30) {
					continue;
				}
				if (test_isnan(bx[i])) {
					continue;
				}
				if (test_isnan(by[i])) {
					continue;
				}
				if (!los[i] > 0.0)
					continue;

				double tmp =
						los[i]
								* ((((bx[i] - bpx[i]) * (bx[i] - bpx[i]))
										+ ((by[i] - bpy[i]) * (by[i] - bpy[i])))
										* (cdelt1 * cdelt1
												* (rsun_ref / rsun_obs)
												* (rsun_ref / rsun_obs) * 100.0
												* 100.0));

				sum += tmp;

				sumOne += los[i]
						* (((bx[i] - bpx[i]) * (bx[i] - bpx[i]))
								+ ((by[i] - bpy[i]) * (by[i] - bpy[i])));

				count_mask++;

			}

#pragma acc region deviceptr(tmpResult)
			{
				/* Units of meanpotptr are ergs per centimeter */
				tmpResult[0] = (sumOne) / (count_mask * 8. * pi); /* Units are ergs per cubic centimeter */

				/* Units of sum are ergs/cm^3, units of factor are cm^2/pix^2; therefore, units of totpotptr are ergs per centimeter */
				tmpResult[1] = (sum) / (8. * pi);
			}

		}

	}

	void computeShearAngle(double *bx, double *by, double *bz, double *bpx,
			double *bpy, double *los, const int nx, const int ny, double *mask,
			double *bitmask, double *tmpResult) {

		const int n = nx * ny;

#pragma acc data present(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(bpx, bpy, los)
		{

			double count_mask = 0;
			double count = 0;
			double sum = 0.0;

#pragma acc parallel loop reduction(+:sum), reduction(+:count), reduction(+:count_mask)
			for (int i = 0; i < n; i++) {

				if (mask[i] < 70 || bitmask[i] < 30)
					continue;
				if (test_isnan(bpx[i]))
					continue;
				if (test_isnan(bpy[i]))
					continue;
				if (test_isnan(bz[i]))
					continue;
				if (test_isnan(bx[i]))
					continue;
				if (test_isnan(by[i]))
					continue;
				if (!los[i] > 0.0)
					continue;

				/* For mean 3D shear angle, percentage with shear greater than 45*/
				const double dotproduct = (bpx[i]) * (bx[i])
						+ (bpy[i]) * (by[i]) + (bz[i]) * (bz[i]);
				const double magnitude_potential = sqrt(
						(bpx[i] * bpx[i]) + (bpy[i] * bpy[i])
								+ (bz[i] * bz[i]));
				const double magnitude_vector = sqrt(
						(bx[i] * bx[i]) + (by[i] * by[i]) + (bz[i] * bz[i]));

				double shear_angle = acosWorkAround(
						dotproduct / (magnitude_potential * magnitude_vector))
						* (180.0 / pi);

				sum += los[i] * shear_angle;

				count++;

				if (shear_angle > 45)
					count_mask++;

			}

#pragma acc region deviceptr(tmpResult)
			{
				/* The area here is a fractional area -- the % of the total area. This has no error associated with it. */
				tmpResult[0] = (count_mask / (count)) * (100.0);

				/* For mean 3D shear angle, area with shear greater than 45*/
				tmpResult[1] = (sum) / (count); /* Units are degrees */

			}
		}

	}

#pragma acc routine seq
	static double acosWorkAround(double value) {
		return acosf((float) value);
	}

#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};

#endif
