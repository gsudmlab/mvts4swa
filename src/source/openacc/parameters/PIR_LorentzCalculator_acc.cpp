/*
 * File: PIR_LorentzCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 31, 2018
 */

#ifndef PIR_LORENTZ_ACC_CPP
#define PIR_LORENTZ_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY, TOTFZ parameters
 * using a weighting function around the PIL
 */
class PIR_LorentzCalculator_acc: public IParamCalculator {

public:

	/**
	 * Example function 16: Lorentz force as defined in Fisher, 2012
	 * This calculation is adapted from Xudong's code
	 *  at /proj/cgem/lorentz/apps/lorentz.c
	 *
	 * calculateParameter: returns the PIR_EPSX, PIR_EPSY, PIR_EPSZ, PIR_TOTBSQ, PIR_TOTFX, PIR_TOTFY,
	 * PIR_TOTFZ parameters in results[0] -- results[6]
	 * Assumes that the los is the PIR weight map and is a device pointer.
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return -1;

		const int n = nx * ny;

		double* fx = (double*) acc_malloc((size_t) n * sizeof(double));
		double* fy = (double*) acc_malloc((size_t) n * sizeof(double));
		double* fz = (double*) acc_malloc((size_t) n * sizeof(double));

#pragma acc data present(bz[0:n], bx[0:n], by[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(fx, fy, fz, los)
		{

			double totfx = 0;
			double totfy = 0;
			double totfz = 0;
			double totbsq = 0;

			//Had to break these apart because the reduction didn't like 4 of them
#pragma acc parallel loop reduction(+:totfx,totfy)
			for (int i = 0; i < n; i++) {

				if (mask[i] < 70 || bitmask[i] < 30)
					continue;
				if (test_isnan(bx[i]))
					continue;
				if (test_isnan(by[i]))
					continue;
				if (test_isnan(bz[i]))
					continue;
				if (!los[i] > 0.0)
					continue;

				fx[i] = bx[i] * bz[i];
				fy[i] = by[i] * bz[i];

				totfx += los[i] * fx[i];
				totfy += los[i] * fy[i];
			}

#pragma acc parallel loop reduction(+:totfz,totbsq)
			for (int i = 0; i < n; i++) {

				if (mask[i] < 70 || bitmask[i] < 30)
					continue;
				if (test_isnan(bx[i]))
					continue;
				if (test_isnan(by[i]))
					continue;
				if (test_isnan(bz[i]))
					continue;
				if (!los[i] > 0.0)
					continue;

				fz[i] = (bx[i] * bx[i] + by[i] * by[i] - bz[i] * bz[i]);
				double bsq = bx[i] * bx[i] + by[i] * by[i] + bz[i] * bz[i];

				totfz += los[i] * fz[i];
				totbsq += los[i] * bsq;
			}

#pragma acc region deviceptr(results)
			{
				double area = cdelt1 * cdelt1 * (rsun_ref / rsun_obs)
						* (rsun_ref / rsun_obs) * 100.0 * 100.0;

				results[0] = (totfx) / totbsq; // epsx
				results[1] = -(totfy) / totbsq; // epsy
				results[2] = (totfz) / totbsq; // epsz
				results[3] = totbsq; //totbsq
				results[4] = -totfx * area; //totfx
				results[5] = (totfy) * area; //totfy
				results[6] = (totfz) * area; //totfz
			}

		}

		acc_free(fx);
		acc_free(fy);
		acc_free(fz);

		return 7;
	}

private:
#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};

#endif
