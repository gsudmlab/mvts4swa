/*
 * File: PIR_BZ_DerivativeCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 * With modification to utilize a weighting map around the polarity inversion line.
 *
 * Created on April 23, 2019
 */

#ifndef PIR_BZ_DERIVATIVE_OMP_CPP
#define PIR_BZ_DERIVATIVE_OMP_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the MEANGBZ parameter using a weighting function around the PIL
 */
class PIR_BZ_DerivativeCalculator_acc: public IParamCalculator {

public:

	/**
	 * calculateParameter: returns the PIR_MEANGBZ parameter in results[0]
	 * Assumes that the los is the PIR weight map and is a device pointer.
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return 0;

		const int n = nx * ny;

		double* derx_bz = (double*) acc_malloc((size_t) n * sizeof(double));
		double* dery_bz = (double*) acc_malloc((size_t) n * sizeof(double));

#pragma acc data present(bz[0:n], bitmask[0:n], mask[0:n])
#pragma acc declare deviceptr(derx_bz, dery_bz, los)
		{

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
#pragma acc loop independent
				for (int i = 1; i <= nx - 2; i++) {
					derx_bz[j * nx + i] = (bz[j * nx + i + 1]
							- bz[j * nx + i - 1]) * 0.5;

				}
			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma acc parallel loop independent
			for (int j = 1; j <= ny - 2; j++) {
#pragma acc loop independent
				for (int i = 0; i <= nx - 1; i++) {
					dery_bz[j * nx + i] = (bz[(j + 1) * nx + i]
							- bz[(j - 1) * nx + i]) * 0.5;

				}
			}

			/* consider the edges for the arrays that contribute to the variable "sum" in the computation below.
			 ignore the edges for the error terms as those arrays have been initialized to zero.
			 this is okay because the error term will ultimately not include the edge pixels as they are selected out by the mask and bitmask arrays.*/

#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
				derx_bz[j * nx] = ((-3 * bz[j * nx]) + (4 * bz[j * nx + 1])
						- (bz[j * nx + 2])) * 0.5;
			}

			const int edgI = nx - 1;
#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
				derx_bz[j * nx + edgI] = ((3 * bz[j * nx + edgI])
						+ (-4 * bz[j * nx + (edgI - 1)])
						- (-bz[j * nx + (edgI - 2)])) * 0.5;
			}

			//j = 0;
#pragma acc parallel loop independent
			for (int i = 0; i <= nx - 1; i++) {
				dery_bz[i] =
						((-3 * bz[i]) + (4 * bz[nx + i]) - (bz[2 * nx + i]))
								* 0.5;
			}

			const int edgJ = ny - 1;
#pragma acc parallel loop independent
			for (int i = 0; i <= nx - 1; i++) {
				dery_bz[edgJ * nx + i] = ((3 * bz[edgJ * nx + i])
						+ (-4 * bz[(edgJ - 1) * nx + i])
						- (-bz[(edgJ - 2) * nx + i])) * 0.5;
			}

			int count_mask = 0;
			double sum = 0.0;

#pragma acc parallel loop collapse(2),reduction(+:sum),reduction(+:count_mask)
			for (int j = 0; j <= ny - 1; j++) {
				for (int i = 0; i <= nx - 1; i++) {
					if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
						continue;
					if ((derx_bz[j * nx + i] + dery_bz[j * nx + i]) == 0)
						continue;
					if (test_isnan(bz[j * nx + i]))
						continue;
					if (test_isnan(bz[(j + 1) * nx + i]))
						continue;
					if (test_isnan(bz[(j - 1) * nx + i]))
						continue;
					if (test_isnan(bz[j * nx + i - 1]))
						continue;
					if (test_isnan(bz[j * nx + i + 1]))
						continue;
					if (test_isnan(derx_bz[j * nx + i]))
						continue;
					if (test_isnan(dery_bz[j * nx + i]))
						continue;
					if (!los[j * nx + i] > 0)
						continue;

					//Compiler doesn't like function calls on variables being reduced
					double result = los[j * nx + i]
							* sqrt(
									derx_bz[j * nx + i] * derx_bz[j * nx + i]
											+ dery_bz[j * nx + i]
													* dery_bz[j * nx + i]); /* Units of Gauss */
					sum += result;
					count_mask++;
				}

			}

			double mean_derivative_bz = (sum) / (count_mask); // would be divided by ((nx-2)*(ny-2)) if shape of count_mask = shape of magnetogram
#pragma acc region deviceptr(results)
			{
				results[0] = mean_derivative_bz;
			}

		}

		acc_free(derx_bz);
		acc_free(dery_bz);

		return 1;

	}

private:
#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};

#endif
