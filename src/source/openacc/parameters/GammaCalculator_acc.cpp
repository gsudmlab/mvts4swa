/*
 * File: GammaCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *  Created on: Jul 28, 2018
 */

#ifndef GAMMA_ACC_CPP
#define GAMMA_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the MEANGAM parameter
 */
class GammaCalculator_acc: public IParamCalculator {
private:
	double pi = 3.14159265358979323846264338327950288;

public:

	/**
	 * calculateParameter: returns the MEANGAM parameter in results[0]
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return -1;

		const int n = nx * ny;

		double* bh = (double*) acc_malloc((size_t) n * sizeof(double));

#pragma acc data present(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(bh, results)
		{

			int count_mask = 0;
			double sum = 0.0;

			computeBh(bx, by, bh, nx, ny);

#pragma acc parallel loop reduction(+:sum,count_mask)
			for (int i = 0; i < n; i++) {

				if (bh[i] > 100) {
					if (mask[i] < 70 || bitmask[i] < 30)
						continue;
					if (test_isnan(bz[i]))
						continue;
					if (test_isnan(bh[i]))
						continue;
					if (bz[i] == 0)
						continue;

					//Compiler doesn't like function calls on variables being reduced
					double absBz = fabs(bz[i]);
					double atBh = atan(bh[i] / absBz);
					double result = fabs(atBh) * (180. / pi);

					sum += result;
					count_mask++;
				}

			}

			double mean_gamma = sum / count_mask;
#pragma acc region
			{
				results[0] = mean_gamma;
			}

		}

		acc_free(bh);

		return 1;
	}

private:

	static void computeBh(double* bx, double * by, double * bh, const int nx,
			const int ny) {

		const int n = nx * ny;

#pragma acc data present(bx[0:n], by[0:n])
#pragma acc declare deviceptr(bh)
		{
#pragma acc parallel loop independent
			for (int i = 0; i < n; i++) {

				if (test_isnan(bx[i])) {
					bh[i] = NAN;
					continue;
				}

				if (test_isnan(by[i])) {
					bh[i] = NAN;
					continue;
				}

				bh[i] = sqrt(bx[i] * bx[i] + by[i] * by[i]);
			}
		}

	}

private:
#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};

#endif
