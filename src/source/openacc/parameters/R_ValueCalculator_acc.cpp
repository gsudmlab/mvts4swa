/*
 * File: R_ValueCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *  R parameter as defined in Schrijver, 2007
 *
 *
 * Created on October 01, 2018
 */

#ifndef RVALUE_ACC_CPP
#define RVALUE_ACC_CPP

#include <cstdlib>
#include <math.h>
#include <openacc.h>

#include "../../include/filters/IImageFilter.cpp"
#include "../../include/parameters/IParamCalculator.cpp"

class R_ValueCalculator_acc: public IParamCalculator {

	IImageFilter* averageDownFilter;
	IImageFilter* downFilter;
	IImageFilter* boxcarFilter;
	IImageFilter* gaussianFilter;
	double scaleFactor = 0.25;

public:

	R_ValueCalculator_acc(IImageFilter* averageDownFilter,
			IImageFilter* downFilter, IImageFilter* boxcarFilter,
			IImageFilter* gaussianFilter, int scale) {

		this->averageDownFilter = averageDownFilter;
		this->downFilter = downFilter;
		this->boxcarFilter = boxcarFilter;
		this->gaussianFilter = gaussianFilter;
		this->scaleFactor = (scale * 0.01);

	}

	~R_ValueCalculator_acc() {
		delete downFilter;
		delete boxcarFilter;
		delete gaussianFilter;
	}

	/**
	 * Example function 15: R parameter as defined in Schrijver, 2007
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		int n = nx * ny;
		int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
		int newheight = (int) ((ny * this->scaleFactor) + 0.5);

		int newN = newwidth * newheight;
		double* rimage = (double*) acc_malloc((size_t) newN * sizeof(double));
		double* p1 = (double*) acc_malloc((size_t) newN * sizeof(double));

#pragma acc data present(bz[0:n], bx[0:n], by[0:n], los[0:n])
#pragma acc declare deviceptr(rimage, p1)
		{
			this->computePIL(los, p1, nx, ny);
			this->getDownSizeRim(los, rimage, mask, bitmask, nx, ny);
			double Rparam = this->computeRValue(p1, rimage, nx, ny);
#pragma acc region deviceptr(results)
			{
				results[0] = Rparam;
			}

			this->computePIL(bz, p1, nx, ny);
			this->getDownSizeRim(bz, rimage, mask, bitmask, nx, ny);
			Rparam = this->computeRValue(p1, rimage, nx, ny);
#pragma acc region deviceptr(results)
			{
				results[1] = Rparam;
			}

			this->computePIL(by, p1, nx, ny);
			this->getDownSizeRim(by, rimage, mask, bitmask, nx, ny);
			Rparam = this->computeRValue(p1, rimage, nx, ny);
#pragma acc region deviceptr(results)
			{
				results[2] = Rparam;
			}

			this->computePIL(bx, p1, nx, ny);
			this->getDownSizeRim(bx, rimage, mask, bitmask, nx, ny);
			Rparam = this->computeRValue(p1, rimage, nx, ny);
#pragma acc region deviceptr(results)
			{
				results[3] = Rparam;
			}

		}

		acc_free(rimage);
		acc_free(p1);

		return 8;
	}

private:

	void getDownSizeRim(double* in_image, double* processed_image, double* mask,
			double* bitmask, int nx, int ny) {

		int n = nx * ny;
		double* tmpImg = (double*) acc_malloc(
				(size_t) nx * ny * sizeof(double));

#pragma acc data present(in_image[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(tmpImg, processed_image, tmpImg)
		{
#pragma acc parallel loop collapse(2)
			for (int j = 0; j < ny; j++) {
				for (int i = 0; i < nx; i++) {
					int idx = j * nx + i;
					double val = in_image[idx];
					if (mask[idx] < 55 || bitmask[idx] < 30)
						val = 0;
					tmpImg[idx] = val;
				}
			}

			//................[Step 1]..................
			//Down sample the image to 1k resolution
			this->averageDownFilter->process(tmpImg, processed_image, nx, ny);
		}
		acc_free(tmpImg);
	}

	double computeRValue(double* pil_image, double* rim, int nx, int ny) {

		int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
		int newheight = (int) ((ny * this->scaleFactor) + 0.5);
		//int n = newwidth * newheight;

		int nxp = newwidth + 40;
		int nyp = newheight + 40;

		double* pmap = (double*) acc_malloc(
				(size_t) nxp * nyp * sizeof(double));
		double* p1pad = (double*) acc_malloc(
				(size_t) nxp * nyp * sizeof(double));
		double* pmapn = (double*) acc_malloc(
				(size_t) newwidth * newheight * sizeof(double));

		double Rparam;

		double sum = 0;

#pragma acc data
#pragma acc declare deviceptr(pil_image, rim, pmap, p1pad, pmapn)
		{
			// pad p1 with zeroes so that the gaussian colvolution in step 5
			// does not cut off data within hwidth of the edge
			// step i: zero p1pad
#pragma acc parallel loop independent
			for (int j = 0; j < nyp; j++) {
#pragma acc loop independent
				for (int i = 0; i < nxp; i++) {
					int index = j * nxp + i;
					p1pad[index] = 0.0;
				}
			}

			// step ii: place p1 at the center of p1pad
#pragma acc parallel loop independent
			for (int j = 0; j < newheight; j++) {
#pragma acc loop independent
				for (int i = 0; i < newwidth; i++) {
					int index = j * newwidth + i;
					int index1 = (j + 20) * nxp + (i + 20);
					p1pad[index1] = pil_image[index];
				}
			}

			this->gaussianFilter->process(p1pad, pmap, nxp, nyp);

			// select out the nx1 x ny1 non-padded array  within pmap
#pragma acc parallel loop independent
			for (int j = 0; j < newheight; j++) {
#pragma acc loop independent
				for (int i = 0; i < newwidth; i++) {
					int index = j * newwidth + i;
					int index1 = (j + 20) * nxp + (i + 20);
					pmapn[index] = pmap[index1];
				}
			}

			// =============== [STEP 6] ===============
			// the R parameter is calculated
#pragma acc parallel loop collapse(2), reduction(+:sum)
			for (int j = 0; j < newheight; j++) {
				for (int i = 0; i < newwidth; i++) {
					int index = j * newwidth + i;
					if (test_isnan(pmapn[index]))
						continue;
					if (test_isnan(rim[index]))
						continue;
					sum += pmapn[index] * fabs(rim[index]);
				}
			}
		}

		if (sum < 1.0)
			Rparam = 0.0;
		else
			Rparam = log10(sum);

		acc_free(pmap);
		acc_free(p1pad);
		acc_free(pmapn);

		return Rparam;
	}

	void computePIL(double* in_image, double* pil_image, int nx, int ny) {

		int n = nx * ny;
		int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
		int newheight = (int) ((ny * this->scaleFactor) + 0.5);

		double* processed_image = (double*) acc_malloc(
				(size_t) newwidth * newheight * sizeof(double));
		double* p1p0 = (double*) acc_malloc(
				(size_t) newwidth * newheight * sizeof(double));
		double* p1n0 = (double*) acc_malloc(
				(size_t) newwidth * newheight * sizeof(double));
		double* p1p = (double*) acc_malloc(
				(size_t) newwidth * newheight * sizeof(double));
		double* p1n = (double*) acc_malloc(
				(size_t) newwidth * newheight * sizeof(double));

#pragma acc data present(in_image[0:n])
#pragma acc declare deviceptr(processed_image, pil_image, p1p0, p1n0, p1p, p1n)
		{
			//................[Step 1]..................
			//Down sample the image to 1k resolution
			this->downFilter->process(in_image, processed_image, nx, ny);

			//................[Step 2]..................
			//Identify positive and negative pixels greater than +/- 150 gauss
			//Label those pixels with a 1.0 in arrays p1p0 and p1n0 respectively
#pragma acc parallel loop independent
			for (int j = 0; j < newheight; j++) {
#pragma acc loop independent
				for (int i = 0; i < newwidth; i++) {
					int index = j * newwidth + i;
					if (processed_image[index] > 150)
						p1p0[index] = 1.0;
					else
						p1p0[index] = 0.0;

					if (processed_image[index] < -150)
						p1n0[index] = 1.0;
					else
						p1n0[index] = 0.0;
				}
			}

			// =============== [STEP 3] ===============
			// smooth each of the negative and positive pixel bitmaps
			this->boxcarFilter->process(p1p0, p1p, newwidth, newheight);
			this->boxcarFilter->process(p1n0, p1n, newwidth, newheight);

			// =============== [STEP 4] ===============
			// find the pixels for which p1p and p1n are both equal to 1.
			// this defines the polarity inversion line
#pragma acc parallel loop independent
			for (int j = 0; j < newheight; j++) {
#pragma acc loop independent
				for (int i = 0; i < newwidth; i++) {
					int index = j * newwidth + i;
					if ((p1p[index] > 0.0) && (p1n[index] > 0.0))
						pil_image[index] = 1.0;
					else
						pil_image[index] = 0.0;
				}
			}
		}

		acc_free(p1p0);
		acc_free(p1n0);
		acc_free(p1p);
		acc_free(p1n);
		acc_free(processed_image);

	}

#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};
#endif
