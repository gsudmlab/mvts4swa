/*
 * File: JZ_Calculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 31, 2018
 */

#ifndef JZ_CALC_ACC_CPP
#define JZ_CALC_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH, ABSNJZH, SAVNCPP parameters.
 */
class JZ_Calculator_acc: public IParamCalculator {
private:
	double MUNAUGHT = 0.0000012566370614; /* magnetic constant */

public:

	/**
	 * calculateParameter: returns the TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH,
	 * ABSNJZH, SAVNCPP parameters in results[0] -- results[6]
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return -1;

		const int n = nx * ny;

		int retVal = 7;

		double* jz = (double*) acc_malloc((size_t) n * sizeof(double));
		double* derx = (double*) acc_malloc((size_t) n * sizeof(double));
		double* dery = (double*) acc_malloc((size_t) n * sizeof(double));
		double* tmpResult = (double*) acc_malloc((size_t) 3 * sizeof(double));

#pragma acc data present(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(jz, derx, dery, tmpResult)
		{

			this->computeJz(bx, by, nx, ny, jz, derx, dery);

			this->computeJzsmooth(jz, mask, bitmask, nx, ny, cdelt1, rsun_ref,
					rsun_obs, derx, dery, tmpResult);
#pragma acc region deviceptr(results)
			{
				double mean_jz = tmpResult[0];
				double us_i = tmpResult[1];
				results[0] = us_i;
				results[1] = mean_jz;
			}

			this->computeAlpha(bz, jz, mask, bitmask, nx, ny, cdelt1, rsun_ref,
					rsun_obs, tmpResult);
#pragma acc region deviceptr(results)
			{
				double mean_alpha = tmpResult[0];
				results[2] = mean_alpha;
			}

			this->computeHelicity(bz, jz, mask, bitmask, nx, ny, cdelt1,
					rsun_ref, rsun_obs, tmpResult);
#pragma acc region deviceptr(results)
			{
				double mean_ih = tmpResult[0];
				double total_us_ih = tmpResult[1];
				double total_abs_ih = tmpResult[2];
				results[3] = mean_ih;
				results[4] = total_us_ih;
				results[5] = total_abs_ih;
			}

			this->computeSumAbsPerPolarity(bz, jz, mask, bitmask, nx, ny,
					cdelt1, rsun_ref, rsun_obs, tmpResult);
#pragma acc region deviceptr(results)
			{
				double totaljz = 0;
				totaljz = tmpResult[0];

				results[6] = totaljz;
			}

		}

		acc_free(jz);
		acc_free(derx);
		acc_free(dery);
		acc_free(tmpResult);

		return retVal;

	}

private:

	void computeJz(double *bx, double *by, const int nx, const int ny,
			double *jz, double *derx, double *dery) {

		const int n = nx * ny;

#pragma acc data present(bx[0:n], by[0:n])
#pragma acc declare deviceptr(jz, derx, dery)
		{

			/* Calculate the derivative*/
			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
#pragma acc loop independent
				for (int i = 1; i < nx - 1; i++) {
					derx[j * nx + i] = (by[j * nx + i + 1] - by[j * nx + i - 1])
							* 0.5;
				}
			}

#pragma acc parallel loop independent
			for (int j = 1; j < ny - 1; j++) {
#pragma acc loop independent
				for (int i = 0; i < nx; i++) {
					dery[j * nx + i] = (bx[(j + 1) * nx + i]
							- bx[(j - 1) * nx + i]) * 0.5;

				}
			}

			/* consider the edges for the arrays that contribute to the variable "sum" in the computation below.
			 ignore the edges for the error terms as those arrays have been initialized to zero.
			 this is okay because the error term will ultimately not include the edge pixels as they are selected out by the mask and bitmask arrays.*/

			//i = 0;
#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
				derx[j * nx] = ((-3 * by[j * nx]) + (4 * by[j * nx + 1])
						- (by[j * nx + 2])) * 0.5;
			}

			const int edgI = nx - 1;
#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
				derx[j * nx + edgI] = ((3 * by[j * nx + edgI])
						+ (-4 * by[j * nx + (edgI - 1)])
						- (-by[j * nx + (edgI - 2)])) * 0.5;
			}

#pragma acc parallel loop independent
			for (int i = 0; i < nx; i++) {
				dery[i] = ((-3 * bx[i]) + (4 * bx[nx + i]) - (bx[2 * nx + i]))
						* 0.5;
			}

			const int edgJ = ny - 1;
#pragma acc parallel loop independent
			for (int i = 0; i < nx; i++) {
				dery[edgJ * nx + i] = ((3 * bx[edgJ * nx + i])
						+ (-4 * bx[(edgJ - 1) * nx + i])
						- (-bx[(edgJ - 2) * nx + i])) * 0.5;
			}

#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
#pragma acc loop independent
				for (int i = 0; i < nx; i++) {
					// calculate jz at all points
					jz[j * nx + i] = (derx[j * nx + i] - dery[j * nx + i]);

				}
			}

		}

	}

	void computeJzsmooth(double *jz, double *mask, double *bitmask,
			const int nx, const int ny, const double cdelt1,
			const double rsun_ref, const double rsun_obs, double *derx,
			double *dery, double *tmpResult) {

		const int n = nx * ny;

#pragma acc data present(mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(jz, derx, dery, tmpResult)
		{

			int count_mask = 0;
			double curl = 0.0;
			double us_i = 0.0;

			/* At this point, use the smoothed Jz array with a Gaussian (FWHM of 4 pix and truncation width of 12 pixels) but keep the original array dimensions*/
#pragma acc parallel loop collapse(2),reduction(+:curl,us_i,count_mask)
			for (int j = 0; j <= ny - 1; j++) {
				for (int i = 0; i <= nx - 1; i++) {
					if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
						continue;
					if (test_isnan(derx[j * nx + i]))
						continue;
					if (test_isnan(dery[j * nx + i]))
						continue;
					if (test_isnan(jz[j * nx + i]))
						continue;

					curl += (jz[j * nx + i]) * (1 / cdelt1)
							* (rsun_obs / rsun_ref) * (0.00010) * (1 / MUNAUGHT)
							* (1000.); /* curl is in units of mA / m^2 */

					//Compiler doesn't like function calls on variables being reduced
					double usTmp = fabs(jz[j * nx + i]) * (cdelt1 / 1)
							* (rsun_ref / rsun_obs) * (0.00010)
							* (1 / MUNAUGHT); /* us_i is in units of A */
					us_i += usTmp;

					count_mask++;
				}
			}

			double tmpMeanJZ = curl / (count_mask);
			double tmpUSI = us_i;
#pragma acc region
			{
				/* Calculate mean vertical current density (mean_jz) and total unsigned vertical current (us_i) using smoothed Jz array and continue conditions above */
				tmpResult[0] = tmpMeanJZ; /* mean_jz gets populated as MEANJZD */
				tmpResult[1] = tmpUSI; /* us_i gets populated as TOTUSJZ */
			}
		}

	}

	void computeAlpha(double *bz, double *jz, double *mask, double *bitmask,
			const int nx, const int ny, const double cdelt1,
			const double rsun_ref, const double rsun_obs, double *tmpResult) {

		const int n = nx * ny;

#pragma acc data present(bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(jz, tmpResult)
		{

			double A = 0.0;
			double B = 0.0;

#pragma acc parallel loop collapse(2),reduction(+:A,B)
			for (int j = 1; j < ny - 1; j++) {
				for (int i = 1; i < nx - 1; i++) {
					if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
						continue;
					if (test_isnan(jz[j * nx + i]))
						continue;
					if (test_isnan(bz[j * nx + i]))
						continue;
					if (jz[j * nx + i] == 0.0)
						continue;
					if (bz[j * nx + i] == 0.0)
						continue;
					A += jz[j * nx + i] * bz[j * nx + i];
					B += bz[j * nx + i] * bz[j * nx + i];
				}
			}

			/* Determine the absolute value of alpha. The units for alpha are 1/Mm */
			double C = ((1 / cdelt1) * (rsun_obs / rsun_ref) * (1000000.0));
			double mean_alpha = ((A / B) * C);
#pragma acc region
			{
				tmpResult[0] = mean_alpha;
			}
		}

	}

	void computeHelicity(double *bz, double *jz, double *mask, double *bitmask,
			const int nx, const int ny, const double cdelt1,
			const double rsun_ref, const double rsun_obs, double *tmpResult) {

		const int n = nx * ny;

#pragma acc data present(bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(jz, tmpResult)
		{

			int count_mask = 0;
			double sum = 0.0;
			double sum2 = 0.0;

#pragma acc parallel loop reduction(+:sum),reduction(+:sum2),reduction(+:count_mask)
			for (int i = 0; i < n; i++) {

				if (mask[i] < 70 || bitmask[i] < 30)
					continue;
				if (test_isnan(jz[i]))
					continue;
				if (test_isnan(bz[i]))
					continue;
				if (bz[i] == 0.0)
					continue;
				if (jz[i] == 0.0)
					continue;

				sum += (jz[i] * bz[i]) * (1 / cdelt1) * (rsun_obs / rsun_ref); // contributes to MEANJZH and ABSNJZH
				sum2 += fabs(jz[i] * bz[i]) * (1 / cdelt1)
						* (rsun_obs / rsun_ref); // contributes to TOTUSJH

				count_mask++;

			}

			double tmpMeanJZH = sum / count_mask;
			double tmpTotusJZH = sum2;
			double absJZH = fabs(sum);
#pragma acc region
			{
				tmpResult[0] = tmpMeanJZH; /* Units are G^2 / m ; keyword is MEANJZH */
				tmpResult[1] = tmpTotusJZH; /* Units are G^2 / m ; keyword is TOTUSJH */
				tmpResult[2] = absJZH; /* Units are G^2 / m ; keyword is ABSNJZH */
			}

		}

	}

	void computeSumAbsPerPolarity(double *bz, double *jz, double *mask,
			double *bitmask, const int nx, const int ny, const double cdelt1,
			const double rsun_ref, const double rsun_obs, double *tmpResult) {

		const int n = nx * ny;

#pragma acc data present(bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc declare deviceptr(jz,  tmpResult)
		{

			double sum1 = 0.0;
			double sum2 = 0.0;

#pragma acc parallel loop reduction(+:sum1,sum2)
			for (int i = 0; i < n; i++) {

				if (mask[i] < 70 || bitmask[i] < 30)
					continue;
				if (test_isnan(bz[i]))
					continue;
				if (test_isnan(jz[i]))
					continue;

				if (bz[i] > 0)
					sum1 += (jz[i]) * (1 / cdelt1) * (0.00010) * (1 / MUNAUGHT)
							* (rsun_ref / rsun_obs);
				if (bz[i] <= 0)
					sum2 += (jz[i]) * (1 / cdelt1) * (0.00010) * (1 / MUNAUGHT)
							* (rsun_ref / rsun_obs);

			}

			double tmpAMP = fabs(sum1) + fabs(sum2);
#pragma acc region
			{
				tmpResult[0] = tmpAMP; /* Units are Amperes per arcsecond */
			}
		}
	}

#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};

#endif
