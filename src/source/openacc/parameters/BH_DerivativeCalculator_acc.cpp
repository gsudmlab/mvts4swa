/*
 * File: BH_DerivativeCalculator_acc.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 30, 2018
 */

#ifndef BH_DERIVATIVE_ACC_CPP
#define BH_DERIVATIVE_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the MEANGBH parameter
 */
class BH_DerivativeCalculator_acc: public IParamCalculator {

public:

	/*
	 * calculateParameter: returns the MEANGBH parameter in results[0]
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return 0;

		const int n = nx * ny;

		double* bh = (double*) acc_malloc((size_t) n * sizeof(double));
		double* derx_bh = (double*) acc_malloc((size_t) n * sizeof(double));
		double* dery_bh = (double*) acc_malloc((size_t) n * sizeof(double));

#pragma acc data present(bx[0:n], by[0:n], bitmask[0:n], mask[0:n])
#pragma acc declare deviceptr(bh, derx_bh, dery_bh, results)
		{

#pragma acc parallel loop independent
			for (int i = 0; i < n; i++) {

				if (test_isnan(bx[i])) {
					bh[i] = NAN;
					continue;
				}

				if (test_isnan(by[i])) {
					bh[i] = NAN;
					continue;
				}

				bh[i] = sqrt(bx[i] * bx[i] + by[i] * by[i]);
			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
#pragma acc loop independent
				for (int i = 1; i <= nx - 2; i++) {
					derx_bh[j * nx + i] = (bh[j * nx + i + 1]
							- bh[j * nx + i - 1]) * 0.5;
				}
			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma acc parallel loop independent
			for (int j = 1; j <= ny - 2; j++) {
#pragma acc loop independent
				for (int i = 0; i <= nx - 1; i++) {
					dery_bh[j * nx + i] = (bh[(j + 1) * nx + i]
							- bh[(j - 1) * nx + i]) * 0.5;
				}
			}

			/* consider the edges for the arrays that contribute to the variable "sum" in the computation below.
			 ignore the edges for the error terms as those arrays have been initialized to zero.
			 this is okay because the error term will ultimately not include the edge pixels as they are selected out by the mask and bitmask arrays.*/

			const int tmpI1 = 0;
#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
				derx_bh[j * nx + tmpI1] = ((-3 * bh[j * nx + tmpI1])
						+ (4 * bh[j * nx + (tmpI1 + 1)])
						- (bh[j * nx + (tmpI1 + 2)])) * 0.5;
			}

			int tmpI2 = nx - 1;
#pragma acc parallel loop independent
			for (int j = 0; j <= ny - 1; j++) {
				derx_bh[j * nx + tmpI2] = ((3 * bh[j * nx + tmpI2])
						+ (-4 * bh[j * nx + (tmpI2 - 1)])
						- (-bh[j * nx + (tmpI2 - 2)])) * 0.5;
			}

			int tmpJ1 = 0;
#pragma acc parallel loop independent
			for (int i = 0; i <= nx - 1; i++) {
				dery_bh[tmpJ1 * nx + i] = ((-3 * bh[tmpJ1 * nx + i])
						+ (4 * bh[(tmpJ1 + 1) * nx + i])
						- (bh[(tmpJ1 + 2) * nx + i])) * 0.5;
			}

			int tmpJ2 = ny - 1;
#pragma acc parallel loop independent
			for (int i = 0; i <= nx - 1; i++) {
				dery_bh[tmpJ2 * nx + i] = ((3 * bh[tmpJ2 * nx + i])
						+ (-4 * bh[(tmpJ2 - 1) * nx + i])
						- (-bh[(tmpJ2 - 2) * nx + i])) * 0.5;
			}

			int count_mask = 0;
			double sum = 0.0;

#pragma acc parallel loop collapse(2) reduction(+:sum,count_mask)
			for (int j = 0; j <= ny - 1; j++) {
				for (int i = 0; i <= nx - 1; i++) {
					if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
						continue;
					if ((derx_bh[j * nx + i] + dery_bh[j * nx + i]) == 0)
						continue;
					if (test_isnan(bh[j * nx + i]))
						continue;
					if (test_isnan(bh[(j + 1) * nx + i]))
						continue;
					if (test_isnan(bh[(j - 1) * nx + i]))
						continue;
					if (test_isnan(bh[j * nx + i - 1]))
						continue;
					if (test_isnan(bh[j * nx + i + 1]))
						continue;
					if (test_isnan(derx_bh[j * nx + i]))
						continue;
					if (test_isnan(dery_bh[j * nx + i]))
						continue;

					//Compiler doesn't like function calls on variables being reduced
					double result = sqrt(
							derx_bh[j * nx + i] * derx_bh[j * nx + i]
									+ dery_bh[j * nx + i]
											* dery_bh[j * nx + i]); /* Units of Gauss */

					sum += result;
					count_mask++;
				}

			}

			double mean_derivative_bh = (sum) / (count_mask); // would be divided by ((nx-2)*(ny-2)) if shape of count_mask = shape of magnetogram
#pragma acc region
			{
				results[0] = mean_derivative_bh;
			}

		}

		acc_free(bh);
		acc_free(derx_bh);
		acc_free(dery_bh);

		return 1;

	}

private:
#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}

};

#endif
