/*
 * File: LOSTransform_omp.cpp
 * Author: Dustin Kempton
 *
 *
 * This class is meant to perform a line of sight correction
 * for the LOS Magnetogram.
 *
 *
 * Created on October 04, 2018
 */

#ifndef PILFILTER_OMP_CPP
#define PILFILTER_OMP_CPP

#include <cstdlib>
#include <math.h>
#include <stdlib.h>
#include <openacc.h>

class LOSTransform_acc {

private:
	double radsindeg = (3.14159265358979323846264338327950288) / 180.0;
public:

	/**
	 * process expects image to be present
	 */
	void process(double* image, double crval1, double crval2, double crln_obs,
			double crpix1, double crpix2, double cdelt1, double rsun_ref,
			double dsun_obs, int nx, int ny) {

		double globalLon = (crval1 - crln_obs);

		int n = nx * ny;

#pragma acc data present(image[0:n])
		{
#pragma acc parallel loop  collapse(2)
			for (int j = 0; j < ny; j++) {
				for (int i = 0; i < nx; i++) {

					double localLon = ((i - crpix1) * cdelt1 + globalLon)
							* this->radsindeg;
					double localLat = ((j - crpix2) * cdelt1 + crval2)
							* this->radsindeg;

					//The compiler of openACC doesn't like nested function calls.
					//So, we will just break them apart here.
					double sinLocLat = sin(localLat);
					double powSinLocLat = pow(sinLocLat, 2.0);
					double cosLocLat = cos(localLat);
					double sinLocLon = sin(localLon);
					double powCosSinLatLon = pow((cosLocLat * sinLocLon), 2.0);
					double d = sqrt(powSinLocLat + powCosSinLatLon);
					double asinD = asinWorkAround(d);
					double atanDRsun = atan(d * rsun_ref / dsun_obs);
					double losCorrection = cos(asinD + atanDRsun);
					image[j * nx + i] /= losCorrection;
				}
			}
		}
	}

private:

#pragma acc routine seq
	static double asinWorkAround(double val) {
		return asinf((float) val);
	}

};

#endif
