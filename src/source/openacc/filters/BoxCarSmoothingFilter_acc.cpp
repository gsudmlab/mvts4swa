/*
 * File: BoxCarSmoothingFilter_acc.cpp
 * Author: Dustin Kempton
 *
 *
 * Source of this filter has been adapted from:
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *
 * Created on October 01, 2018
 */

#ifndef BOXFILTER_ACC_CPP
#define BOXFILTER_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/filters/IImageFilter.cpp"

class BoxCarSmoothingFilter_acc : public IImageFilter {

    double *kerx;
    double *kery;
    int fwidth;
    double normalizer;

public:

    /**
     * hwidth: is half width of boxcar. Full is 2*hwidth+1.
     */
    BoxCarSmoothingFilter_acc(int hwidth) {

        int fwidth = 2 * hwidth + 1;

        this->kerx = new double[fwidth];
        this->kery = new double[fwidth];

        this->normalizer = fwidth;

        this->fwidth = fwidth;

        for (int i = 0; i < fwidth; i++) {
            this->kerx[i] = 1.0;
            this->kery[i] = 1.0;
        }
    }

    ~BoxCarSmoothingFilter_acc() {
        delete[] kerx;
        delete[] kery;

    }

    /**
     * Process assumes that all arrays passed in are device pointers.
     */
    int process(double *img, double *out_img, int nx, int ny) {

        int fwidth = this->fwidth;
        double *kerX = new double[fwidth];
        double *kerY = new double[fwidth];
        for (int i = 0; i < fwidth; i++) {
            kerX[i] = this->kerx[i];
            kerY[i] = this->kery[i];
        }
        double norm = this->normalizer;

#pragma acc data copyin(kerX[0:fwidth], kerY[0:fwidth])
#pragma acc data deviceptr(img, out_img)
        {
            //for each pixel in output image
#pragma acc parallel loop independent
            for (int y = 0; y < ny; y++) {
#pragma acc loop independent
                for (int x = 0; x < nx; x++) {
                    // compute resampled value
                    double val = this->convolve(img, nx, ny, x, y, kerX, kerY,
                                                norm, fwidth);
                    out_img[y * nx + x] = val;
                }
            }
        }

        delete[] kerX;
        delete[] kerY;

        return 0;
    }

private:

#pragma acc routine seq
    double convolve(double *img, int nx, int ny, int x, int y, double *kerX,
                    double *kerY, double norm, int kernSize) {

        //Assume an odd size
        int halfwindow = kernSize / 2;

        double outValue = 0;
        //Loop over the row and column offset from the pixel we are to
        //put the output into.
#pragma acc data present(kerX[0:kernSize], kerY[0:kernSize])
#pragma acc data deviceptr(img)
        {
            for (int dy = -halfwindow; dy <= halfwindow; dy++) {
                int row = y + dy;

                if ((row < 0 || row >= ny))
                    continue;

                int idxY = (row) * nx;

                double gx = 0;
                //pass 1: horizontal convolution of values
                for (int i = -halfwindow; i <= halfwindow; i++) {
                    int idx = (x + i);
                    if (idx < 0 || idx >= nx)
                        continue;

                    gx += img[idxY + idx] * kerX[halfwindow - i];
                }

                outValue += gx * kerY[halfwindow - dy];

            }
        }

        //normalization;
        double normVal = norm * norm;
        outValue = outValue / normVal;

        return outValue;
    }

};

#endif
