/*
 * File: PIL_Filter_acc.cpp
 * Author: Dustin Kempton
 *
 *
 * Source of this filter has been adapted from:
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *
 * Created on October 03, 2018
 */

#ifndef PILFILTER_ACC_CPP
#define PILFILTER_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/filters/IImageFilter.cpp"

class PIL_Filter_acc: public IImageFilter {

	IImageFilter* boxcarFilter;
	int threshold;

public:

	PIL_Filter_acc(IImageFilter* boxcarFilter, int threshold) {
		this->threshold = threshold;
		this->boxcarFilter = boxcarFilter;
	}

	/**
	 *
	 * Process assumes that img is present and  out_image array is a device pointer.
	 */
	int process(double* img, double* out_img, int nx, int ny) {

		int n = nx * ny;

		double* p1n0 = (double*) acc_malloc((size_t) n * sizeof(double));
		double* p1p0 = (double*) acc_malloc((size_t) n * sizeof(double));
		double* p1p = (double*) acc_malloc((size_t) n * sizeof(double));
		double* p1n = (double*) acc_malloc((size_t) n * sizeof(double));

#pragma acc data present(img[0:n])
#pragma acc declare deviceptr(p1n0, p1p0, p1p, p1n, out_img)
		{

			//................[Step 1]..................
			//Identify positive and negative pixels greater than +/- threshold gauss
			//Label those pixels with a 1.0 in arrays p1p0 and p1n0 respectively
#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
#pragma acc loop independent
				for (int i = 0; i < nx; i++) {
					int index = j * nx + i;
					if (img[index] > this->threshold)
						p1p0[index] = 1.0;
					else
						p1p0[index] = 0.0;

					if (img[index] < -this->threshold)
						p1n0[index] = 1.0;
					else
						p1n0[index] = 0.0;
				}
			}

			//................[Step 2]..................
			// smooth each of the negative and positive pixel bitmaps
			this->boxcarFilter->process(p1p0, p1p, nx, ny);
			this->boxcarFilter->process(p1n0, p1n, nx, ny);

			// =============== [STEP 3] ===============
			// find the pixels for which p1p and p1n are both equal to 1.
			// this defines the polarity inversion line
#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
#pragma acc loop independent
				for (int i = 0; i < nx; i++) {
					int index = j * nx + i;
					if ((p1p[index] > 0.0) && (p1n[index] > 0.0))
						out_img[index] = 1.0;
					else
						out_img[index] = 0.0;
				}
			}
		}

		acc_free(p1p0);
		acc_free(p1n0);
		acc_free(p1p);
		acc_free(p1n);

		return 0;
	}
};

#endif
