/*
 * File: GaussianSmoothingFilter_acc.cpp
 * Author: Dustin Kempton
 *
 *
 * Source of this filter has been adapted from:
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *
 * Created on October 01, 2018
 */
#ifndef GAUSSIANFILTER_ACC_CPP
#define GAUSSIANFILTER_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/filters/IImageFilter.cpp"

class GaussianSmoothingFilter_acc: public IImageFilter {

	double *kerx;
	double *kery;
	int hwidth;
	int fwidth;

public:
	/**
	 * hwidth: is half width of the filter. Full is 2*hwidth+1.
	 * Shape is exp(-(d/sigma)^2/2)
	 *
	 */
	GaussianSmoothingFilter_acc(int hwidth, double sigma) {
		this->hwidth = hwidth;
		int fwidth = 2 * hwidth + 1;

		this->kerx = new double[fwidth];
		this->kery = new double[fwidth];

		this->fwidth = fwidth;

		double sum = 0.0;
		for (int i = 0; i < fwidth; i++) {
			double x = (i - hwidth) / sigma;
			double y = exp(-x * x / 2);

			sum += y;
		}
		for (int i = 0; i < fwidth; i++) {
			double x = (i - hwidth) / sigma;
			double y = exp(-x * x / 2);

			this->kerx[i] = y / sum;
			this->kery[i] = y / sum;
		}
	}

	~GaussianSmoothingFilter_acc() {

		delete[] kerx;
		delete[] kery;
	}

	/**
	 * Process assumes that all arrays passed in are device pointers.
	 */
	int process(double *img, double *out_img, int nx, int ny) {

		int fwidth = this->fwidth;
		double *kerX = new double[fwidth];
		double *kerY = new double[fwidth];
		for (int i = 0; i < fwidth; i++) {
			kerX[i] = this->kerx[i];
			kerY[i] = this->kery[i];
		}

#pragma acc data copyin(kerX[0:fwidth], kerY[0:fwidth])
#pragma acc data deviceptr(img, out_img)
		{
			//for each pixel in output image
#pragma acc parallel loop independent
			for (int y = 0; y < ny; y++) {
#pragma acc loop independent
				for (int x = 0; x < nx; x++) {
					double val = this->convolve(img, nx, ny, x, y, kerX, kerY,
							fwidth);
					out_img[y * nx + x] = val;
				}
			}
		}

		delete[] kerX;
		delete[] kerY;

		return 0;
	}

private:

#pragma acc routine seq
	double convolve(double *img, int nx, int ny, int x, int y, double *kerX,
			double *kerY, int kernSize) {

		const int halfwindow = kernSize / 2;

		double outValue = 0;
		//Loop over the row and column offset from the pixel we are to
		//put the output into.
#pragma acc data present(kerX[0:kernSize], kerY[0:kernSize])
#pragma acc data deviceptr(img)
		{
			for (int dy = -halfwindow, dx = -halfwindow; dy <= halfwindow;
					dy++, dx++) {
				int row = y + dy;
				int col = x + dx;
				if ((row < 0 || row >= ny) || (col < 0 || col >= nx))
					continue;

				int idxY = (row) * nx;

				double gx = 0;
				//pass 1: horizontal convolution of values
				for (int i = -halfwindow; i <= halfwindow; i++) {
					int idx = (x + i);
					if (idx < 0 || idx >= nx)
						continue;

					gx += img[idxY + idx] * kerX[halfwindow - i];
				}

				outValue += gx * kerY[halfwindow - dy];
			}
		}

		return outValue;
	}
};

#endif
