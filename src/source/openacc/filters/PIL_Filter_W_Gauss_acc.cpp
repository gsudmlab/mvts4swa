/*
 * File: PIL_Filter_W_Gauss_acc.cpp
 * Author: Dustin Kempton
 *
 *
 * Source of this filter has been adapted from:
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *
 * Created on April 23, 2019
 */

#ifndef PILFILTER_W_GAUSS_ACC_CPP
#define PILFILTER_W_GAUSS_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include <iostream>
using namespace std;

#include "../../include/filters/IImageFilter.cpp"

/**
 * This class will be used to produce the Gaussian Filter over the PIL
 * used in all of the PIR classes.  The output image will be the map of weights.
 */
class PIL_Filter_W_Gauss_acc: public IImageFilter {

	IImageFilter* boxcarFilter;
	IImageFilter* gaussianFilter;
	int threshold;
	int gaussSize;

public:

	PIL_Filter_W_Gauss_acc(IImageFilter* boxcarFilter,
			IImageFilter* gaussianFilter, int threshold, int gaussSize) {
		this->threshold = threshold;
		this->gaussSize = gaussSize;
		this->boxcarFilter = boxcarFilter;
		this->gaussianFilter = gaussianFilter;
	}

	/**
	 *
	 * Process assumes that img is present and  out_image array is a device pointer.
	 */
	int process(double* img, double* out_img, int nx, int ny) {

		int n = nx * ny;

		int nxp = nx + (this->gaussSize * 2);
		int nyp = ny + (this->gaussSize * 2);

		double* p1n0 = (double*) acc_malloc((size_t) n * sizeof(double));
		double* p1p0 = (double*) acc_malloc((size_t) n * sizeof(double));
		double* p1p = (double*) acc_malloc((size_t) n * sizeof(double));
		double* p1n = (double*) acc_malloc((size_t) n * sizeof(double));
		double* tmpPilImg = (double*) acc_malloc((size_t) n * sizeof(double));

		double* pmap = (double*) acc_malloc(
				(size_t) nxp * nyp * sizeof(double));
		double* p1pad = (double*) acc_malloc(
				(size_t) nxp * nyp * sizeof(double));

		double tmpResults[1] = { 0 };

#pragma acc data present(img[0:n])
#pragma acc data create(tmpResults[0:1])
#pragma acc declare deviceptr(p1n0, p1p0, p1p, p1n, pmap, p1pad, tmpPilImg, out_img)
		{

			//................[Step 1]..................
			//Identify positive and negative pixels greater than +/- threshold gauss
			//Label those pixels with a 1.0 in arrays p1p0 and p1n0 respectively
#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
#pragma acc loop independent
				for (int i = 0; i < nx; i++) {
					int index = j * nx + i;
					if (img[index] > this->threshold)
						p1p0[index] = 1.0;
					else
						p1p0[index] = 0.0;

					if (img[index] < -this->threshold)
						p1n0[index] = 1.0;
					else
						p1n0[index] = 0.0;
				}
			}

			//................[Step 2]..................
			// smooth each of the negative and positive pixel bitmaps
			this->boxcarFilter->process(p1p0, p1p, nx, ny);
			this->boxcarFilter->process(p1n0, p1n, nx, ny);

			// =============== [STEP 3] ===============
			// find the pixels for which p1p and p1n are both equal to 1.
			// this defines the polarity inversion line
#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
#pragma acc loop independent
				for (int i = 0; i < nx; i++) {
					int index = j * nx + i;
					if ((p1p[index] > 0.0) && (p1n[index] > 0.0))
						tmpPilImg[index] = 1.0;
					else
						tmpPilImg[index] = 0.0;
				}
			}

			int sum = 0;
			//Count the non-zero values to return as the pil length.
#pragma acc parallel loop collapse(2), reduction(+:sum)
			for (int j = 0; j < ny; j++) {
				for (int i = 0; i < nx; i++) {
					int index = j * nx + i;
					if (test_isnan(tmpPilImg[index]))
						continue;
					if (tmpPilImg[index] > 0)
						sum += 1;
				}
			}

#pragma acc region
			{
				tmpResults[0] = sum;
			}

			// step i: zero p1pad
#pragma acc parallel loop independent
			for (int j = 0; j < nyp; j++) {
#pragma acc loop independent
				for (int i = 0; i < nxp; i++) {
					int index = j * nxp + i;
					p1pad[index] = 0.0;
				}
			}

			// step ii: place p1 at the center of p1pad
#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
#pragma acc loop independent
				for (int i = 0; i < nx; i++) {
					int index = j * nx + i;
					int index1 = (j + this->gaussSize) * nxp
							+ (i + this->gaussSize);
					p1pad[index1] = tmpPilImg[index];
				}
			}

			this->gaussianFilter->process(p1pad, pmap, nxp, nyp);

			// select out the nx1 x ny1 non-padded array  within pmap
			// and place it into the output image.
#pragma acc parallel loop independent
			for (int j = 0; j < ny; j++) {
#pragma acc loop independent
				for (int i = 0; i < nx; i++) {
					int index = j * nx + i;
					int index1 = (j + this->gaussSize) * nxp
							+ (i + this->gaussSize);
					out_img[index] = pmap[index1];
				}
			}
			//update the host before we leave the acc data region.
#pragma acc update host(tmpResults[0:1])
		}

		acc_free(p1p0);
		acc_free(p1n0);
		acc_free(p1p);
		acc_free(p1n);
		acc_free(tmpPilImg);
		acc_free(pmap);
		acc_free(p1pad);

		return tmpResults[0];
	}

#pragma acc routine seq
	static bool test_isnan(double val) {
		return val != val;
	}
};

#endif
