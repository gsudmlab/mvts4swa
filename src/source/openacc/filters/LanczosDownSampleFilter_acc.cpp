/*
 * File: LanczosFilter_acc.cpp
 * Author: Dustin Kempton
 *
 * Source of this filter has been adapted from:
 * http://subversion.developpez.com/projets/Millie/trunk/MillieCoreFilter/src/millie/plugins/core/transform/LanczosResamplePlugin.java
 *
 * Created on July 31, 2018
 */

#ifndef LANFILTER_ACC_CPP
#define LANFILTER_ACC_CPP

#include <math.h>
#include <stdlib.h>
#include <openacc.h>

#include "../../include/filters/IImageFilter.cpp"

using namespace std;

class LanczosDownSampleFilter_acc : public IImageFilter {
private:
    double pi = (3.14159265358979323846264338327950288);
    int minsupport = 3;
    double scaleFactor = 1.0;

    struct Kernel1D {
    public:
        int *coeffs = nullptr;
        int size = 0;
        int normalizer = 0;
    };

public:

    /**
     * Scale is percent where 100.0 is 100%
     */
    LanczosDownSampleFilter_acc(double scale) {
        this->scaleFactor = scale * 0.01;
    }

    ~LanczosDownSampleFilter_acc() {

    }

    /**
     * Process expects image to be present and out_img to be a
     * device pointer.
     */
    int process(double *img, double *out_img, int nx, int ny) {

        int n = nx * ny;
        // compute support size = how many source pixels for 1 sampled pixel ?
        int support = (int) (1 + 1.0 / this->scaleFactor);
        // minimum support
        if (support < this->minsupport)
            support = this->minsupport;
        // support size must be odd
        if (support % 2 == 0)
            support++;

        int numKern = 100;
        int **kernelsYArr = new int *[numKern];
        int **kernelsXArr = new int *[numKern];
        for (int i = 0; i < numKern; ++i) {
            kernelsYArr[i] = nullptr;
            kernelsXArr[i] = nullptr;
        }
        int *kernelnormY = new int[numKern];
        int *kernelnormX = new int[numKern];

        int newwidth = (int) ((nx * this->scaleFactor) + 0.5);
        int newheight = (int) ((ny * this->scaleFactor) + 0.5);

        //Initialize the kernel y
        for (int y = 0; y < newheight; y++) {
            // ideal sample point in the source image
            double yo = y / this->scaleFactor;

            // separate integer part and fractional part
            int y_int = (int) yo;
            double y_frac = yo - y_int;
            int kid = (int) (y_frac * 100);

            if (kernelsYArr[kid] == nullptr) {
                Kernel1D ky = this->precompute(this->scaleFactor, y_frac);
                kernelsYArr[kid] = ky.coeffs;
                kernelnormY[kid] = ky.normalizer;
            }
        }

        //Initialize the kernel x
        for (int x = 0; x < newwidth; x++) {
            // ideal sample point in the source image
            double xo = x / this->scaleFactor;

            // separate integer part and fractional part
            int x_int = (int) xo;
            double x_frac = xo - x_int;
            int kid = (int) (x_frac * 100);

            if (kernelsXArr[kid] == nullptr) {
                Kernel1D kx = this->precompute(this->scaleFactor, x_frac);
                kernelsXArr[kid] = kx.coeffs;
                kernelnormX[kid] = kx.normalizer;
            }
        }

        for (int i = 0; i < numKern; ++i) {
            if (kernelsYArr[i] == nullptr) {
                kernelsYArr[i] = new int[support];
            }

            if (kernelsXArr[i] == nullptr) {
                kernelsXArr[i] = new int[support];
            }
        }

#pragma acc data copyin(kernelsYArr[0:numKern][0:support], kernelsXArr[0:numKern][0:support])
#pragma acc data copyin(kernelnormY[0:numKern], kernelnormX[0:numKern])
#pragma acc data present(img[0:n])
#pragma acc declare deviceptr(out_img)
        {
            //for each pixel in output image
#pragma acc parallel loop independent
            for (int y = 0; y < newheight; y++) {
#pragma acc loop independent
                for (int x = 0; x < newwidth; x++) {

                    // ideal sample point in the source image
                    double xo = x / this->scaleFactor;
                    double yo = y / this->scaleFactor;

                    // separate integer part and fractionnal part
                    int x_int = (int) xo;
                    double x_frac = xo - x_int;
                    int y_int = (int) yo;
                    double y_frac = yo - y_int;

                    // get/compute resampling Kernels
                    int kidX = (int) (x_frac * numKern);
                    int *kx = kernelsXArr[kidX];
                    int xNorm = kernelnormX[kidX];

                    int kidY = (int) (y_frac * numKern);
                    int *ky = kernelsYArr[kidY];
                    int yNorm = kernelnormY[kidY];

                    // compute resampled value
                    double val = this->convolve(img, nx, ny, x_int, y_int, kx,
                                                ky, xNorm, yNorm, support);

                    out_img[y * newwidth + x] = val;
                }
            }
        }

        for (int i = 0; i < numKern; ++i) {
            delete[] kernelsYArr[i];
            delete[] kernelsXArr[i];
        }
        delete[] kernelsYArr;
        delete[] kernelsXArr;
        delete[] kernelnormY;
        delete[] kernelnormX;

        return 0;
    }

private:

    /**
     *  Lanczos function: sinc(d.pi)*sinc(d.pi/support)
     *
     * @param support,d input parameters
     * @return value of the function
     */
    double lanczos(double support, double d) {
        if (d == 0)
            return 1.0;
        if (d >= support)
            return 0.0;
        double t = d * pi;
        return support * sin(t) * sin(t / support) / (t * t);
    }

    /**
     * Compute a Lanczos resampling kernel
     *
     * @param scale scale to apply on the original image
     * @param frac fractionnal part of ideal sample point
     * @return the Lanczos resampling kernel
     */
    Kernel1D precompute(double scaleVal, double frac) {

        // compute support size = how many source pixels for 1 sampled pixel ?
        int support = (int) (1 + 1.0 / scaleVal);
        // minimum support
        if (support < this->minsupport)
            support = this->minsupport;
        // support size must be odd
        if (support % 2 == 0)
            support++;

        // scale limiter (minimum unit = 1 pixel)
        scaleVal = scaleVal < 1.0 ? scaleVal : 1.0;

        // construct an empty kernel
        Kernel1D kernel = Kernel1D();
        kernel.size = support;
        kernel.coeffs = new int[support];

        int i = 0;
        int halfwindow = support / 2;
        for (int dx = -halfwindow; dx <= halfwindow; dx++) {

            // ideal sample points (in the source image)
            double x = scaleVal * (dx + frac);

            // corresponding weight (=contribution) of closest source pixel
            double coef = this->lanczos(halfwindow, x);

            // store to kernel
            int c = (int) (1000 * coef + 0.5);
            kernel.coeffs[i++] = c;
            kernel.normalizer += c;
        }

        return kernel;
    }

#pragma acc routine seq
    double convolve(double *img, int nx, int ny, int x, int y, int *kernelX,
                    int *kernelY, int kXNorm, int kYNorm, int kSize) {

        int halfwindow = kSize / 2;
        int n = nx * ny;
        double outValue = 0;
        //Loop over the row and column offset from the pixel we are to
        //put the output into.
#pragma acc data present(kernelX[0:kSize], kernelY[0:kSize], img[0:n])
        {
            for (int dy = -halfwindow; dy <= halfwindow; dy++) {
                int row = y + dy;
                if ((row < 0 || row >= ny))
                    continue;

                int idxY = (row) * nx;

                double gx = 0;
                //pass 1: horizontal convolution of values
                for (int i = -halfwindow; i <= halfwindow; i++) {
                    int idx = (x + i);
                    if (idx < 0 || idx >= nx)
                        continue;

                    gx += img[idxY + idx] * kernelX[halfwindow - i];
                }

                outValue += gx * kernelY[halfwindow - dy];

            }
        }

        //normalization;
        double norm = kXNorm * kYNorm;
        outValue = outValue / norm;

        return outValue;
    }

};

#endif /* LANFILTER_OMP_CPP */
