/*
 * File: SolarFeatureCalc_acc.cpp
 * Author: Dustin Kempton
 *
 * Created on July 30, 2018
 */

#ifndef FEATURE_CALC_ACC_CPP
#define FEATURE_CALC_ACC_CPP

#include "parameters/AbsoluteFluxCalculator_acc.cpp"
#include "parameters/GammaCalculator_acc.cpp"
#include "parameters/B_DerivativeCalculator_acc.cpp"
#include "parameters/LorentzCalculator_acc.cpp"
#include "parameters/BH_DerivativeCalculator_acc.cpp"
#include "parameters/BZ_DerivativeCalculator_acc.cpp"
#include "parameters/JZ_Calculator_acc.cpp"
#include "parameters/GreenPotCalculator_acc.cpp"
#include "parameters/R_ValueCalculator_acc.cpp"
#include "parameters/FractalDimCalculator_acc.cpp"
#include "parameters/PIR_AbsoluteFluxCalculator_acc.cpp"
#include "parameters/PIR_GammaCalculator_acc.cpp"
#include "parameters/PIR_B_DerivativeCalculator_acc.cpp"
#include "parameters/PIR_LorentzCalculator_acc.cpp"
#include "parameters/PIR_BH_DerivativeCalculator_acc.cpp"
#include "parameters/PIR_BZ_DerivativeCalculator_acc.cpp"
#include "parameters/PIR_JZ_Calculator_acc.cpp"
#include "parameters/PIR_GreenPotCalculator_acc.cpp"
#include "filters/BoxCarSmoothingFilter_acc.cpp"
#include "filters/GaussianSmoothingFilter_acc.cpp"
#include "filters/AverageDownSampleFilter_acc.cpp"
#include "filters/LanczosDownSampleFilter_acc.cpp"
#include "filters/PIL_Filter_acc.cpp"
#include "filters/PIL_Filter_W_Gauss_acc.cpp"
#include "transforms/LOSTransform_acc.cpp"

#include <math.h>
#include <openacc.h>
#include <mutex>

#include "../include/filters/IImageFilter.cpp"
#include "../include/parameters/IParamCalculator.cpp"
using namespace std;

IParamCalculator** calculators = 0;
IParamCalculator** weightedCalculators = 0;

LOSTransform_acc* losCorrection = 0;
IImageFilter* PIR_PILFilter = 0;

int dev;
int numDevices;
std::mutex mtx;
double radsindeg = (3.14159265358979323846264338327950288) / 180.0;
double scale = 25;
double sigma = 10.0 / 2.3548;
double pir_sigma = (10.0 / 2.3548) * 4;
int hwidthBoxCar = 1;
int hwidthGauss = 20;
int pir_hwidthGauss = 80;
int pilThreshold = 50;
int pir_pilThreshold = 150;

extern "C" double* computeRegularParams(double* bx, double* by, double* bz,
		double* mask, double* bitmask, double* los, double cdelt1,
		double crpix1, double crpix2, double crval1, double crval2,
		double rsun_ref, double rsun_obs, double dsun_obs, double crln_obs,
		int nx, int ny);

extern "C" double* computePIRParams(double* bx, double* by, double* bz,
		double* mask, double* bitmask, double* los, double cdelt1,
		double crpix1, double crpix2, double crval1, double crval2,
		double rsun_ref, double rsun_obs, double dsun_obs, double crln_obs,
		int nx, int ny);

extern "C" void init_lib();

extern "C" void computeSolarFeatures(double* bx, double* by, double* bz,
		double* mask, double* bitmask, double* los, double cdelt1,
		double crpix1, double crpix2, double crval1, double crval2,
		double rsun_ref, double rsun_obs, double dsun_obs, double crln_obs,
		int nx, int ny, double* out_solar_features);

void init_lib() {

	if (!calculators || !weightedCalculators) {

		///////////////////////////////////////////
		//Init for the regular param calculation classes
		///////////////////////////////////////////
		calculators = new IParamCalculator*[10];
		calculators[0] = new AbsoluteFluxCalculator_acc();
		calculators[1] = new GammaCalculator_acc();
		calculators[2] = new BH_DerivativeCalculator_acc();
		calculators[3] = new LorentzCalculator_acc();
		calculators[4] = new B_DerivativeCalculator_acc();
		calculators[5] = new BZ_DerivativeCalculator_acc();
		calculators[6] = new JZ_Calculator_acc();
		calculators[7] = new GreenPotCalculator_acc();

		IImageFilter* averDownFilter = new AverageDownSampleFilter_acc(scale);
		IImageFilter* lanzDownFilter = new LanczosDownSampleFilter_acc(scale);
		IImageFilter* boxcarFilter = new BoxCarSmoothingFilter_acc(
				hwidthBoxCar);
		IImageFilter* gaussianFilter = new GaussianSmoothingFilter_acc(
				hwidthGauss, sigma);

		calculators[8] = new R_ValueCalculator_acc(averDownFilter,
				lanzDownFilter, boxcarFilter, gaussianFilter, scale);

		IImageFilter* PILFilter = new PIL_Filter_acc(boxcarFilter,
				pilThreshold);
		calculators[9] = new FractalDimCalculator_acc(PILFilter);

		losCorrection = new LOSTransform_acc();

		////////////////////////////////////////////
		//Init for the PIR calculations classes.
		///////////////////////////////////////////
		weightedCalculators = new IParamCalculator*[8];
		weightedCalculators[0] = new PIR_AbsoluteFluxCalculator_acc();
		weightedCalculators[1] = new PIR_GammaCalculator_acc();
		weightedCalculators[2] = new PIR_BH_DerivativeCalculator_acc();
		weightedCalculators[3] = new PIR_LorentzCalculator_acc();
		weightedCalculators[4] = new PIR_B_DerivativeCalculator_acc();
		weightedCalculators[5] = new PIR_BZ_DerivativeCalculator_acc();
		weightedCalculators[6] = new PIR_JZ_Calculator_acc();
		weightedCalculators[7] = new PIR_GreenPotCalculator_acc();

		IImageFilter* pir_gaussianFilter = new GaussianSmoothingFilter_acc(
				pir_hwidthGauss, pir_sigma);

		PIR_PILFilter = new PIL_Filter_W_Gauss_acc(boxcarFilter,
				pir_gaussianFilter, pir_pilThreshold, pir_hwidthGauss);

		dev = 0;
		numDevices = acc_get_num_devices(acc_device_nvidia);
		//cout << "Config Devs: " << numDevices << endl;
	}
}

/**
 * Computs and retuns the solar features in this order:
 * 	[USFLUX, MEANGAM, MEANGBH, EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY, TOTFZ
 * 	MEANGBT, MEANGBZ, TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH, ABSNJZH, SAVNCPP
 * 	TOTPOT, MEANPOT, SHRGT45, MEANSHR, R_VALUE, RBZ_VALUE, RBT_VALUE, RBP_VALUE,
 * 	FDIM, BZ_FDIM, BT_FDIM, BP_FDIM, PIR_USFLUX, PIR_MEANGAM, PIR_MEANGBH, PIR_EPSX,
 * 	PIR_EPSY, PIR_EPSZ, PIR_TOTBSQ, PIR_TOTFX, PIR_TOTFY, PIR_TOTFZ,
 * 	PIR_MEANGBT, PIR_MEANGBZ, PIR_TOTUSJZ, PIR_MEANJZD, PIR_MEANALP, PIR_MEANJZH,
 * 	PIR_TOTUSJH, PIR_ABSNJZH, PIR_SAVNCPP, PIR_TOTPOT, PIR_MEANPOT, PIR_SHRGT45,
 * 	PIR_MEANSHR, PIL_LEN]
 */
void computeSolarFeatures(double* bx, double* by, double* bz, double* mask,
		double* bitmask, double* los, double cdelt1, double crpix1,
		double crpix2, double crval1, double crval2, double rsun_ref,
		double rsun_obs, double dsun_obs, double crln_obs, int nx, int ny,
		double* out_solar_features) {

	mtx.lock();
	dev = (dev + 1) % numDevices;
	int numHolder = dev;
	acc_set_device_num(numHolder, acc_device_nvidia);
	mtx.unlock();

	double* results1;
	double* results2;

	int n = nx * ny;

#pragma acc enter data copyin(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n], los[0:n])

	results1 = computeRegularParams(bx, by, bz, mask, bitmask, los, cdelt1,
			crpix1, crpix2, crval1, crval2, rsun_ref, rsun_obs, dsun_obs,
			crln_obs, nx, ny);

	results2 = computePIRParams(bx, by, bz, mask, bitmask, los, cdelt1, crpix1,
			crpix2, crval1, crval2, rsun_ref, rsun_obs, dsun_obs, crln_obs, nx,
			ny);

#pragma acc exit data delete(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n], los[0:n])

	int nResult1 = 31;
	for (int i = 0; i < nResult1; i++) {
		out_solar_features[i] = results1[i];
	}

	int nResult2 = 24;
	for (int i = 0; i < nResult2; i++) {
		out_solar_features[i + nResult1] = results2[i];
	}

	delete[] results1;
	delete[] results2;

}

/**
 * Computes and returns the solar features in this order:
 * 	[USFLUX, MEANGAM, MEANGBH, EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY, TOTFZ
 * 	MEANGBT, MEANGBZ, TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH, ABSNJZH, SAVNCPP
 * 	TOTPOT, MEANPOT, SHRGT45, MEANSHR, R_VALUE, RBZ_VALUE, RBT_VALUE, RBP_VALUE,
 * 	FDIM, BZ_FDIM, BT_FDIM, BP_FDIM] 31 of them
 */
double* computeRegularParams(double* bx, double* by, double* bz, double* mask,
		double* bitmask, double* los, double cdelt1, double crpix1,
		double crpix2, double crval1, double crval2, double rsun_ref,
		double rsun_obs, double dsun_obs, double crln_obs, int nx, int ny) {

	//convert cdelt1 from degrees to arcsec
	double convertedCdelt1 =
			(atan((rsun_ref * cdelt1 * radsindeg) / (dsun_obs)))
					* (1 / radsindeg) * (3600.0);

	const int n = nx * ny;

	double tmpResults[31] = { 0 };

	double* results = (double*) acc_malloc((size_t) 7 * sizeof(double));

#pragma acc data present(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n], los[0:n])
#pragma acc data create(tmpResults[0:31])
#pragma acc declare deviceptr(results)
	{
		losCorrection->process(los, crval1, crval2, crln_obs, crpix1, crpix2,
				cdelt1, rsun_ref, dsun_obs, nx, ny);

		//Calculate USFLUX
		calculators[0]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[0] = results[0];
		}

		//Calculate MEANGAM
		calculators[1]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[1] = results[0];
		}

		//Calculate MEANGBH
		calculators[2]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[2] = results[0];
		}

		//Calculate EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY, TOTFZ
		calculators[3]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[3] = results[0]; // EPSX
			tmpResults[4] = results[1]; // EPSY
			tmpResults[5] = results[2]; // EPSZ
			tmpResults[6] = results[3]; // TOTBSQ
			tmpResults[7] = results[4]; // TOTFX
			tmpResults[8] = results[5]; // TOTFY
			tmpResults[9] = results[6]; // TOTFZ
		}

		//Calculate MEANGBT
		calculators[4]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[10] = results[0];
		}

		//Calculate MEANGBZ
		calculators[5]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[11] = results[0];
		}

		//Calculate TOTUSJZ MEANJZD MEANALP MEANJZH TOTUSJH ABSNJZH SAVNCPP
		calculators[6]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[12] = results[0]; // TOTUSJZ
			tmpResults[13] = results[1]; // MEANJZD
			tmpResults[14] = results[2]; // MEANALP
			tmpResults[15] = results[3]; // MEANJZH
			tmpResults[16] = results[4]; // TOTUSJH
			tmpResults[17] = results[5]; // ABSNJZH
			tmpResults[18] = results[6]; // SAVNCPP
		}

		//Calculate TOTPOT MEANPOT SHRGT45 MEANSHR
		calculators[7]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[19] = results[0]; // TOTPOT
			tmpResults[20] = results[1]; // MEANPOT
			tmpResults[21] = results[2]; // SHRGT45
			tmpResults[22] = results[3]; // MEANSHR
		}

		//Calculate R_VALUE, RBZ_VALUE, RBT_VALUE, RBP_VALUE
		calculators[8]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[23] = results[0]; // R_VALUE
			tmpResults[24] = results[1]; // RBZ_VALUE
			tmpResults[25] = results[2]; // RBT_VALUE
			tmpResults[26] = results[3]; // RBP_VALUE
		}

		//Calculate FDIM, BZ_FDIM, BT_FDIM, BP_FDIM
		calculators[9]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[27] = results[0]; // FDIM
			tmpResults[28] = results[1]; // BZ_FDIM
			tmpResults[29] = results[2]; // BT_FDIM
			tmpResults[30] = results[3]; // BP_FDIM
		}

		//update the host before we leave the acc data region.
#pragma acc update host(tmpResults[0:31])
	}

	acc_free(results);

	double* out_solar_features = new double[31];
	for (int i = 0; i < 31; i++) {
		out_solar_features[i] = tmpResults[i];
	}

	return out_solar_features;
}

/**
 * Computes and returns the solar features in this order:
 * 	[PIR_USFLUX, PIR_MEANGAM, PIR_MEANGBH, PIR_EPSX, PIR_EPSY, PIR_EPSZ, PIR_TOTBSQ,
 * 	PIR_TOTFX, PIR_TOTFY, PIR_TOTFZ, PIR_MEANGBT, PIR_MEANGBZ, PIR_TOTUSJZ, PIR_MEANJZD,
 * 	PIR_MEANALP, PIR_MEANJZH, PIR_TOTUSJH, PIR_ABSNJZH, PIR_SAVNCPP, PIR_TOTPOT,
 * 	PIR_MEANPOT, PIR_SHRGT45, PIR_MEANSHR, PIL_LEN] 24 of them
 */
double* computePIRParams(double* bx, double* by, double* bz, double* mask,
		double* bitmask, double* los, double cdelt1, double crpix1,
		double crpix2, double crval1, double crval2, double rsun_ref,
		double rsun_obs, double dsun_obs, double crln_obs, int nx, int ny) {

	//convert cdelt1 from degrees to arcsec
	double convertedCdelt1 =
			(atan((rsun_ref * cdelt1 * radsindeg) / (dsun_obs)))
					* (1 / radsindeg) * (3600.0);

	const int n = nx * ny;

	double tmpResults[24] = { 0 };

	double* pil_img = (double*) acc_malloc((size_t) n * sizeof(double));
	double* results = (double*) acc_malloc((size_t) 7 * sizeof(double));

#pragma acc data present(bx[0:n], by[0:n], bz[0:n], mask[0:n], bitmask[0:n])
#pragma acc data create(tmpResults[0:24])
#pragma acc declare deviceptr(results, pil_img)
	{
		int pilLen = PIR_PILFilter->process(bz, pil_img, nx, ny);
#pragma acc region
		{
			tmpResults[23] = pilLen;
		}
		//Calculate PIR_USFLUX
		weightedCalculators[0]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[0] = results[0];
		}

		//Calculate PIR_MEANGAM
		weightedCalculators[1]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[1] = results[0];
		}

		//Calculate PIR_MEANGBH
		weightedCalculators[2]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[2] = results[0];
		}

		//Calculate PIR_EPSX, PIR_EPSY, PIR_EPSZ, PIR_TOTBSQ, PIR_TOTFX, PIR_TOTFY, PIR_TOTFZ
		weightedCalculators[3]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[3] = results[0]; // PIR_EPSX
			tmpResults[4] = results[1]; // PIR_EPSY
			tmpResults[5] = results[2]; // PIR_EPSZ
			tmpResults[6] = results[3]; // PIR_TOTBSQ
			tmpResults[7] = results[4]; // PIR_TOTFX
			tmpResults[8] = results[5]; // PIR_TOTFY
			tmpResults[9] = results[6]; // PIR_TOTFZ
		}

		//Calculate PIR_MEANGBT
		weightedCalculators[4]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[10] = results[0];
		}

		//Calculate PIR_MEANGBZ
		weightedCalculators[5]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[11] = results[0];
		}

		//Calculate PIR_TOTUSJZ PIR_MEANJZD PIR_MEANALP PIR_MEANJZH PIR_TOTUSJH PIR_ABSNJZH PIR_SAVNCPP
		weightedCalculators[6]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[12] = results[0]; // PIR_TOTUSJZ
			tmpResults[13] = results[1]; // PIR_MEANJZD
			tmpResults[14] = results[2]; // PIR_MEANALP
			tmpResults[15] = results[3]; // PIR_MEANJZH
			tmpResults[16] = results[4]; // PIR_TOTUSJH
			tmpResults[17] = results[5]; // PIR_ABSNJZH
			tmpResults[18] = results[6]; // PIR_SAVNCPP
		}

		//Calculate PIR_TOTPOT PIR_MEANPOT PIR_SHRGT45 PIR_MEANSHR
		weightedCalculators[7]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
#pragma acc parallel
		{
			tmpResults[19] = results[0]; // PIR_TOTPOT
			tmpResults[20] = results[1]; // PIR_MEANPOT
			tmpResults[21] = results[2]; // PIR_SHRGT45
			tmpResults[22] = results[3]; // PIR_MEANSHR
		}

		//update the host before we leave the acc data region.
#pragma acc update host(tmpResults[0:24])
	}

	acc_free(results);
	acc_free(pil_img);

	double* out_solar_features = new double[24];
	for (int i = 0; i < 24; i++) {
		out_solar_features[i] = tmpResults[i];
	}

	return out_solar_features;
}

#endif
