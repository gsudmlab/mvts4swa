/*
 * File: SolarFeatureCalc_omp.cpp
 * Author: Dustin Kempton
 *
 * Created on July 30, 2018
 */

#ifndef FEATURE_CALC_OMP_CPP
#define FEATURE_CALC_OMP_CPP

#include "parameters/AbsoluteFluxCalculator_omp.cpp"
#include "parameters/GammaCalculator_omp.cpp"
#include "parameters/BH_DerivativeCalculator_omp.cpp"
#include "parameters/LorentzCalculator_omp.cpp"
#include "parameters/B_DerivativeCalculator_omp.cpp"
#include "parameters/BZ_DerivativeCalculator_omp.cpp"
#include "parameters/JZ_Calculator_omp.cpp"
#include "parameters/GreenPotCalculator_omp.cpp"
#include "parameters/R_ValueCalculator_omp.cpp"
#include "parameters/FractalDimCalculator_omp.cpp"
#include "parameters/PIR_AbsoluteFluxCalculator_omp.cpp"
#include "parameters/PIR_GammaCalculator_omp.cpp"
#include "parameters/PIR_BH_DerivativeCalculator_omp.cpp"
#include "parameters/PIR_LorentzCalculator_omp.cpp"
#include "parameters/PIR_B_DerivativeCalculator_omp.cpp"
#include "parameters/PIR_BZ_DerivativeCalculator_omp.cpp"
#include "parameters/PIR_JZ_Calculator_omp.cpp"
#include "parameters/PIR_GreenPotCalculator_omp.cpp"
#include "filters/BoxCarSmoothingFilter_omp.cpp"
#include "filters/GaussianSmoothingFilter_omp.cpp"
#include "filters/AverageDownSampleFilter_omp.cpp"
#include "filters/LanczosDownSampleFilter_omp.cpp"
#include "filters/PIL_Filter_omp.cpp"
#include "filters/PIL_Filter_W_Gauss_omp.cpp"
#include "transforms/LOSTransform_omp.cpp"

#include <iostream>
#include <math.h>
#include "../include/filters/IImageFilter.cpp"
#include "../include/parameters/IParamCalculator.cpp"
using namespace std;

IParamCalculator** calculators = 0;
IParamCalculator** weightedCalculators = 0;

LOSTransform_omp* losCorrection = 0;
IImageFilter* PIR_PILFilter = 0;

int dev;
int numDevices;
double radsindeg = (3.14159265358979323846264338327950288) / 180.0;
double scale = 25;
double sigma = 10.0 / 2.3548;
double pir_sigma = (10.0 / 2.3548) * 4;
int hwidthBoxCar = 1;
int hwidthGauss = 20;
int pir_hwidthGauss = 80;
int pilThreshold = 50;
int pir_pilThreshold = 150;

extern "C" double* computeRegularParams(double* bx, double* by, double* bz,
		double* mask, double* bitmask, double* los, double cdelt1,
		double crpix1, double crpix2, double crval1, double crval2,
		double rsun_ref, double rsun_obs, double dsun_obs, double crln_obs,
		int nx, int ny);

extern "C" double* computePIRParams(double* bx, double* by, double* bz,
		double* mask, double* bitmask, double* los, double cdelt1,
		double crpix1, double crpix2, double crval1, double crval2,
		double rsun_ref, double rsun_obs, double dsun_obs, double crln_obs,
		int nx, int ny);

extern "C" void init_lib();

extern "C" void computeSolarFeatures(double* bx, double* by, double* bz,
		double* mask, double* bitmask, double* los, double cdelt1,
		double crpix1, double crpix2, double crval1, double crval2,
		double rsun_ref, double rsun_obs, double dsun_obs, double crln_obs,
		int nx, int ny, double* out_solar_features);

void init_lib() {
	if (!calculators || !weightedCalculators) {

		///////////////////////////////////////////
		//Init for the regular param calculation classes
		///////////////////////////////////////////
		calculators = new IParamCalculator*[10];
		calculators[0] = new AbsoluteFluxCalculator_omp();
		calculators[1] = new GammaCalculator_omp();
		calculators[2] = new BH_DerivativeCalculator_omp();
		calculators[3] = new LorentzCalculator_omp();
		calculators[4] = new B_DerivativeCalculator_omp();
		calculators[5] = new BZ_DerivativeCalculator_omp();
		calculators[6] = new JZ_Calculator_omp();
		calculators[7] = new GreenPotCalculator_omp();

		IImageFilter* averDownFilter = new AverageDownSampleFilter_omp(scale);
		IImageFilter* lanzDownFilter = new LanczosDownSampleFilter_omp(scale);
		IImageFilter* boxcarFilter = new BoxCarSmoothingFilter_omp(
				hwidthBoxCar);
		IImageFilter* gaussianFilter = new GaussianSmoothingFilter_omp(
				hwidthGauss, sigma);

		calculators[8] = new R_ValueCalculator_omp(averDownFilter,
				lanzDownFilter, boxcarFilter, gaussianFilter, scale);

		IImageFilter * PILFilter = new PIL_Filter_omp(boxcarFilter,
				pilThreshold);
		calculators[9] = new FractalDimCalculator_omp(PILFilter);

		losCorrection = new LOSTransform_omp();

		////////////////////////////////////////////
		//Init for the PIR calculations classes.
		///////////////////////////////////////////
		weightedCalculators = new IParamCalculator*[8];
		weightedCalculators[0] = new PIR_AbsoluteFluxCalculator_omp();
		weightedCalculators[1] = new PIR_GammaCalculator_omp();
		weightedCalculators[2] = new PIR_BH_DerivativeCalculator_omp();
		weightedCalculators[3] = new PIR_LorentzCalculator_omp();
		weightedCalculators[4] = new PIR_B_DerivativeCalculator_omp();
		weightedCalculators[5] = new PIR_BZ_DerivativeCalculator_omp();
		weightedCalculators[6] = new PIR_JZ_Calculator_omp();
		weightedCalculators[7] = new PIR_GreenPotCalculator_omp();

		IImageFilter* pir_gaussianFilter = new GaussianSmoothingFilter_omp(
				pir_hwidthGauss, pir_sigma);

		PIR_PILFilter = new PIL_Filter_W_Gauss_omp(boxcarFilter,
				pir_gaussianFilter, pir_pilThreshold, pir_hwidthGauss);
	}
}

/**
 * Computs and retuns the solar features in this order:
 * 	[USFLUX, MEANGAM, MEANGBH, EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY, TOTFZ
 * 	MEANGBT, MEANGBZ, TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH, ABSNJZH, SAVNCPP
 * 	TOTPOT, MEANPOT, SHRGT45, MEANSHR, R_VALUE, RBZ_VALUE, RBT_VALUE, RBP_VALUE,
 * 	FDIM, BZ_FDIM, BT_FDIM, BP_FDIM, PIR_USFLUX, PIR_MEANGAM, PIR_MEANGBH, PIR_EPSX,
 * 	PIR_EPSY, PIR_EPSZ, PIR_TOTBSQ, PIR_TOTFX, PIR_TOTFY, PIR_TOTFZ,
 * 	PIR_MEANGBT, PIR_MEANGBZ, PIR_TOTUSJZ, PIR_MEANJZD, PIR_MEANALP, PIR_MEANJZH,
 * 	PIR_TOTUSJH, PIR_ABSNJZH, PIR_SAVNCPP, PIR_TOTPOT, PIR_MEANPOT, PIR_SHRGT45,
 * 	PIR_MEANSHR, PIL_LEN]
 */
void computeSolarFeatures(double* bx, double* by, double* bz, double* mask,
		double* bitmask, double* los, double cdelt1, double crpix1,
		double crpix2, double crval1, double crval2, double rsun_ref,
		double rsun_obs, double dsun_obs, double crln_obs, int nx, int ny,
		double* out_solar_features) {

	double* results1 = computeRegularParams(bx, by, bz, mask, bitmask, los,
			cdelt1, crpix1, crpix2, crval1, crval2, rsun_ref, rsun_obs,
			dsun_obs, crln_obs, nx, ny);

	double* results2 = computePIRParams(bx, by, bz, mask, bitmask, los, cdelt1,
			crpix1, crpix2, crval1, crval2, rsun_ref, rsun_obs, dsun_obs,
			crln_obs, nx, ny);

	int nResult1 = 31;
	for (int i = 0; i < nResult1; i++) {
		out_solar_features[i] = results1[i];
	}

	int nResult2 = 23;
	for (int i = 0; i < nResult2; i++) {
		out_solar_features[i + nResult1] = results2[i];
	}

	delete[] results1;
	delete[] results2;
}

/**
 * Computes and returns the solar features in this order:
 * 	[USFLUX, MEANGAM, MEANGBH, EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY, TOTFZ
 * 	MEANGBT, MEANGBZ, TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH, ABSNJZH, SAVNCPP
 * 	TOTPOT, MEANPOT, SHRGT45, MEANSHR, R_VALUE, RBZ_VALUE, RBT_VALUE, RBP_VALUE,
 * 	FDIM, BZ_FDIM, BT_FDIM, BP_FDIM] 31 of them
 */
double* computeRegularParams(double* bx, double* by, double* bz, double* mask,
		double* bitmask, double* los, double cdelt1, double crpix1,
		double crpix2, double crval1, double crval2, double rsun_ref,
		double rsun_obs, double dsun_obs, double crln_obs, int nx, int ny) {

	losCorrection->process(los, crval1, crval2, crln_obs, crpix1, crpix2,
			cdelt1, rsun_ref, dsun_obs, nx, ny);

	//convert cdelt1 from degrees to arcsec
	double convertedCdelt1 =
			(atan((rsun_ref * cdelt1 * radsindeg) / (dsun_obs)))
					* (1 / radsindeg) * (3600.0);

	double* results = new double[7];
	double* out_solar_features = new double[31];

	//Calculate USFLUX, MEANGAM, MEANGBH
	for (int i = 0; i < 3; i++) {

		calculators[i]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
		out_solar_features[i] = results[0];
	}

	//Calculate EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY, TOTFZ
	calculators[3]->calculateParameter(bx, by, bz, mask, bitmask, los,
			convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
	out_solar_features[3] = results[0]; // EPSX
	out_solar_features[4] = results[1]; // EPSY
	out_solar_features[5] = results[2]; // EPSZ
	out_solar_features[6] = results[3]; // TOTBSQ
	out_solar_features[7] = results[4]; // TOTFX
	out_solar_features[8] = results[5]; // TOTFY
	out_solar_features[9] = results[6]; // TOTFZ

	//Calculate MEANGBT, MEANGBZ
	for (int i = 0; i < 2; i++) {
		calculators[i + 4]->calculateParameter(bx, by, bz, mask, bitmask, los,
				convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
		out_solar_features[i + 10] = results[0];
	}

	//Calculate // TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH, ABSNJZH, SAVNCPP
	calculators[6]->calculateParameter(bx, by, bz, mask, bitmask, los,
			convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
	out_solar_features[12] = results[0]; // TOTUSJZ
	out_solar_features[13] = results[1]; // MEANJZD
	out_solar_features[14] = results[2]; // MEANALP
	out_solar_features[15] = results[3]; // MEANJZH
	out_solar_features[16] = results[4]; // TOTUSJH
	out_solar_features[17] = results[5]; // ABSNJZH
	out_solar_features[18] = results[6]; // SAVNCPP

	//Calculate TOTPOT, MEANPOT, SHRGT45, MEANSHR
	calculators[7]->calculateParameter(bx, by, bz, mask, bitmask, los,
			convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
	out_solar_features[19] = results[0]; // TOTPOT
	out_solar_features[20] = results[1]; // MEANPOT
	out_solar_features[21] = results[2]; // SHRGT45
	out_solar_features[22] = results[3]; // MEANSHR

	//Calculate R_VALUE, RBZ_VALUE, RBT_VALUE, RBP_VALUE
	calculators[8]->calculateParameter(bx, by, bz, mask, bitmask, los,
			convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
	out_solar_features[23] = results[0]; // R_VALUE
	out_solar_features[24] = results[1]; // RBZ_VALUE
	out_solar_features[25] = results[2]; // RBT_VALUE
	out_solar_features[26] = results[3]; // RBP_VALUE

	//Calculate FDIM, BZ_FDIM, BT_FDIM, BP_FDIM
	calculators[9]->calculateParameter(bx, by, bz, mask, bitmask, los,
			convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
	out_solar_features[27] = results[0]; // R_VALUE
	out_solar_features[28] = results[1]; // RBZ_VALUE
	out_solar_features[29] = results[2]; // RBT_VALUE
	out_solar_features[30] = results[3]; // RBP_VALUE

	delete[] results;

	return out_solar_features;
}

/**
 * Computes and returns the solar features in this order:
 * 	[PIR_USFLUX, PIR_MEANGAM, PIR_MEANGBH, PIR_EPSX, PIR_EPSY, PIR_EPSZ, PIR_TOTBSQ,
 * 	PIR_TOTFX, PIR_TOTFY, PIR_TOTFZ, PIR_MEANGBT, PIR_MEANGBZ, PIR_TOTUSJZ, PIR_MEANJZD,
 * 	PIR_MEANALP, PIR_MEANJZH, PIR_TOTUSJH, PIR_ABSNJZH, PIR_SAVNCPP, PIR_TOTPOT,
 * 	PIR_MEANPOT, PIR_SHRGT45, PIR_MEANSHR] 24 of them
 */
double* computePIRParams(double* bx, double* by, double* bz, double* mask,
		double* bitmask, double* los, double cdelt1, double crpix1,
		double crpix2, double crval1, double crval2, double rsun_ref,
		double rsun_obs, double dsun_obs, double crln_obs, int nx, int ny) {

	//convert cdelt1 from degrees to arcsec
	double convertedCdelt1 =
			(atan((rsun_ref * cdelt1 * radsindeg) / (dsun_obs)))
					* (1 / radsindeg) * (3600.0);

	double* pil_img = new double[nx * ny];
	int pilLen = PIR_PILFilter->process(bz, pil_img, nx, ny);

	double* results = new double[7];
	double* out_solar_features = new double[24];
	out_solar_features[23] = pilLen;

	//Calculate PIR_USFLUX, PIR_MEANGAM, PIR_MEANGBH
	for (int i = 0; i < 3; i++) {

		weightedCalculators[i]->calculateParameter(bx, by, bz, mask, bitmask,
				pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
		out_solar_features[i] = results[0];
	}

	//Calculate PIR_EPSX, PIR_EPSY, PIR_EPSZ, PIR_TOTBSQ,
	//PIR_TOTFX, PIR_TOTFY, PIR_TOTFZ
	weightedCalculators[3]->calculateParameter(bx, by, bz, mask, bitmask,
			pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
	out_solar_features[3] = results[0]; // PIR_EPSX
	out_solar_features[4] = results[1]; // PIR_EPSY
	out_solar_features[5] = results[2]; // PIR_EPSZ
	out_solar_features[6] = results[3]; // PIR_TOTBSQ
	out_solar_features[7] = results[4]; // PIR_TOTFX
	out_solar_features[8] = results[5]; // PIR_TOTFY
	out_solar_features[9] = results[6]; // PIR_TOTFZ

	//Calculate PIR_MEANGBT, PIR_MEANGBZ
	for (int i = 0; i < 2; i++) {
		weightedCalculators[i + 4]->calculateParameter(bx, by, bz, mask,
				bitmask, pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny,
				results);
		out_solar_features[i + 10] = results[0];
	}

	//Calculate PIR_TOTUSJZ, PIR_MEANJZD, PIR_MEANALP, PIR_MEANJZH,
	//PIR_TOTUSJH, PIR_ABSNJZH, PIR_SAVNCPP
	weightedCalculators[6]->calculateParameter(bx, by, bz, mask, bitmask,
			pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
	out_solar_features[12] = results[0]; // PIR_TOTUSJZ
	out_solar_features[13] = results[1]; // PIR_MEANJZD
	out_solar_features[14] = results[2]; // PIR_MEANALP
	out_solar_features[15] = results[3]; // PIR_MEANJZH
	out_solar_features[16] = results[4]; // PIR_TOTUSJH
	out_solar_features[17] = results[5]; // PIR_ABSNJZH
	out_solar_features[18] = results[6]; // PIR_SAVNCPP

	//Calculate PIR_TOTPOT, PIR_MEANPOT, PIR_SHRGT45, PIR_MEANSHR
	weightedCalculators[7]->calculateParameter(bx, by, bz, mask, bitmask,
			pil_img, convertedCdelt1, rsun_ref, rsun_obs, nx, ny, results);
	out_solar_features[19] = results[0]; // PIR_TOTPOT
	out_solar_features[20] = results[1]; // PIR_MEANPOT
	out_solar_features[21] = results[2]; // PIR_SHRGT45
	out_solar_features[22] = results[3]; // PIR_MEANSHR

	delete[] pil_img;
	delete[] results;

	return out_solar_features;
}

#endif
