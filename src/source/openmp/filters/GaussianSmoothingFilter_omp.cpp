/*
 * File: GaussianSmoothingFilter_omp.cpp
 * Author: Dustin Kempton
 *
 *
 * Source of this filter has been adapted from:
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *
 * Created on October 01, 2018
 */
#ifndef GAUSSIANFILTER_OMP_CPP
#define GAUSSIANFILTER_OMP_CPP

#include <math.h>

#include "../../include/filters/IImageFilter.cpp"

class GaussianSmoothingFilter_omp : public IImageFilter {
    struct Kernel1D {
    public:
        double *coeffs = nullptr;
        int size = 0;
    };

    Kernel1D *kerx;
    Kernel1D *kery;
    int hwidth;

public:
    /**
     * hwidth: is half width of boxcar. Full is 2*hwidth+1.
     * Shape is exp(-(d/sigma)^2/2)
     *
     */
    GaussianSmoothingFilter_omp(int hwidth, double sigma) {
        this->hwidth = hwidth;
        int fwidth = 2 * hwidth + 1;

        this->kerx = new Kernel1D();
        this->kery = new Kernel1D();

        this->kerx->coeffs = new double[fwidth];
        this->kery->coeffs = new double[fwidth];

        this->kerx->size = fwidth;
        this->kery->size = fwidth;

        double sum = 0.0f;

        for (int i = 0; i < fwidth; i++) {
            double x = (i - hwidth) / sigma;
            double y = exp(-x * x / 2);

            sum = sum + y;
        }

        for (int i = 0; i < fwidth; i++) {
            double x = (i - hwidth) / sigma;
            double y = exp(-x * x / 2);

            this->kerx->coeffs[i] = y / sum;
            this->kery->coeffs[i] = y / sum;
        }
    }

    ~GaussianSmoothingFilter_omp() {
        delete[] kerx->coeffs;
        delete[] kery->coeffs;
        delete kerx;
        delete kery;
    }

    int process(double *img, double *out_img, int nx, int ny) {

        Image *inImgWrapper = new Image();
        inImgWrapper->values = img;
        inImgWrapper->nX = nx;
        inImgWrapper->nY = ny;

        //for each pixel in output image
#pragma omp parallel for collapse(2)
        for (int y = 0; y < ny; y++) {
            for (int x = 0; x < nx; x++) {
                double val = this->convolve(inImgWrapper, x, y,
                                            this->kerx->coeffs, this->kery->coeffs,
                                            this->kerx->size);
                out_img[y * nx + x] = val;
            }
        }

        delete inImgWrapper;

        return 0;
    }

private:

    struct Image {
    public:
        double *values;
        int nX;
        int nY;
    };

    double convolve(Image *img, int x, int y, double *kerX, double *kerY,
                    int kernSize) {

        int nx = img->nX;
        int ny = img->nY;

        const int halfwindow = kernSize / 2;

        double outValue = 0;
        //Loop over the row and column offset from the pixel we are to
        //put the output into.

        for (int dy = -halfwindow; dy <= halfwindow; dy++) {
            int row = y + dy;

            if ((row < 0 || row >= ny))
                continue;

            int idxY = (row) * nx;

            double gx = 0;
            //pass 1: horizontal convolution of values
            for (int i = -halfwindow; i <= halfwindow; i++) {
                int idx = (x + i);
                if (idx < 0 || idx >= nx)
                    continue;

                gx += img->values[idxY + idx] * kerX[halfwindow - i];
            }

            outValue += gx * kerY[halfwindow - dy];
        }

        return outValue;
    }
};

#endif
