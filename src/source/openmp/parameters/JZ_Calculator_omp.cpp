/*
 * File: JZ_Calculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 31, 2018
 */

#ifndef JZ_CALC_OMP_CPP
#define JZ_CALC_OMP_CPP

#include <math.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH, ABSNJZH, SAVNCPP parameters.
 */
class JZ_Calculator_omp: public IParamCalculator {
private:
	double MUNAUGHT = 0.0000012566370614; /* magnetic constant */

public:

	/**
	 * calculateParameter: returns the TOTUSJZ, MEANJZD, MEANALP, MEANJZH, TOTUSJH,
	 * ABSNJZH, SAVNCPP parameters in results[0] -- results[6]
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, double cdelt1, double rsun_ref,
			double rsun_obs, int nx, int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return 0;

		double* jz = new double[nx * ny];
		double* derx = new double[nx * ny];
		double* dery = new double[nx * ny];

		int retVal = 7;
		this->computeJz(bx, by, mask, bitmask, nx, ny, cdelt1, rsun_ref,
				rsun_obs, jz, derx, dery);

		double us_i = 0;
		double mean_jz = 0;

		this->computeJzsmooth(jz, mask, bitmask, &mean_jz, &us_i, nx, ny,
				cdelt1, rsun_ref, rsun_obs, derx, dery);

		double mean_alpha = this->computeAlpha(bz, jz, mask, bitmask, nx, ny,
				cdelt1, rsun_ref, rsun_obs);

		double mean_ih = 0;
		double total_us_ih = 0;
		double total_abs_ih = 0;

		computeHelicity(bz, jz, mask, bitmask, &mean_ih, &total_us_ih,
				&total_abs_ih, nx, ny, cdelt1, rsun_ref, rsun_obs);

		double totaljz = computeSumAbsPerPolarity(bz, jz, mask, bitmask, nx, ny,
				cdelt1, rsun_ref, rsun_obs);

		results[0] = us_i;
		results[1] = mean_jz;
		results[2] = mean_alpha;
		results[3] = mean_ih;
		results[4] = total_us_ih;
		results[5] = total_abs_ih;
		results[6] = totaljz;

		delete[] jz;
		delete[] derx;
		delete[] dery;

		return retVal;

	}

private:

	void computeJz(double *bx, double *by, double *mask, double *bitmask,
			int nx, int ny, double cdelt1, double rsun_ref, double rsun_obs,
			double *jz, double *derx, double *dery) {

#pragma omp parallel
		{
			/* Calculate the derivative*/
			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma omp for nowait collapse(2)
			for (int i = 1; i < nx - 1; i++) {
				for (int j = 0; j < ny; j++) {
					derx[j * nx + i] = (by[j * nx + i + 1] - by[j * nx + i - 1])
							* 0.5;
				}
			}

#pragma omp for collapse(2)
			for (int i = 0; i < nx; i++) {
				for (int j = 1; j < ny - 1; j++) {
					dery[j * nx + i] = (bx[(j + 1) * nx + i]
							- bx[(j - 1) * nx + i]) * 0.5;

				}
			}

			/* consider the edges for the arrays that contribute to the variable "sum" in the computation below.
			 ignore the edges for the error terms as those arrays have been initialized to zero.
			 this is okay because the error term will ultimately not include the edge pixels as they are selected out by the mask and bitmask arrays.*/

#pragma omp for
			for (int j = 0; j < ny; j++) {
				derx[j * nx] = ((-3 * by[j * nx]) + (4 * by[j * nx + 1])
						- (by[j * nx + 2])) * 0.5;
			}

			const int edgI = nx - 1;
#pragma omp for
			for (int j = 0; j < ny; j++) {
				derx[j * nx + edgI] = ((3 * by[j * nx + edgI])
						+ (-4 * by[j * nx + (edgI - 1)])
						- (-by[j * nx + (edgI - 2)])) * 0.5;
			}

#pragma omp for
			for (int i = 0; i < nx; i++) {
				dery[i] = ((-3 * bx[i]) + (4 * bx[nx + i]) - (bx[2 * nx + i]))
						* 0.5;
			}

			const int edgJ = ny - 1;
#pragma omp for
			for (int i = 0; i < nx; i++) {
				dery[edgJ * nx + i] = ((3 * bx[edgJ * nx + i])
						+ (-4 * bx[(edgJ - 1) * nx + i])
						- (-bx[(edgJ - 2) * nx + i])) * 0.5;
			}

#pragma omp  for collapse(2)
			for (int i = 0; i < nx; i++) {
				for (int j = 0; j < ny; j++) {
					// calculate jz at all points
					jz[j * nx + i] = (derx[j * nx + i] - dery[j * nx + i]);

				}
			}
		}

	}

	void computeJzsmooth(double *jz, double *mask, double *bitmask,
			double *mean_jz_ptr, double *us_i_ptr, int nx, int ny,
			double cdelt1, double rsun_ref, double rsun_obs, double *derx,
			double *dery)

			{

		int count_mask = 0;
		double curl = 0.0;
		double us_i = 0.0;

		/* At this point, use the smoothed Jz array with a Gaussian (FWHM of 4 pix and truncation width of 12 pixels) but keep the original array dimensions*/
#pragma omp parallel for collapse(2),reduction(+:curl),reduction(+:us_i),reduction(+:count_mask)
		for (int i = 0; i < nx; i++) {
			for (int j = 0; j < ny; j++) {
				if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
					continue;
				if (isnan(derx[j * nx + i]))
					continue;
				if (isnan(dery[j * nx + i]))
					continue;
				if (isnan(jz[j * nx + i]))
					continue;
				curl += (jz[j * nx + i]) * (1 / cdelt1) * (rsun_obs / rsun_ref)
						* (0.00010) * (1 / MUNAUGHT) * (1000.); /* curl is in units of mA / m^2 */
				us_i += fabs(jz[j * nx + i]) * (cdelt1 / 1)
						* (rsun_ref / rsun_obs) * (0.00010) * (1 / MUNAUGHT); /* us_i is in units of A */

				count_mask++;
			}
		}

		/* Calculate mean vertical current density (mean_jz) and total unsigned vertical current (us_i) using smoothed Jz array and continue conditions above */
		*mean_jz_ptr = curl / (count_mask); /* mean_jz gets populated as MEANJZD */

		*us_i_ptr = us_i; /* us_i gets populated as TOTUSJZ */

	}

	double computeAlpha(double *bz, double *jz, double *mask, double *bitmask,
			int nx, int ny, double cdelt1, double rsun_ref, double rsun_obs) {

		double A = 0.0;
		double B = 0.0;

#pragma omp parallel for collapse(2),reduction(+:A),reduction(+:B)
		for (int i = 1; i < nx - 1; i++) {
			for (int j = 1; j < ny - 1; j++) {
				if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
					continue;
				if (isnan(jz[j * nx + i]))
					continue;
				if (isnan(bz[j * nx + i]))
					continue;
				if (jz[j * nx + i] == 0.0)
					continue;
				if (bz[j * nx + i] == 0.0)
					continue;
				A += jz[j * nx + i] * bz[j * nx + i];
				B += bz[j * nx + i] * bz[j * nx + i];
			}
		}

		/* Determine the absolute value of alpha. The units for alpha are 1/Mm */
		double C = ((1 / cdelt1) * (rsun_obs / rsun_ref) * (1000000.));
		double mean_alpha = ((A / B) * C);

		return mean_alpha;
	}

	void computeHelicity(double *bz, double *jz, double *mask, double *bitmask,
			double *mean_ih_ptr, double *total_us_ih_ptr,
			double *total_abs_ih_ptr, int nx, int ny, double cdelt1,
			double rsun_ref, double rsun_obs) {

		int count_mask = 0;
		double sum = 0.0;
		double sum2 = 0.0;

		const int n = nx * ny;

#pragma omp parallel for reduction(+:sum),reduction(+:sum2),reduction(+:count_mask)
		for (int i = 0; i < n; i++) {

			if (mask[i] < 70 || bitmask[i] < 30)
				continue;
			if (isnan(jz[i]))
				continue;
			if (isnan(bz[i]))
				continue;
			if (bz[i] == 0.0)
				continue;
			if (jz[i] == 0.0)
				continue;

			sum += (jz[i] * bz[i]) * (1 / cdelt1) * (rsun_obs / rsun_ref); // contributes to MEANJZH and ABSNJZH
			sum2 += fabs(jz[i] * bz[i]) * (1 / cdelt1) * (rsun_obs / rsun_ref); // contributes to TOTUSJH

			count_mask++;

		}

		*mean_ih_ptr = sum / count_mask; /* Units are G^2 / m ; keyword is MEANJZH */
		*total_us_ih_ptr = sum2; /* Units are G^2 / m ; keyword is TOTUSJH */
		*total_abs_ih_ptr = fabs(sum); /* Units are G^2 / m ; keyword is ABSNJZH */

	}

	double computeSumAbsPerPolarity(double *bz, double *jz, double *mask,
			double *bitmask, int nx, int ny, double cdelt1, double rsun_ref,
			double rsun_obs) {

		double sum1 = 0.0;
		double sum2 = 0.0;

		const int n = nx * ny;

#pragma omp parallel for reduction(+:sum1),reduction(+:sum2)
		for (int i = 0; i < n; i++) {

			if (mask[i] < 70 || bitmask[i] < 30)
				continue;
			if (isnan(bz[i]))
				continue;
			if (isnan(jz[i]))
				continue;

			if (bz[i] > 0)
				sum1 += (jz[i]) * (1 / cdelt1) * (0.00010) * (1 / MUNAUGHT)
						* (rsun_ref / rsun_obs);
			if (bz[i] <= 0)
				sum2 += (jz[i]) * (1 / cdelt1) * (0.00010) * (1 / MUNAUGHT)
						* (rsun_ref / rsun_obs);

		}

		double totaljzptr = fabs(sum1) + fabs(sum2); /* Units are Amperes per arcsecond */

		return totaljzptr;
	}
};

#endif
