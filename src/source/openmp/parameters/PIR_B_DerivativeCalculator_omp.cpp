/*
 * File: PIR_B_derivativeCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 * With modification to utilize a weighting map around the polarity inversion line.
 *
 * Created on April 23, 2019
 */

#ifndef PIR_B_DERIVATIVE_OMP_CPP
#define PIR_B_DERIVATIVE_OMP_CPP

#include <math.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the MEANGBT parameter using a weighting function around the PIL
 */
class PIR_B_DerivativeCalculator_omp: public IParamCalculator {

public:

	/**
	 * calculateParameter: returns the PIR_MEANGBT parameter in results[0]
	 *
	 * Assumes that los is the PIL weight map and not the los image as is expected
	 * in other classes of the IParamCalculator type.
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, double cdelt1, double rsun_ref,
			double rsun_obs, int nx, int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return 0;

		const int n = nx * ny;

		double* bt = new double[n];
		double* derx_bt = new double[n];
		double* dery_bt = new double[n];

		int count_mask = 0;
		double sum = 0.0;

#pragma omp parallel
		{

#pragma omp parallel for
			for (int i = 0; i < n; i++) {
				if (isnan(bx[i])) {
					bt[i] = NAN;
					continue;
				}
				if (isnan(by[i])) {
					bt[i] = NAN;
					continue;
				}
				if (isnan(bz[i])) {
					bt[i] = NAN;
					continue;
				}
				bt[i] = sqrt(bx[i] * bx[i] + by[i] * by[i] + bz[i] * bz[i]);

			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma omp for nowait collapse(2)
			for (int j = 0; j <= ny - 1; j++) {
				for (int i = 1; i <= nx - 2; i++) {
					derx_bt[j * nx + i] = (bt[j * nx + i + 1]
							- bt[j * nx + i - 1]) * 0.5;

				}
			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma omp  for collapse(2)
			for (int j = 1; j <= ny - 2; j++) {
				for (int i = 0; i <= nx - 1; i++) {
					dery_bt[j * nx + i] = (bt[(j + 1) * nx + i]
							- bt[(j - 1) * nx + i]) * 0.5;

				}
			}

			/* consider the edges for the arrays that contribute to the variable "sum" in the computation below.
			 ignore the edges for the error terms as those arrays have been initialized to zero.
			 this is okay because the error term will ultimately not include the edge pixels as they are selected out by the mask and bitmask arrays.*/
			//i = 0;
#pragma omp  for
			for (int j = 0; j <= ny - 1; j++) {
				derx_bt[j * nx] = ((-3 * bt[j * nx]) + (4 * bt[j * nx + (1)])
						- (bt[j * nx + (2)])) * 0.5;
			}

			const int edgI = nx - 1;
#pragma omp  for
			for (int j = 0; j <= ny - 1; j++) {
				derx_bt[j * nx + edgI] = ((3 * bt[j * nx + edgI])
						+ (-4 * bt[j * nx + (edgI - 1)])
						- (-bt[j * nx + (edgI - 2)])) * 0.5;
			}

			//j = 0;
#pragma omp  for
			for (int i = 0; i <= nx - 1; i++) {
				dery_bt[i] =
						((-3 * bt[i]) + (4 * bt[nx + i]) - (bt[2 * nx + i]))
								* 0.5;
			}

			const int edgJ = ny - 1;
#pragma omp  for
			for (int i = 0; i <= nx - 1; i++) {
				dery_bt[edgJ * nx + i] = ((3 * bt[edgJ * nx + i])
						+ (-4 * bt[(edgJ - 1) * nx + i])
						- (-bt[(edgJ - 2) * nx + i])) * 0.5;
			}

#pragma omp  for collapse(2),reduction(+:sum),reduction(+:count_mask)
			for (int j = 1; j <= ny - 2; j++) {
				for (int i = 1; i <= nx - 2; i++) {
					if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
						continue;
					if ((derx_bt[j * nx + i] + dery_bt[j * nx + i]) == 0)
						continue;
					if (isnan(bt[j * nx + i]))
						continue;
					if (isnan(bt[(j + 1) * nx + i]))
						continue;
					if (isnan(bt[(j - 1) * nx + i]))
						continue;
					if (isnan(bt[j * nx + i - 1]))
						continue;
					if (isnan(bt[j * nx + i + 1]))
						continue;
					if (isnan(derx_bt[j * nx + i]))
						continue;
					if (isnan(dery_bt[j * nx + i]))
						continue;
					if (los[j * nx + i] == 0)
						continue;

					sum += los[j * nx + i]
							* sqrt(
									derx_bt[j * nx + i] * derx_bt[j * nx + i]
											+ dery_bt[j * nx + i]
													* dery_bt[j * nx + i]); /* Units of Gauss */
					count_mask++;
				}
			}
		}

		delete[] bt;
		delete[] derx_bt;
		delete[] dery_bt;

		double mean_derivative_btotal = (sum) / (count_mask);
		results[0] = mean_derivative_btotal;

		return 1;

	}

};

#endif
