/*
 * File: LorentzCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 * Created on July 31, 2018
 */

#ifndef LORENTZ_OMP_CPP
#define LORENTZ_OMP_CPP

#include <math.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY, TOTFZ parameters
 */
class LorentzCalculator_omp: public IParamCalculator {

public:

	/**
	 * Example function 16: Lorentz force as defined in Fisher, 2012
	 * This calculation is adapted from Xudong's code
	 *  at /proj/cgem/lorentz/apps/lorentz.c
	 *
	 * calculateParameter: returns the EPSX, EPSY, EPSZ, TOTBSQ, TOTFX, TOTFY,
	 * TOTFZ parameters in results[0] -- results[6]
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, double cdelt1, double rsun_ref,
			double rsun_obs, int nx, int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return -1;

		int n = nx * ny;
		double* fx = new double[n];
		double* fy = new double[n];
		double* fz = new double[n];

		double totfx = 0;
		double totfy = 0;
		double totfz = 0;
		double totbsq = 0;

#pragma omp parallel for reduction(+:totfx),reduction(+:totfy),reduction(+:totfz),reduction(+:totbsq)
		for (int i = 0; i < n; i++) {

			if (mask[i] < 70 || bitmask[i] < 30)
				continue;
			if (isnan(bx[i]))
				continue;
			if (isnan(by[i]))
				continue;
			if (isnan(bz[i]))
				continue;

			fx[i] = bx[i] * bz[i];
			fy[i] = by[i] * bz[i];
			fz[i] = (bx[i] * bx[i] + by[i] * by[i] - bz[i] * bz[i]);
			double bsq = bx[i] * bx[i] + by[i] * by[i] + bz[i] * bz[i];
			totfx += fx[i];
			totfy += fy[i];
			totfz += fz[i];
			totbsq += bsq;
		}

		double area = cdelt1 * cdelt1 * (rsun_ref / rsun_obs)
				* (rsun_ref / rsun_obs) * 100.0 * 100.0;

		results[0] = (totfx) / totbsq; // epsx
		results[1] = -(totfy) / totbsq; // epsy
		results[2] = (totfz) / totbsq; // epsz
		results[3] = totbsq; //totbsq
		results[4] = -totfx * area; //totfx
		results[5] = (totfy) * area; //totfy
		results[6] = (totfz) * area; //totfz

		delete[] fx;
		delete[] fy;
		delete[] fz;

		return 7;
	}

};

#endif
