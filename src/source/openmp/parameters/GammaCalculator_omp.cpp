/*
 * File: GammaCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 *
 *  Created on: Jul 28, 2018
 */

#ifndef GAMMA_OMP_CPP
#define GAMMA_OMP_CPP

#include <math.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the MEANGAM parameter
 */
class GammaCalculator_omp: public IParamCalculator {
private:
	double pi = 3.14159265358979323846264338327950288;
public:

	/**
	 * calculateParameter: returns the MEANGAM parameter in results[0]
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, double cdelt1, double rsun_ref,
			double rsun_obs, int nx, int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return 0;

		int n = nx * ny;
		double* bh = new double[n];

		int count_mask = 0;
		double sum = 0.0;

#pragma omp parallel
		{

#pragma omp for
			for (int i = 0; i < n; i++) {

				if (isnan(bx[i])) {
					bh[i] = -1;
					continue;
				}

				if (isnan(by[i])) {
					bh[i] = -1;
					continue;
				}

				bh[i] = sqrt(bx[i] * bx[i] + by[i] * by[i]);
			}

#pragma omp for reduction(+:sum), reduction(+:count_mask)
			for (int i = 0; i < n; i++) {

				if (bh[i] > 100) {
					if (mask[i] < 70 || bitmask[i] < 30)
						continue;
					if (isnan(bz[i]))
						continue;
					if (isnan(bh[i]))
						continue;
					if (bz[i] == 0)
						continue;
					sum += fabs(atan(bh[i] / fabs(bz[i]))) * (180.0 / pi);

					count_mask++;
				}

			}
		}
		delete[] bh;

		double mean_gamma = sum / count_mask;
		results[0] = mean_gamma;

		return 1;
	}

};

#endif
