/*
 * File: PIR_AbsoluteFluxCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 * With modification to utilize a weighting map around the polarity inversion line.
 *
 * Created on April 23, 2019
 */

#ifndef PIR_ABSOLUTEFLUX_OMP_CPP
#define PIR_ABSOLUTEFLUX_OMP_CPP

#include <math.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the USFLUX parameter using the PIR weighting function.
 */
class PIR_AbsoluteFluxCalculator_omp: public IParamCalculator {

public:

	/**
	 * calculateParameter: returns the PIR_USFLUX parameter in results[0]
	 *
	 * Assumes that los is the PIL weight map and not the los image as is expected
	 * in other classes of the IParamCalculator type.
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, double cdelt1, double rsun_ref,
			double rsun_obs, int nx, int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return 0;

		//Calculate how many elements to sum
		const int n = nx * ny;
		//var for holding sum
		double sum = 0.0;

#pragma omp parallel for reduction(+:sum)
		for (int i = 0; i < n; i++) {
			if (mask[i] < 70 || bitmask[i] < 30 || isnan(bz[i]))
				continue;

			sum += (los[i] * fabs(bz[i]));
		}

		double mean_vf = sum * cdelt1 * cdelt1 * (rsun_ref / rsun_obs)
				* (rsun_ref / rsun_obs) * 100.0 * 100.0;

		results[0] = mean_vf;

		return 1;

	}

};

#endif
