/*
 * File: PIR_GreenPotCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 * With modification to utilize a weighting map around the polarity inversion line.
 *
 * Created on April 23, 2019
 */

#ifndef PIR_GREENPOT_OMP_CPP
#define PIR_GREENPOT_OMP_CPP

#include <cstdlib>
#include <math.h>

using namespace std;

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the TOTPOT, MEANPOT, SHRGT45, MEANSHR parameters
 * using a weighting function around the PIL.
 */
class PIR_GreenPotCalculator_omp: public IParamCalculator {
private:
	double pi = (3.14159265358979323846264338327950288);

public:

	/**
	 * calculateParameter: returns PIR_TOTPOT, PIR_MEANPOT, PIR_SHRGT45, PIR_MEANSHR
	 * parameters in results[0] -- results[3]
	 *
	 * Assumes that los is the PIL weight map and not the los image as is expected
	 * in other classes of the IParamCalculator type.
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, double cdelt1, double rsun_ref,
			double rsun_obs, int nx, int ny, double* results) {

		int n = nx * ny;

		double *bztmp = new double[n];
		double *bpx = new double[n]();
		double *bpy = new double[n]();

		double totpot = 0.0;
		double meanpot = 0.0;

		double area_w_shear_gt_45 = 0.0;
		double meanshear_angle = 0.0;

		double retValue = 4;
		if (this->greenpot(bz, bpx, bpy, nx, ny, bztmp)) {

			computeFreeEnergy(bx, by, bpx, bpy, los, nx, ny, &meanpot, &totpot,
					mask, bitmask, cdelt1, rsun_ref, rsun_obs);

			computeShearAngle(bx, by, bz, bpx, bpy, los, nx, ny,
					&meanshear_angle, &area_w_shear_gt_45, mask, bitmask);

		} else {
			retValue = -1;
		}

		results[0] = totpot;
		results[1] = meanpot;
		results[2] = area_w_shear_gt_45;
		results[3] = meanshear_angle;

		delete[] bztmp;
		delete[] bpx;
		delete[] bpy;

		return retValue;
	}
private:

	bool greenpot(double *bz, double *bxp, double *byp, int nx, int ny,
			double *bztmp) {

		if (nx <= 0 || ny <= 0)
			return false;

		int n = nx * ny;

		double *pfpot = new double[n]();
		double *rdist = new double[n]();

		// dz is the monopole depth
		double dz = 0.001;
		int iwindow = 3; //this used to be a bunch of code that did nothing.

#pragma omp parallel
		{

#pragma omp  for nowait
			for (int i = 0; i < n; i++) {

				double val0 = bz[i];
				if (isnan(val0)) {
					bztmp[i] = 0.0;
				} else {
					bztmp[i] = val0;
				}
			}

#pragma omp for collapse(2)
			for (int iny = 0; iny < ny; iny++) {
				for (int inx = 0; inx < nx; inx++) {
					double rdd1 = (double) (inx);
					double rdd2 = (double) (iny);
					double rdd = rdd1 * rdd1 + rdd2 * rdd2 + dz * dz;
					rdist[nx * iny + inx] = 1.0 / sqrt(rdd);
				}
			}

#pragma omp for collapse(2)
			for (int iny = 0; iny < ny; iny++) {
				for (int inx = 0; inx < nx; inx++) {
					double val0 = bz[nx * iny + inx];
					if (isnan(val0)) {
						pfpot[nx * iny + inx] = 0.0; // hmmm.. NAN;
					} else {

						int j2s, j2e, i2s, i2e;
						j2s = iny - iwindow;
						j2e = iny + iwindow;
						if (j2s < 0) {
							j2s = 0;
						}
						if (j2e > ny) {
							j2e = ny;
						}
						i2s = inx - iwindow;
						i2e = inx + iwindow;
						if (i2s < 0) {
							i2s = 0;
						}
						if (i2e > nx) {
							i2e = nx;
						}

						double sum = 0;
						for (int j2 = j2s; j2 < j2e; j2++) {
							for (int i2 = i2s; i2 < i2e; i2++) {
								double val1 = bztmp[nx * j2 + i2];

								int di, dj;
								di = std::abs(i2 - inx);
								dj = std::abs(j2 - iny);
								sum = sum + val1 * rdist[nx * dj + di] * dz;

							}
						}
						pfpot[nx * iny + inx] = sum; // Note that this is a simplified definition.
					}
				}
			}

#pragma omp for collapse(2)
			for (int iny = 1; iny < ny - 1; iny++) {
				for (int inx = 1; inx < nx - 1; inx++) {
					bxp[nx * iny + inx] = -(pfpot[nx * iny + (inx + 1)]
							- pfpot[nx * iny + (inx - 1)]) * 0.5;
					byp[nx * iny + inx] = -(pfpot[nx * (iny + 1) + inx]
							- pfpot[nx * (iny - 1) + inx]) * 0.5;
				}
			}
		}

		delete[] pfpot;
		delete[] rdist;

		return true;
	}

	void computeFreeEnergy(double *bx, double *by, double *bpx, double *bpy,
			double* los, int nx, int ny, double *meanpotptr, double *totpotptr,
			double *mask, double *bitmask, double cdelt1, double rsun_ref,
			double rsun_obs) {

		*totpotptr = 0.0;
		*meanpotptr = 0.0;

		int n = nx * ny;

		int count_mask = 0;
		double sum = 0.0;
		double sum1 = 0.0;

#pragma omp parallel for reduction(+:sum), reduction(+:sum1), reduction(+:count_mask)
		for (int i = 0; i < n; i++) {

			if (mask[i] < 70 || bitmask[i] < 30)
				continue;
			if (isnan(bx[i]))
				continue;
			if (isnan(by[i]))
				continue;
			if (los[i] == 0)
				continue;

			sum += los[i]
					* ((((bx[i] - bpx[i]) * (bx[i] - bpx[i]))
							+ ((by[i] - bpy[i]) * (by[i] - bpy[i])))
							* (cdelt1 * cdelt1 * (rsun_ref / rsun_obs)
									* (rsun_ref / rsun_obs) * 100.0 * 100.0));

			sum1 += los[i]
					* (((bx[i] - bpx[i]) * (bx[i] - bpx[i]))
							+ ((by[i] - bpy[i]) * (by[i] - bpy[i])));

			count_mask++;

		}

		/* Units of meanpotptr are ergs per centimeter */
		*meanpotptr = (sum1) / (count_mask * 8. * pi); /* Units are ergs per cubic centimeter */

		/* Units of sum are ergs/cm^3, units of factor are cm^2/pix^2; therefore, units of totpotptr are ergs per centimeter */
		*totpotptr = (sum) / (8. * pi);

	}

	void computeShearAngle(double *bx, double *by, double *bz, double *bpx,
			double *bpy, double *los, int nx, int ny,
			double *meanshear_angleptr, double *area_w_shear_gt_45ptr,
			double *mask, double *bitmask) {

		*area_w_shear_gt_45ptr = 0.0;
		*meanshear_angleptr = 0.0;

		double count_mask = 0;
		double count = 0;
		double sum = 0.0;

		int n = nx * ny;

#pragma omp parallel for reduction(+:sum), reduction(+:count), reduction(+:count_mask)
		for (int i = 0; i < n; i++) {

			if (mask[i] < 70 || bitmask[i] < 30)
				continue;
			if (isnan(bpx[i]))
				continue;
			if (isnan(bpy[i]))
				continue;
			if (isnan(bz[i]))
				continue;
			if (isnan(bx[i]))
				continue;
			if (isnan(by[i]))
				continue;
			if (los[i] == 0)
				continue;

			/* For mean 3D shear angle, percentage with shear greater than 45*/
			double dotproduct = (bpx[i]) * (bx[i]) + (bpy[i]) * (by[i])
					+ (bz[i]) * (bz[i]);
			double magnitude_potential = sqrt(
					(bpx[i] * bpx[i]) + (bpy[i] * bpy[i]) + (bz[i] * bz[i]));
			double magnitude_vector = sqrt(
					(bx[i] * bx[i]) + (by[i] * by[i]) + (bz[i] * bz[i]));

			double shear_angle = acos(
					dotproduct / (magnitude_potential * magnitude_vector))
					* (180.0 / pi);

			sum += los[i] * shear_angle;

			count++;

			if (shear_angle > 45)
				count_mask++;

		}

		/* For mean 3D shear angle, area with shear greater than 45*/
		*meanshear_angleptr = (sum) / (count); /* Units are degrees */

		/* The area here is a fractional area -- the % of the total area. This has no error associated with it. */
		*area_w_shear_gt_45ptr = (count_mask / (count)) * (100.0);

	}

};

#endif
