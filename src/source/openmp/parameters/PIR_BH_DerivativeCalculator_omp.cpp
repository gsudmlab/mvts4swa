/*
 * File: PIR_BH_DerivativeCalculator_omp.cpp
 * Author: Dustin Kempton
 *
 * C++ parameter computation modules taken from SHARP JSOC code repo.
 * Source of parameter modules: http://jsoc.stanford.edu/cvs/JSOC/proj/sharp/apps/
 * With modification to utilize a weighting map around the polarity inversion line.
 *
 * Created on April 23, 2019
 */

#ifndef PIR_BH_DERIVATIVE_OMP_CPP
#define PIR_BH_DERIVATIVE_OMP_CPP

#include <math.h>

#include "../../include/parameters/IParamCalculator.cpp"

/**
 * This class calculates the MEANGBH parameter using a weighting function around the PIL
 */
class PIR_BH_DerivativeCalculator_omp: public IParamCalculator {

public:

	/*
	 * calculateParameter: returns the PIR_MEANGBH parameter in results[0]
	 *
	 * Assumes that los is the PIL weight map and not the los image as is expected
	 * in other classes of the IParamCalculator type.
	 */
	int calculateParameter(double* bx, double* by, double* bz, double* mask,
			double* bitmask, double* los, const double cdelt1,
			const double rsun_ref, const double rsun_obs, const int nx,
			const int ny, double* results) {

		if (nx <= 0 || ny <= 0)
			return 0;

		const int n = nx * ny;
		double* bh = new double[n];
		double* derx_bh = new double[n];
		double* dery_bh = new double[n];

		int count_mask = 0;
		double sum = 0.0;

#pragma omp parallel
		{
#pragma omp for
			for (int i = 0; i < n; i++) {

				if (isnan(bx[i])) {
					bh[i] = NAN;
					continue;
				}

				if (isnan(by[i])) {
					bh[i] = NAN;
					continue;
				}

				bh[i] = sqrt(bx[i] * bx[i] + by[i] * by[i]);
			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma omp  for nowait collapse(2)
			for (int j = 0; j <= ny - 1; j++) {
				for (int i = 1; i <= nx - 2; i++) {
					derx_bh[j * nx + i] = (bh[j * nx + i + 1]
							- bh[j * nx + i - 1]) * 0.5;
				}
			}

			/* brute force method of calculating the derivative (no consideration for edges) */
#pragma omp  for nowait collapse(2)
			for (int j = 1; j <= ny - 2; j++) {
				for (int i = 0; i <= nx - 1; i++) {
					dery_bh[j * nx + i] = (bh[(j + 1) * nx + i]
							- bh[(j - 1) * nx + i]) * 0.5;
				}
			}

			/* consider the edges for the arrays that contribute to the variable "sum" in the computation below.
			 ignore the edges for the error terms as those arrays have been initialized to zero.
			 this is okay because the error term will ultimately not include the edge pixels as they are selected out by the mask and bitmask arrays.*/

			const int tmpI1 = 0;
#pragma omp  for
			for (int j = 0; j <= ny - 1; j++) {
				derx_bh[j * nx + tmpI1] = ((-3 * bh[j * nx + tmpI1])
						+ (4 * bh[j * nx + (tmpI1 + 1)])
						- (bh[j * nx + (tmpI1 + 2)])) * 0.5;
			}

			int tmpI2 = nx - 1;
#pragma omp  for
			for (int j = 0; j <= ny - 1; j++) {
				derx_bh[j * nx + tmpI2] = ((3 * bh[j * nx + tmpI2])
						+ (-4 * bh[j * nx + (tmpI2 - 1)])
						- (-bh[j * nx + (tmpI2 - 2)])) * 0.5;
			}

			int tmpJ1 = 0;
#pragma omp  for
			for (int i = 0; i <= nx - 1; i++) {
				dery_bh[tmpJ1 * nx + i] = ((-3 * bh[tmpJ1 * nx + i])
						+ (4 * bh[(tmpJ1 + 1) * nx + i])
						- (bh[(tmpJ1 + 2) * nx + i])) * 0.5;
			}

			int tmpJ2 = ny - 1;
#pragma omp  for
			for (int i = 0; i <= nx - 1; i++) {
				dery_bh[tmpJ2 * nx + i] = ((3 * bh[tmpJ2 * nx + i])
						+ (-4 * bh[(tmpJ2 - 1) * nx + i])
						- (-bh[(tmpJ2 - 2) * nx + i])) * 0.5;
			}

#pragma omp  for collapse(2),reduction(+:sum),reduction(+:count_mask)
			for (int j = 0; j <= ny - 1; j++) {
				for (int i = 0; i <= nx - 1; i++) {
					if (mask[j * nx + i] < 70 || bitmask[j * nx + i] < 30)
						continue;
					if ((derx_bh[j * nx + i] + dery_bh[j * nx + i]) == 0)
						continue;
					if (isnan(bh[j * nx + i]))
						continue;
					if (isnan(bh[(j + 1) * nx + i]))
						continue;
					if (isnan(bh[(j - 1) * nx + i]))
						continue;
					if (isnan(bh[j * nx + i - 1]))
						continue;
					if (isnan(bh[j * nx + i + 1]))
						continue;
					if (isnan(derx_bh[j * nx + i]))
						continue;
					if (isnan(dery_bh[j * nx + i]))
						continue;
					if (los[j * nx + i] == 0)
						continue;

					sum += los[j * nx + i]
							* sqrt(
									derx_bh[j * nx + i] * derx_bh[j * nx + i]
											+ dery_bh[j * nx + i]
													* dery_bh[j * nx + i]); /* Units of Gauss */

					count_mask++;
				}
			}

		}

		delete[] bh;
		delete[] derx_bh;
		delete[] dery_bh;

		double mean_derivative_bh_ptr = (sum) / (count_mask); // would be divided by ((nx-2)*(ny-2)) if shape of count_mask = shape of magnetogram
		results[0] = mean_derivative_bh_ptr;

		return 1;

	}

};

#endif
